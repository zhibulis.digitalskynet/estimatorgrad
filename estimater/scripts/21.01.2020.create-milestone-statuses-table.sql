create table milestonestatuses(
	milestonestatusid serial not null,
	milestonestatusname varchar,
	constraint milestonestatutes_pkey primary key (milestonestatusid)
);

insert into milestonestatuses (milestonestatusname) values ('planning'),('in progress'),('in pending'),('expired'),('finished');