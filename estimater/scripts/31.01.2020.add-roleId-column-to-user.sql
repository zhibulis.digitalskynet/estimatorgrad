ALTER TABLE public.users ADD roleid int4 NULL;
ALTER TABLE public.users ADD CONSTRAINT users_fk FOREIGN KEY (roleid) REFERENCES public.roles(roleid) ON DELETE SET NULL;