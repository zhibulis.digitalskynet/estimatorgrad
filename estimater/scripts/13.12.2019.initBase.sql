--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Ubuntu 12.1-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.1 (Ubuntu 12.1-1.pgdg18.04+1)

-- Started on 2019-12-11 12:55:01 MSK

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 217 (class 1259 OID 16485)
-- Name: milestones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.milestones (
    id integer NOT NULL,
    name text,
    projectid integer NOT NULL,
    status text
);


ALTER TABLE public.milestones OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16483)
-- Name: milestones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.milestones_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.milestones_id_seq OWNER TO postgres;

--
-- TOC entry 3034 (class 0 OID 0)
-- Dependencies: 216
-- Name: milestones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.milestones_id_seq OWNED BY public.milestones.id;


--
-- TOC entry 213 (class 1259 OID 16453)
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects (
    projectid integer NOT NULL,
    projectname character varying(30),
    startdate date,
    enddate date,
    projectstatusid integer,
    deleted boolean DEFAULT false
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16451)
-- Name: projects_projectid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projects_projectid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_projectid_seq OWNER TO postgres;

--
-- TOC entry 3035 (class 0 OID 0)
-- Dependencies: 212
-- Name: projects_projectid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.projects_projectid_seq OWNED BY public.projects.projectid;


--
-- TOC entry 207 (class 1259 OID 16413)
-- Name: projectstatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projectstatus (
    projectstatusname text,
    projectstatusid integer NOT NULL
);


ALTER TABLE public.projectstatus OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16411)
-- Name: projectstatus_projectstatusid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projectstatus_projectstatusid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projectstatus_projectstatusid_seq OWNER TO postgres;

--
-- TOC entry 3036 (class 0 OID 0)
-- Dependencies: 206
-- Name: projectstatus_projectstatusid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.projectstatus_projectstatusid_seq OWNED BY public.projectstatus.projectstatusid;


--
-- TOC entry 205 (class 1259 OID 16405)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    roleid integer NOT NULL,
    rolename character varying(30),
    createdate date
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16403)
-- Name: roles_roleid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_roleid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_roleid_seq OWNER TO postgres;

--
-- TOC entry 3037 (class 0 OID 0)
-- Dependencies: 204
-- Name: roles_roleid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_roleid_seq OWNED BY public.roles.roleid;


--
-- TOC entry 219 (class 1259 OID 16501)
-- Name: tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tasks (
    taskid integer NOT NULL,
    taskname character varying(60),
    datestart date,
    endstart date,
    taskstatusid integer,
    milestoneid integer,
    projectid integer NOT NULL,
    optimistictime integer,
    pessimistictime integer
);


ALTER TABLE public.tasks OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16499)
-- Name: tasks_taskid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tasks_taskid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_taskid_seq OWNER TO postgres;

--
-- TOC entry 3038 (class 0 OID 0)
-- Dependencies: 218
-- Name: tasks_taskid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tasks_taskid_seq OWNED BY public.tasks.taskid;


--
-- TOC entry 211 (class 1259 OID 16442)
-- Name: taskstatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.taskstatus (
    taskstatusname text,
    taskstatusid integer NOT NULL
);


ALTER TABLE public.taskstatus OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16440)
-- Name: taskstatus_taskstatusid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.taskstatus_taskstatusid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taskstatus_taskstatusid_seq OWNER TO postgres;

--
-- TOC entry 3039 (class 0 OID 0)
-- Dependencies: 210
-- Name: taskstatus_taskstatusid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.taskstatus_taskstatusid_seq OWNED BY public.taskstatus.taskstatusid;


--
-- TOC entry 203 (class 1259 OID 16396)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    userid integer NOT NULL,
    username character varying(30),
    email character varying(100),
    password text,
    registerdate date,
    firstname character varying(40),
    lastname character varying(40),
    isconfirmed boolean DEFAULT false
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16394)
-- Name: users_userid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_userid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_userid_seq OWNER TO postgres;

--
-- TOC entry 3040 (class 0 OID 0)
-- Dependencies: 202
-- Name: users_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_userid_seq OWNED BY public.users.userid;


--
-- TOC entry 221 (class 1259 OID 16529)
-- Name: usersimages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usersimages (
    id integer NOT NULL,
    userid integer,
    image character varying
);


ALTER TABLE public.usersimages OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16527)
-- Name: usersimages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usersimages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usersimages_id_seq OWNER TO postgres;

--
-- TOC entry 3041 (class 0 OID 0)
-- Dependencies: 220
-- Name: usersimages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usersimages_id_seq OWNED BY public.usersimages.id;


--
-- TOC entry 215 (class 1259 OID 16467)
-- Name: usersprojects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usersprojects (
    id integer NOT NULL,
    userid integer,
    projectid integer
);


ALTER TABLE public.usersprojects OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16465)
-- Name: usersprojects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usersprojects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usersprojects_id_seq OWNER TO postgres;

--
-- TOC entry 3042 (class 0 OID 0)
-- Dependencies: 214
-- Name: usersprojects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usersprojects_id_seq OWNED BY public.usersprojects.id;


--
-- TOC entry 209 (class 1259 OID 16424)
-- Name: usersroles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usersroles (
    id integer NOT NULL,
    userid integer,
    roleid integer,
    createdate date
);


ALTER TABLE public.usersroles OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16422)
-- Name: usersroles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usersroles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usersroles_id_seq OWNER TO postgres;

--
-- TOC entry 3043 (class 0 OID 0)
-- Dependencies: 208
-- Name: usersroles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usersroles_id_seq OWNED BY public.usersroles.id;


--
-- TOC entry 2871 (class 2604 OID 16488)
-- Name: milestones id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestones ALTER COLUMN id SET DEFAULT nextval('public.milestones_id_seq'::regclass);


--
-- TOC entry 2868 (class 2604 OID 16456)
-- Name: projects projectid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects ALTER COLUMN projectid SET DEFAULT nextval('public.projects_projectid_seq'::regclass);


--
-- TOC entry 2865 (class 2604 OID 16416)
-- Name: projectstatus projectstatusid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectstatus ALTER COLUMN projectstatusid SET DEFAULT nextval('public.projectstatus_projectstatusid_seq'::regclass);


--
-- TOC entry 2864 (class 2604 OID 16408)
-- Name: roles roleid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN roleid SET DEFAULT nextval('public.roles_roleid_seq'::regclass);


--
-- TOC entry 2872 (class 2604 OID 16504)
-- Name: tasks taskid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks ALTER COLUMN taskid SET DEFAULT nextval('public.tasks_taskid_seq'::regclass);


--
-- TOC entry 2867 (class 2604 OID 16445)
-- Name: taskstatus taskstatusid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taskstatus ALTER COLUMN taskstatusid SET DEFAULT nextval('public.taskstatus_taskstatusid_seq'::regclass);


--
-- TOC entry 2862 (class 2604 OID 16399)
-- Name: users userid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN userid SET DEFAULT nextval('public.users_userid_seq'::regclass);


--
-- TOC entry 2873 (class 2604 OID 16532)
-- Name: usersimages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersimages ALTER COLUMN id SET DEFAULT nextval('public.usersimages_id_seq'::regclass);


--
-- TOC entry 2870 (class 2604 OID 16470)
-- Name: usersprojects id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersprojects ALTER COLUMN id SET DEFAULT nextval('public.usersprojects_id_seq'::regclass);


--
-- TOC entry 2866 (class 2604 OID 16427)
-- Name: usersroles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersroles ALTER COLUMN id SET DEFAULT nextval('public.usersroles_id_seq'::regclass);


--
-- TOC entry 2889 (class 2606 OID 16493)
-- Name: milestones milestones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestones
    ADD CONSTRAINT milestones_pkey PRIMARY KEY (id);


--
-- TOC entry 2885 (class 2606 OID 16459)
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (projectid);


--
-- TOC entry 2879 (class 2606 OID 16421)
-- Name: projectstatus projectstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projectstatus
    ADD CONSTRAINT projectstatus_pkey PRIMARY KEY (projectstatusid);


--
-- TOC entry 2877 (class 2606 OID 16410)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (roleid);


--
-- TOC entry 2891 (class 2606 OID 16506)
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (taskid);


--
-- TOC entry 2883 (class 2606 OID 16450)
-- Name: taskstatus taskstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.taskstatus
    ADD CONSTRAINT taskstatus_pkey PRIMARY KEY (taskstatusid);


--
-- TOC entry 2875 (class 2606 OID 16402)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);


--
-- TOC entry 2887 (class 2606 OID 16472)
-- Name: usersprojects usersprojects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersprojects
    ADD CONSTRAINT usersprojects_pkey PRIMARY KEY (id);


--
-- TOC entry 2881 (class 2606 OID 16429)
-- Name: usersroles usersroles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersroles
    ADD CONSTRAINT usersroles_pkey PRIMARY KEY (id);


--
-- TOC entry 2898 (class 2606 OID 16507)
-- Name: tasks milestoneid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT milestoneid FOREIGN KEY (milestoneid) REFERENCES public.milestones(id);


--
-- TOC entry 2897 (class 2606 OID 16494)
-- Name: milestones projectid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestones
    ADD CONSTRAINT projectid FOREIGN KEY (projectid) REFERENCES public.projects(projectid);


--
-- TOC entry 2894 (class 2606 OID 16460)
-- Name: projects projectstatusid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projectstatusid FOREIGN KEY (projectstatusid) REFERENCES public.projectstatus(projectstatusid);


--
-- TOC entry 2899 (class 2606 OID 16517)
-- Name: tasks tasks_milestoneid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_milestoneid_fkey FOREIGN KEY (milestoneid) REFERENCES public.milestones(id);


--
-- TOC entry 2901 (class 2606 OID 24767)
-- Name: tasks tasks_projectid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_projectid_fkey FOREIGN KEY (projectid) REFERENCES public.projects(projectid) ON DELETE CASCADE;


--
-- TOC entry 2900 (class 2606 OID 16522)
-- Name: tasks taskstatusid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT taskstatusid FOREIGN KEY (taskstatusid) REFERENCES public.taskstatus(taskstatusid);


--
-- TOC entry 2902 (class 2606 OID 24778)
-- Name: usersimages usersimages_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersimages
    ADD CONSTRAINT usersimages_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid) ON DELETE CASCADE;


--
-- TOC entry 2895 (class 2606 OID 16473)
-- Name: usersprojects usersprojects_projectid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersprojects
    ADD CONSTRAINT usersprojects_projectid_fkey FOREIGN KEY (projectid) REFERENCES public.projects(projectid) ON DELETE CASCADE;


--
-- TOC entry 2896 (class 2606 OID 24788)
-- Name: usersprojects usersprojects_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersprojects
    ADD CONSTRAINT usersprojects_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid) ON DELETE CASCADE;


--
-- TOC entry 2892 (class 2606 OID 16430)
-- Name: usersroles usersroles_roleid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersroles
    ADD CONSTRAINT usersroles_roleid_fkey FOREIGN KEY (roleid) REFERENCES public.roles(roleid);


--
-- TOC entry 2893 (class 2606 OID 24773)
-- Name: usersroles usersroles_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usersroles
    ADD CONSTRAINT usersroles_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid) ON DELETE CASCADE;

INSERT INTO roles (rolename, createdate) VALUES ('admin','20191014');
INSERT INTO users (username, email, password, registerdate, isconfirmed, firstname, lastname )	VALUES('1','estimator@test.com','$2b$10$jmgCCyRq6kalEVKOwg2pD.Tx01XSMgQOyGp0P.uLE7jun.Ad1TWu','20191014',true,'Admin','Admin');
INSERT INTO usersroles (userid,roleid,createdate) VALUES (1,1,'20191014');
INSERT INTO usersimages (userid,image) VALUES (1,'iVBORw0KGgoAAAANSUhEUgAAAewAAAIACAYAAABARF8EAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAMl1SURBVHhe7b0JmCRJWT6+i8uxgLDLfYNcwgK7W1nVPT2znZnVPTs7R1dm9iw7CggoCCunPznldFVEEBTllEMREVFAUU5RQQFFEURE5BJQRO77hgV2/v94q9/oicqKzIyIiurKqor3ed5ntrfifSO+LyLjq8rKyjwtICCgnThx4sSPrK1dcp3z0uJW528cPy9KijhKtweduPjJXlrcq5fk942S7UujOH9wlBa/IPjIKM0eGyX5Lwle1k3zJ+DvblI8WmgfgTbi/z1A/PszQ48kz7tJdqjbzy7orGfndi8Y3KJ74Ylri65P3xlBQEDAQuHCC/unC15J4UQHe/ALfjaYJ7873vHHf6SXbl2/lx6/Mwolim6U5o8SxfQZoui+vBNnf99Niw900uwznbT4tvjv/282zK7oxtnXunH+CTGO93Tj4q9Ekf9DUfifhjcFw3HH2xchjtXVI9diqEOo8ZJhvVgg+AW/qUF09iNl8iUnBL/gZ4M2+p1zzomrrKxv376XbB8Vhe/nu0n+O+Lf14lC+EFRiL8zXhznn6Kof70T5+8X8b1RfKJ/geDjV9L8Ht14q3en9JJrMjXW8DEfKoJf8LNB2/2MITrCu4QzBK+sEH87vVuAjvrg5wDoqA9+DoCOemO/YWFOi/PxaROfPPEpFJ9Ih59ONUVtmYkzBlGSv1Xk6IXDMwr9fAun35nKMSDvzL/xfNQBOuqDnwOgoz74OQA66r34WQGdsMOrKMTfkwQT/IKfEaCjfs/8zl+/5Pr47jhKiseJgvynogh9oBMXPygXpkBLxtlXxb9vE3l9tijk9+/G26u93tY1mP/K+bABdNQHPwdAR33wcwB01HvxswI6Ebyq4NUU4u9Jggl+wc8I0FE/Nb/Nzf41oo3tnijMDxHF5GWdNP/4SJExoPjUvUvd67ZcLr/siijO/jNKBi/upvkDV/tFdKMbPdDp1KFufvl3OD4MAB31wc8B0FHvxc8K6IQdnqkQf08STPALfkaAjnqvfuvrh24oisRde0n+jF46+MdJvmfeKVq5Qn07Uwa/nf/fSYtviH/fIgr4k3Gmg1ey10LOr6DX9UKf4GcJ6KgPfo4w1oqGuJrt6oI4XSWJv6/EJlaAjvrg5wDoqA9+luh2L71yJ87TKMme0kuzd0Zx/gNZINTiYcvRQhP8bGnj14mzH0Zp8a4ozX8j6heHzz106Bqc3iGwLrg+Jl4vAHTUBz8HQEd98HOA0KH44yK1Zj0aCaLDayrE35MEE/yCnxGgo97Zb/gzqjR/mPj0/IZunH3TpjiYMPjN1k/M6/c7SfGPUVI8SbwBSw8e7F97kvWiAjrqg58DoKM++DlA6FCscYFac8FGA0F0di2FGMAkwUAf/BwAHfXBrwa44Uivv512k/y3RYH+H3Vzn7Q4lBn8Wun3VTH3f9ZLBveL48HNm9ZLFaDjerNaf1WAjvrg5wDoqF8WPxRrXKAG1hdsNsbHeDkAvGvFv5MEI30kg58hoKM++GmQpunVhnfuiosXd+LiizWb+Qh17UwZ/Nrvh9Pn3TR7O+721km278jl0gisM643o/XXBOioD34OgI76ZfFD/cUFavKqchRs/XfYeEEQX5DLj/bouE3BBL/gd9ra2iVnik35bmKz/nNRqL9V3sBVljdyUNfOlMFvPv2iNPtIJ8l/ta54Y51xvdWuP1NAR33wcwB01C+TH+ovuFuw+fIoxAuysuOqNvVcfEi2AaCjPvg5ADrqq/xOj5J8Y/hJOi2+rtuUyzTdzE0Z/BbDr5Nm/4Hf1vfWL74111Y43oKfFaCj3qcfzmzLq8pRsM/gy6MQL8hz5mgoC/YkX5iHyQt+xoCO+jG/89fzczpJ8RSxyX5St/lW0XUzr2LwW0w/XHXeTfJHrCVHb69bf1yiVoCO+uDnAOioXyY/WXNlwUY9rjwVjqvRUNFlwZ700va2Jyf4tdhv/8aRm4tP0/9v+AAKzSbbRF+buWTwW3y/KB78UPz7xpV+9pOHD194NtYll6gVoBOcq+ONfwc/A0BHvU8/eTZbFuzqm6yIF/ClNqq5LNhgSLYBoKM++DkAOuqHXvv7W+vddPASUaidn1ilbuKSunamDH5L6BcXnxp+371x8S25VI1QXs9ka483MvgZAjrqffuBsmCj/tZeZKYWbOfbpwldmLzgZwzooI/ji66/kmb3E4X6ncPNUrOpmrK88QY/Owa/sl92RTfJ3tTrF3fFjXe4dLWQ61nQ6/FBn+BnCeionwc//IuCjTPb1fVXvAiRLNjV58wbQJ9pBRP8LAEd9a31258eu2Ovnz29lw6+oN8s7ahuusHPnsGv0e/T3TR/zF3Wt87mMt4F1jOPB2/HB/XBzwHQUT8vfvjv5mvG0EAQBXuSR4gte7KDnwVW0u1zxeb4CvXWoAabZS1Vn+Bnz+Bn4RcX3xJr91nyCnMcBzwevBwf0FEf/BwAHfXz5Gf2ayw0Eqz+YXYDqJ+35AQ/Q0BH/cR+UT/fF6XFa8XmeNJ5s9Sw7BX87Bj8XP2yK6Ik//OVNLvQx/EBQEd98HMAdNQvpp9o6FSoAXTCztw6LwE66oOfA6CjvlV+YkO8sJMWf4dNrrxRguMboTmDX/Cz4fT8sn9Z7Q/u4XJ8SEBH/UTHmwR01Ac/B0BHfSv9rIBO2JmXzqGjPvg5ADrq2+J3utjQtoe/cR3b3E5RvubC4Bf8bLgXfvgZIh4DymPAGDiueHy5Hm8jgI764OcA6KhvpZ8V0Ak789I5dNQHPwdAR30r/Dr94mA3Lt7dtLmpr9sy+AU/G+61Xycp3hnF2xfxkKgFjiseX07HWxnQUR/8HAAd9a30swI6YWdeOoeO+uDnAOion7lfL8k74hPG36gbF9i0udky+AU/G87SrxPn/4Db6fIQGQOOKx5f1sebDtBRH/wcAB31rfSzAjphZ146h4764OcA6KifqR+ulo2S4k/EBnWyvGHZbG4mDH7Bz4Zt8cM1HJ1kez8PmSFwXPH4sjreqgAd9cHPAdBR30o/K6ATdualc+ioD34OgI76mfmdu//4DcRm9JxOmn2/vEGBrptbFYNf8LNhG/2iOH/5anr8ZjiueHwZH291gI764OcA6KhvpZ8V0Ak789I5dNQHPwdAR/1M/HDHpyjNH9WNs2/qNiXQx+amMvgFPxu22Q+33e2l2ZP7/cM3MjnemgAd9UbHbxOgoz74OQA66r34AUJr9msudMLOvHQOHfXBzwHQUT8TP7Hh9AU/oG5AZfrc3MDgF/xsOC9+4k3v//WS7D51x1sToKPe6PhtAnTUBz8HQEe9Lz956/BmPRoJ4g4svjoPkzenflF8/MadNPtj3QakUt3UJHXtTBn8gp8N59Mv+wdcsMlDzRg4Tnm8Nh6/JoCO+uDnAOio9+WHYo07kDYXbDQQxL1N1QG0KZjgtwd+aZqe0U3zh3XS4uu6DUilfjPStzVh8At+Npxvv+yKTpw/78CB/Ed56NUCxymP19rj1xTQUR/8HAAd9b78UKzl8z3qCzYb46kh+HQtB9GmYILfHvh1+9kFYjN536lNpZr1m5E9g1/ws+Gi+HXS7JO9ZPsoD0EtcJzyeK09fk0BHfXBzwHQUe/LD/UXT83E0zNlwdZ/h40XBPE8Tny6lgW7TcEEvyn7ra1dcmY3yX97+GhBzaZSpulmZMrgF/xsuIh+UVr8YXgq2NL6of6CuwWbL49CvCAr+5mCsmCbPUVEA+gE256c4Kf4RWm21kmKD+s2ER1dNqM6Br/gZ8NF9uvExWc7SXYxD82wXy2HH85so/7Kgn0GXx6FeEGeM0dDWbCbn89ZAegE256c4Ee/2x45ctUozZ/aibMf6jYPHSfZjHQMfsHPhsviFyXFq9Y2Bzfm8ao9fm0BHfXBzwHQUe/TT9ZcWbBRjytPheNqNFR0WbBR6UOyDQAd9XPpF8WDSGwK/6nbLKroazOSDH7Bz4bL5helg6+s9gc/rTt+bQEd9dr9wBbQUR/8HACdoDybLQs2znRXFmt8qY1qLgs2GJJtAOionzu/nSvAi1/uxMUPyhtEHX1vRsEv+Nlwmf1W0uwP0vTIDXD88hC3AnQ8/sf2AzaxAnTUBz8HQEc9KAs26m/tRWZqwa6u7A0QujB5c+KH2yNGafYO3QZRR3XzkNS1M2XwC342DH45Hijy/k6yfUce5sbAcc/jf2w/YBMrQEd98HMAdNRLHxRsnNmurr/iRYhkwa4+Z94A+kwrmOBnCeioH/PDz0Y6cfYl3QZRR93moWtnyuAX/GwY/E75DW9vmuT35eHeCBz3PP7H9gM2sQJ01Ac/B0BHverVfM0YGgiiYOM77FCsDQAd9XPll+eDK3eT4tfFAT/2VK0m1m0eLgx+wc+GwU/vh7sPNt1sRRz7Yf9rv5/Zr7HQSLD6h9kNoH7ekrN0ft1k+6ZRkr9Vd9A30XTzMGXwC342DH4NfnHxUVw4yi1gBOLYD/vfIvmJhk6FGkAn7Myt8xKgoz74OQA66kf8xAF/UTfNPq892BtY3jhAXTtTBr/gZ8PgZ+gXF9+L4uynuRUMIY79sP8tsJ8V0Ak789I5dNQHPwdAR/2IXzfJn2B6x7IynTePCga/4GfD4OfglxRPO+20y7AXhP1vgf2sgE7YmZfOoaM++DkAOup3/Q4cOng9cQA3Pl2ril42D4XBL/jZMPhN4pe9bm3zGG60MrX9hX8HPwNAR70XPyugE3bmpXPoqA9+DoCO+l2/ffGxW7v8ZEvS7+YR/IKfHYPf5H5ROvjA2ubgztwTWr1f8e/g5xvohJ156Rw66oOfA6Cjftdv/+ax1W6c/Y/uQDZheeMAde1MGfyCnw2Dn0+/7IurG8cuwj7BLcMK0AnO1f7HvxfSzwrohJ156Rw66oOfA6CjftdvX5rd1eS51VUcPdh3qGtnyuAX/GwY/Kbhl11evhjNBLr9hX+H/dkA0FHvxc8K6ISdeekcOuqDnwOgo37Xr5cOHm3z4I4y9Qe7vq0Jg1/ws2Hwm65flBa/xu2jEbr9hX+H/dkA0FHvxQ8QWrNfc6ETdualc+ioD34OgI76oVe/v35WN81/Vz04bdl0sNsy+AU/Gwa/PfN7jthCajf+8v5Ctnb/IxfZT946vFmPRoK4A4uvzsPkefQ7eLB/3W6S/YnmwDSmxcFuxOAX/GwY/PbWL4rzl544ceJHuKWMoLy/kK3d/8hF9kOxxh1Imws2Ggji3qbqANoUzFL7HTi0eYNukr9ed1Ca0vZgb2LwC342DH6z8esk+V/g+ffcWoYo7y9ka/c/cpH9UKzl8z3qCzYb46kh+HQtB9GmYJbab//m0Zv2kszpNqOSrgd7FYNf8LNh8JutXxQXf3vuoUPX0O0vZGv3P3KR/VB/8dRMPD1TFmz9Vxl4QRDP48Sna1mw2xTMUvut9S+6ZS/O36U7CE056cFeZvALfjYMfu3ww70aon52HXV/IVu7/5GL7of6C+4WbL48CvGCrOxnCsqCbfYUEQ2gE2x7cubGb/3gkduJg/P9uoPPlOUDHdS1M2XwC342DH7t8uvGg//Ylxy7rdxjuN+E/dkA0FHv0w9ntlF/ZcE+gy+PQrwgz5mjoSzYzc/nrAB0gm1Pztz47U+O3Fl8sv649qAzpO+DPfgFPxsGv7b6DT58wcbhW3O/CfuzAaCj3qefrLmyYKMeV54Kx9VoqOiyYKPSh2QbADrqp+K3lhy9QxQP/lt30JlSPcglde1MGfyCnw2DX8v9ksF719YOn8UtyArl/Yps7X5Kts1Pns2WBRtnuiuLNb7URjWXBRsMyTYAdNRPxW9/unUbvAPWHXSmHDs4BXXtTBn8gp8Ng9+8+GVvX1u75ExuRUYo71dka/dTso1+oCzYqL+1F5mpBbu6sjdA6MLkefQ7sHn4FviOqXzA2bD64HRj8At+Ngx+8+UXpfkbu91Lr8wtqRbl/Yps7X5KttUP/6Jg48x2df0VL0IkC3b1OfMG0GdawSyd3/CnW+FqcCsGv+Bnw+BX5Ze/As/U5takRXm/Ilu7n5Jt9sN/N18zhgaCKNj4DjsUawNAR/1U/Pr9wzfqJtk/6A8mM5ofnGYMfsHPhsFvvv06SfYibk9jKO9XZGv3U7Ltfma/xkIjweofZjeA+nlLTmv94vii63fT7G90B5EpbQ/OJga/4GfD4LcYfp00/01uU7so71dka/dTcnH8REOnQg2gE3bm1nkJ0FG/lH5xfPhs8cn6L3UHjyldD84qBr/gZ8Pgt1h+UVI8jttV2O9b5mcFdMLOvHQOHfVL67eS5s/THTSmnPTgLDP4BT8bBr+F9DvZTfMTuv2Kf4f93gDQUe/FzwrohJ156Rw66pfWr5cMHqE5WIzp6eDcZfALfjYMfovr10mLb6+lW+vqfiXY6v2Ufy+knxXQCTvz0jl01C+t32qaXdKJsx/qDhYT+jw4weAX/GwY/BbfL0oHn7pg46Lbc89q9X7KvxfSzwrohJ156Rw66pfWbzU5tl+8g/2G7kAxYfnABHXtTBn8gp8Ng98S+SX5uw4cOng97GPc0qwAneBc7c/8uxV+VkAn7MxL59BRv7R++/tbt+uk2Sd1B4oJRw4mUtfOlMEv+Nkw+C2fX5QWL+WWZgXd/se/Q/3wDXTCzrx0Dh31S+sXx4du2E2yf9UdKCbUHUy6dqYMfsHPhsFvef2iNH8UtzYj6PY//h3qhyGE1uzXXOiEnXnpHDrql9Zvff3IWZ0kd/75Vt3B5MLgF/xsGPyW3S+7IurnW9ziaqHb//h3qB8GEDp56/BmPRoJ4g4svjoPkyf+Fov+V0YPAHM2H0x2DH7Bz4bBL/iBnbT4eifZviO3Oi3EXhf2+8n8UKxxB9Lmgo0Ggri3qTqANgUzl37dePsI3qHqDoImmh5Mpgx+wc+GwS/4qezE+fvTNL0at7wRiL0u7PeT+aFYy+d71BdsNsZTQ/DpWg6iTcHMpd++dPsWnbj4om7xN9H2YGpi8At+Ngx+wU9HUbSfx21vF2KvC/v9ZH6ov3hqJp6eKQu2/jtsvCCI53Hi07Us2G0KZi79NjbSq0Rp9g7dom+i68FUxeAX/GwY/IJfA7e5/YX93o8f6i+4W7D58ijEC7KynykoC7bZU0Q0gE6w7cnZE78oKZ6hWeiN9HAwjTD4BT8bBr/g18ROnH3pvM2tm2Kf4343tv9xi7QCdNQvkx/ObKP+yoJ9Bl8ehXhBnjNHQ1mwm5/PWQHoBNuenD3x6yTZxbqF3kQfB5PK4Bf8bBj8gp8pRdF+K379otv/uEVaATrql8lP1lxZsFGPK0+F42o0VHRZsFHpQ7INAB31Y37RxvZtunH2Nd0ir6PPgwkMfsHPhsEv+NkQ+pV08KTy/sct0grQUT+2n7KJFaCjvs1+8my2LNg4011ZrPGlNqq5LNhgSLYBoKN+zA9XUEZx9l7dAq9j+UACde1MGfyCnw2DX/CzofSI4vwHq/1jF8r9j1ukFaCjfmw/ZRMrQEd92/1AWbBRf2svMlMLdnVlb4DQLWuytX5RWvyWboHXUT2IJHXtTBn8gp8Ng1/ws+GYXzz435ULLz57uEFaAvsm90/tfmoL6KifBz/8i4KNM9vV9Ve8CJEs2NXnzBtAn2kFM3d+nThPbH9vPbb4BXXtTBn8gp8Ng1/ws2GVn/ik/fLhJmkB7JvcP7X7qS2go35e/PDfzdeMoYEgCja+ww7F2gDQUa/1Oyc9cc1Omn+8vMDrWLX4XRn8gp8Ng1/ws2GTX5RuD4abpQGwb3L/1O6ntoCO+nnyM/s1FhoJVv8wuwHUz1typuoXpdnz1cXbxKbFb8vgF/xsGPyCnw1N/Dpp8b/nHjp0DW6JlcC+yf2zcj+1AXTUL6afaOhUqAF0ws7cOi8BOurn1i/qF4fLi7eOJovfhsEv+Nkw+AU/G9r44d4T3Ba1wL7J/bNyP7UBdNQvhZ8V0Ak789I5dNTPrd95aXFWNy4+pVu8OtosfhMGv+Bnw+AX/Gxo69eJsx9GG9tdbo8jwL7J/bNyP7UBdNQvhZ8V0Ak789I5dNTPtV83yf9It3B1tF38TQx+wc+GwS/42dDVr5MW/3bixImRW2ti3+T+WbufmgI66pfCzwrohJ156Rw66ufaL0ry47oFq6Pr4q9i8At+Ngx+wc+Gk/pFSfEIbpOhfkzoZwV0ws68dA4d9XPt173wxLU7cfE53WItc9LFX2bwC342DH7Bz4Ze/OLiW52Ni2+JfZP7Z+1+agroqF8KPyugE3bmpXPoqJ97vyjOn6VdqCV6WfwKg1/ws2HwC3429OkXpflfcf9s3E9NAB31S+FnBXTCzrx0Dh31c++3khbn4+IK3SJV6XPxg8Ev+Nkw+AU/G07FL8nu07SfmgA66hv3ZxNAR30r/QChNfs1FzphZ146h476RfA7vZvm/6RboCrLCx/UtTNl8At+Ngx+wc+G0/KL0sFnL7hw8yY1+2kjoKPeZH9uBHTUt9VP3jq8WY9GgrgDi6/OFyrZvSS/r26BqlQXvaSunSmDX/CzYfALfjacvl/25Kr9tAnQcT822p+bAB31bfVDscYdSJsLNhoI4t6m6gDaFMxM/e6yvnW2+HT9Bd0ilRxfrOFgt2HwC342DH5z4Bdn3zx3//EbcBs1BvZh7sdG+3MToKO+rX4o1vL5HvUFm43x1BB8upaDaFMwM/eLkuJ3dYtUUrtYNe1MGfyCnw2DX/Cz4V76deL8edxGjYB9mPux8f5cB+iob6sf6i+emomnZ8qCrf8OGy8I4nmc+HQtC3abgpm5XyfNe3VP4qpbrC4MfsHPhsEv+Nlwr/06cfGDlfXt23M7rQX2Ye7HxvtzHaCjvs1+qL/gbsHmy6MQL8jKfqagLNhmTxHRADrBtifH1u/0Tpr9i7oAVTYtVlsGv+Bnw+AX/Gw4Q79Xcz+tBPZh7sc2+3MloKO+zX44s436Kwv2GXx5FOIFec4cDWXBbn4+ZwWgE2x7cqz9Oun2PTWLb0iLxWrE4Bf8bBj8gp8NZ+3Xi4sD3FbHgH2Y+7HV/lwF6Khvs5+subJgox5XngrH1Wio6LJgo9IvcnKs/brdS68cpcV/axef5WJtYvALfjYMfsHPhm3wi9LsHdxaR4B9mPux1f5cBeiob7OfPJstCzbOdFcWa3ypjWouCza4yMlx8uum2UN0C89lsdYx+AU/Gwa/4GfDNvl1kuxibq9DYB/mfmy9P+sAHfVt9wNlwUb9rb3ITC3Y1ZW9AUK3sMnGw9h19wufZLHqGPyCnw2DX/CzYdv8xKfsj6RpOvyeFvsw92Pr/VkH6KifBz/8i4KNM9vV9Ve8CJEs2NXnzBtAn2kFM3O/KC2eWF5sky7WMoNf8LNh8At+NmytX1z8HPZh7sdO+3MZ0FE/L3747+ZrxtBAEAUb32GHYq3BysHt63bS4uvqIvO2WMngF/xsGPyCnw3b7NdJ848fPnzh2a77cxnQUT9Pfma/xkIjweofZjeA+nlLjpWf+HT9W+oC87lYweAX/GwY/IKfDefBb7U/uK/r/qwCOuqd93sV0FHfDj/R0KlQA+iEnbl1XgJ01LfGrxcXN+/GxffUxVWmuvhsGfyCnw2DX/Cz4bz4dZPBf7jszyqgo955v1cBHfWt9LMCOmFnXjqHjvpW+Yli/eLy4lKpLj5bBr/gZ8PgF/xsOHd+/a2LbfdnCei4v0+030tAR30r/ayATtiZl86ho75Vfp00u6181vXY4hIsL0AbBr/gZ8PgF/xsOI9+UZK/jVuvFbCvc3+faL+XgI76VvpZAZ2wMy+dQ0d96/yiNHt+1eIqL0AbBr/gZ8PgF/xsOM9+Ys9d4/ZrBOzr3N8n3u8B6KhvpZ8V0Ak789I5dNS3zm/fZnbDTlx8t25xuTD4BT8bBr/gZ8N594uS4i+5BTcC+zr394n3ewA66lvpZwV0ws68dA4d9a306yTFU5sWly2DX/CzYfALfjZcEL+TnWT7jtyGK4F9nfu7l/0eOupb6WcFdMLOvHQOHfWt9FtbO3xWLx583WBxGbPsFfzsGPyCnw2D31z7vYRbsRbY17m/e9nvoaO+lX6A0Jr9mgudsDMvnUNHfWv9ev3BEywWVyPLXsHPjsEv+Nkw+M23XyfNvo+f03JLHgH2Z+7v3vZ76tvqJ28d3qxHI0HcgcVX561P9vr6RdcVC+qzpouriapP8LNn8At+Ngx+i+GHm1VxW94F9mfu7972e+rb6odijTuQNhdsNBDEvU3VAbQpmKn4rSTZQ2wXVxVVn+Bnz+AX/GwY/BbHT3zK/vJtjxy5Krfnuakf9PHhh2Itn+9RX7DZGE8NwadrOYg2BTMVvzg+fHaU5P9lu7h0VBdp8LNn8At+Ngx+i+cXJcU91P1Z0Ot+T582+qH+4qmZeHqmLNj677DxgiCex4lP17JgtymYqfn10q17ui4uleoiDX72DH7Bz4bBbzH9xIent6r7s8JW1g+Fk/qh/oK7BZsvj0K8ICv7mYKyYJs9RUQD6ATbnpxdP7Gw3uG6uCTVRSqpa2fK4Bf8bBj8gp8N2+63PxlEcn8mW1s/yEn9cGYb9VcW7OGzwscgXpDnzNFQFuzm53NWADrBtidn1281ObZv0sVVXqjz7Zddof//p2jn18zgF/xsuOB+J8v/b0K/Mc6D30qaPVPu0dyvF7keyZorCzbqceWpcFyNhoouCzYq/SInZ8Svm+a/q1s0piwvVFDXzpQz9UvyP4iS7Z/oxMUPtK8LWvkZMPgFPxsusl8nLb7RifOkkxT/KP/fJH46zo9f9sU4vuj63K8XuR7Js9myYONMd2WxxpfaqOayYIOLnJwRvzQ9coNunH1Vt3BMqC5SSV07U87ST2wU7+92B1dHrnr94q74TWS5jY2fCYNf8LPhQvvF2Td7cXEAx99qevxm4k3zFyfy03De/Fb6g3tj30ZObAGd4DzUI1AWbNTf2ovM1IJdXdkbIHTzkpwRvygt7qNbOCYsLy5Q186UM/UTm0U3HdyB6RpC/P9ttWhb+Rkw+AU/Gy6yHz5Zd9PtdR56w/1qZWNwXHicdPHTUfWZF78oLt7MlFgB+cP+zn1+d78XbGM9wr8o2DizXV1/xYsQyYJdfc68AfSZVjBT9eskxTt1i6eJusWla2fK2ftt353pGkEvyfNuml1u71fP4Bf8bLjQfnH21aif7+MhN7Jf9frZ0639NFTHNWd+J3vrF9+aqTGCmj+FU6kfbGIF6KhXvZqvGUMDQRRsfIe9dMX6/I3j56mLxpQ1i8uJs/aLkuJ3ma4xIE/inf6JXpx/z9SviapP8LNn8Fscv+Fp7yTv8HAb268OHz54nSjN36bTmnKS8em4535J8etMTyPK+SOnUj/YxArQUa/6mf0aC40Eq3+Y3QDq5y05u34oVCMLw4CNi8uSs/brpMV71LsKqUCemK9rryTZ8V48+FaTXxPLYwt+dgx+C+QXF59a2cjuxMOtcr9aTY/eRBT2z2o9GjjR+DSchR9iT9NU/xMnBSJXc12PGiEaOhVqAJ2wM7fOS4CO+j3xOyc9cc2d741GF0cdTRaXDWfuh4tcKk43IU/M127+VjcHSTfNPq/1MqD1+BoY/IKfDdvkF6XZRzobF9+Sh1vjfhXF2abQjf3cq46TjE/HWfp1kiIbJqoCyBPzpc2fLaCjvpV+VkAn7MxL59BRv2d+UbJ9qW5hVNFmcZmwDX5Rkv880zEC5In5GstftLF9G/HJ4KM6vzq6jK+OwS/42bBVfkn2r+evX3J9Hm7G+5847l6o9dNwovFpOHO/JP8jpmEMyBPzVZs/U0BHfSv9rIBO2JmXzqGjfk/9orR4l3ZhaGi9uBrYCr+4+OfTTrtsLMfIE/NVmT9sNkufvxoGv+BXxU6aveHcQ4euwUPJav/rXnji2sLj02XPMicZn46t8Iuzr51zzomrMBW7QJ6Yr8b8mQA66lvpZwV0ws68dA4d9XvqJw6a22oXhYZOi6uG7fDLLj9/PT+H6dgF8sR81eYPwKYjiv5f6f1P0W181Qx+wc+G7fLLX3DixInde0PjuOLx1Xi8SQif7XHfU5xsfONsk1+Ubg+YhiGQJ+bLOH91gI76VvpZAZ2wMy+dQ0f9nvuJT4dP1C2IMidZXDq2yO9XmIpdIE/MV2P+JHAhSN1pugnGp2XwC342bJHfyW5S/CIPmyFwXPH4Mj7eJITfn5f8h5xgfFq20O8lTMFC1SPvQCfszEvn0FE/Ez8x8R8oLYQxelhcI2yPX/bB8qkl5In5MspfGVFSPKh8VzT38ekZ/IKfDVvjhws7kzznoTIEjiseX27HW3z8xvjtttqP8/gq2Eo/xNy9FPcLWah65BXohJ156Rw66mfi11nPztUuBoVeFpfCFvmdlLc+lECemC+j/FWh199Ou2n+BfQzwfi0DH7Bz4at8Yuz/xFvZu/CQ2QIHFc8viY63qI0v7/sx3l8FWyzXy/Z3mK+JsqfBHTUt9IPEFqzX3OhE3bmpXPoqJ+ZXycpnqJbCJI+FxfYJr9OnD+PaRgCeWK+jPNXh9761q3ERvI+1/HpWPYKfnYMfrPxi+Lib1cObl+Xh8YQOK54fPk43k4X/bzddXxVbL1fkv+Rp/zNQ32Ttw5v1qORIO7A4qvzViQH73p1CwH0vrha5NdJi69308H1mIapzUeSHLrxSjr4c9vx6ajGGfzsGfxm4yfetD5VvbgMkMeHoLfjDfdFEGNaqnuNR+ngKwcP9q/rI3/Ue5sP6n35oVjjDqTNBRsNBHFvU3UAbQrGya+TbO/XLQSwvLBAXTtTts0vSorHMQ17Mh8r/cFjcTW6biwmnDTeMoNf8LOhk1+cfa2TZBfzsNiF7vjg3xMfb70ke5Xx+Gqoxtl2v5WNwcW+8qewTX4o1vL5HvUFm43x1BB8upaDaFMwzn5RnD+raTFI6tqZsoV+n5aPzUSemC/r/OkAHfVjfp0073Xi7GOa8dTSQ7wjDH7Bz4ZOfnHxbt1dA3Ec8HgYOz7YxArQUT/0uiA9em4vHTi/MQad4q3htP266eAlvvJHepsPchI/1F88NRNPz5QFW/8dNl4QxPM48elaFuw2BePsh1NUuvvxlhfDzoIYbWPDNvpFaXE/5AB5Yr6s86cDdNRX+h04kP+oGMPLymOqoo94VQa/4GdDF79OWjxzljf1EGN4ZnlMpnSJt4574Sc+BHzJ5N7iZYhc7fn+ZwPoBFF/wd2CzZdHIV6Qlf1MQVmwzZ4iogF0gq1JTqdfHDRZDOU2Nmyp3wfwZgV5Yr6c8lcGdNQb+Yk3DT8jPoV8SzO+XXqKd5fBL/jZ0N4v+3w3KY5xiY8AxwGPB6PjownQUT/mh2tTcDpeP8Zq2sdbz730E/99IVNjBOSJ+RrLH5tYATrqffrhzDbqryzY+jcl4gV5zhwNZcFufj5nBaATbFVyyk/mKi8EUH3dlm31w03zkSfmyzl/KqCj3sqvt5H/eBRn79WN01e8ksEv+NnQ2i8u/mrfZnZDLu0R4Djg8WB1fFQBOuor/aI0e6x2nBW0jreBe+2HsxoMvRHIE/NVmT8bQEe9Tz9Zc2XBRj2uPBWOq9FQ0WXBRqVvUzAT+3XS7JOmi8GW7fXL3o48MV8T5U8COuqd/HDjgyjJf0m90Yq/eHcY/IKfDa384uybeHAQl/MYcBzweHA6PsqAjvpav7W1S84UbyI+pR1ziVbxGnA2ftkHGXotkCfmqzZ/poCOep9+8my2LNg4011ZrHG6FNVcFmywTcFM7IebF8iJNlsM5my1X5KnzNdE+ZOAjvqJ/YZzkmT/6jVeweAX/Gxo49eJs78/Ly1uxSU8BhwHPB4mPj4A6Kg38uum+QN041ZpE68JZ+l33ubWTRm6FsgT82WUvyZAR71vP1AWbNTf2ovM1IJdXdkbIHStTY5YyI/BBNssBhO22S9Ks3cwXxPnD4COem9+Bw/2z+71t36lF+ffmzReUM1b8LNn8NP7De9hEBc/J5Zt5d6I9czjwdvxQb2xX5qmVxt+r66JATSN15Sz9ovi7KcZ+hiQJ+bLOH91gI76afjhXxRsnNmurr/iRYhkwa4+Z94A+kwrmIn9xCL+B9vF0MS2+61ubN3NV/6go34qfqvJsX1Rkr9bF4cpfecv+AU/MEqKv+wcyG/CpatFeT2TMzneukn+eF0cpvGasiV+L2PYI0CemC/r/OkAHfXT8sN/N18zhgaCKNj4Dnshi/X58bHrRvHghw6LoZJlr7b5RXH+kX5//Swf+YOOei/zAR31I363uc29z4jS7Qd20uzLupjq6Dt/wS/4iU/V/yv+3eayrYRYu63a/+6yvnU2vmdXYzGJ14Zt8evExedEyCN1C3livpzyVwZ01E/Tz+zXWGgkiIK9kMUautX+4D4ui6GKZa82+q2mgwf7yh/13uaD+ko/3H85SrPnd9PsCl18ZZZzB+ramTL4LbcfLobErUXljYbqgHXL9Vu5nm0AHfUT+UVJ8QzTeG3ZNj88zIlht3Y+JKCj3s1PNHQq1AA6YWdunZcAHfVe/VbS7BWui6FM1afFfp+N44uu7yt/9PE2H/Rp9IviQYTv4XVxSqp5k9S1M2XwW24/UajfiJ8ecgnWAuuW69doPTcBOuon9ltNj98Mbzya4rVlG/3Em5NHIGbkifmaOH8AdNS30s8K6ISdeekcOuq9+sXx4bOjOP+S62JQqS6qVvv1B0/0lT/6eJsP+tj4nd5N8nsv6x3qVAa/6fmJ9fWhXrJ9lGuuEVi3XL+261kL6Kj35ifi++OqeF1Y9mqNX5K9CfEyX97yR30r/ayATtiZl86ho96730o6ODTRYiDVRdVqv3jw9SS58Ga+8qdwpn7npCeu2U3zJ8i7Oal5kyznxIbBbzn9dt4I5g+wuc0l1i3Xr/N6VgEd9V799m8eXRNx7j7Jq5wTG0oPlbp2pvTp10mLbx84dPB6zJu3/NGndX5WQCfszEvn0FE/Fb9emj1tksUAlhdWu/2y5zL+hVys+/dffD0R429H6eC7aty6vJhS9Ql+9pxHP/xMK0qLJ5576NA1uLSMgHXL9etlPUNH/VT8esngzYhXlxdTlnPXRr/VdGsgY2b8C7n/WQGdsDMvnUNH/dT8xILV3gbTlOWFBeramXLafmv9oz3kgSmxAnSCczG/a8nRO4h4f18U7t27pbmwnD9Q186Uwa/lfnH+7U5SPHVt7ZLrcFkZQ11/Clt5fEiubmT31OXFlGP5E9S1M+W0/FbS7LcYc6vng387+VkBnbAzL51DR/3U/C64cOtmplca66guKkldO1NO2y+Ks39CHpgSK0AnOFfzi7+7G8XtRC5eot7m1JTl/IG6dqYMfi32E4VabOrPjOKLtff+boJYa3N5fKyt3eOqdTdSqeNI/khdO1NO1S/J/43xL+T+ZwV0ws68dA4d9VP16ybFQDfRJhxZDKSunSn3xC/JK+/6UweRq7mcX8Fdv15c3FzE/9tNTwOT1OZP086Uwa+lfvHga9108BsXHDx6K3W92AA6rrfK9WcD6KjfEz+xDz5Nl6M6qvMgqWtnyun7Da6I+pn1WRMAeWK+tPmzBXTUe/EDhNbs11zohJ156Rw66qfuN/wtpWaymzi+GOZgc4uzr+Lm/0yLMZAn5mssf2xiBeion4kfTnXiu8lOXHxRlztQmz9NO1MGv/b5dePBp3v97AmLeAGmCuior/RbWd++vS5PVSzPxTCfmnam3Cu/KN6+iCEbA3livirzZwPoqPflJ28d3qxHI0HcgcVX53uanG6a/1N5sptYtRhcuYd+zxkmxQLIE/OlzZ8toKN+5n548xLF+YM7af5xw/w5Mfi1y0+8SX/fSjK4f79/VF41vBDrWQfoqG/0i5L8rbp8lel7PvbWL38CwzUC8sR8NebPBNBR78sPxRp3IG0u2GggiHubqgNoUzC1ftiwbb/XrF8M9txLv/M3jp83TIwhkCfmS5s/W0BHfcv8LruSWAdHoqR4zbLdnnZ5/LLLxRy/fF8yODT5etkBdNQvhJ948/pT+tydor/52OHe+2WvZbiNQJ6YL6P8NQE66n35oVjL53vUF2w2xlND8OlaDqJNwTT6iXeUG6OTWc/mxWDHvfSL0uJdDNsIyBPzVZk/G0BHfav99h08fMdeOnjK8HSpkj8Xluci+NnRi19cfDRKs8eubQ5uzPXhdb3QZyH8dp7iVXxlLIekl/lQOAs//Kae4dYCeWK+jPNXB+io9+WH+ounZuLpmbJg67/DxguCeB4nPl3Lgt2mYIz8RMG+rDyhVTRZDDbca7+6h+mXgTwxX7X5MwV01M+N3+HDF57dS/KikxSvFwf5D9RcmrBpPmwZ/Mz9xCfp74h/X4Y35GJ6sVct/Xrm341+UVI8u5xPcJL50HGmfhcMbsFwtUCemC/r/OkAHfU+/VB/wd2CzZdHIV6Qlf1MQVmwzZ4iogF0gjNJjpi8t4xNpoZWi8GAe+2HgmP6m1LkiflqzJ8JoKN+bv266eB6eEJYN83eLvJ5Us2tjk3zYcvgZ+KXXdFN8jfj2ccHDuQ/yqkL69nSr9vPLijn1m0+qjl7v/wShjsG5In5cspfGdBR79MPZ7ZRf2XB1t+BT7yAYo1qjoayYDc/n7MC0AnOJDnd7qVXxu3q9BN6ivaLoZ4z8UuyNzHsWiBPzFdj/kwAHfUL44efhkVp8Uixdt4zlmdBo/mwYPCr9TvZifN/EP8+VPcsaswj59N4fusAHfWL7He6eNPzfzLHlvPRyFb4JfnTGesIkCfma5L87QI66n36yZorCzbqceWpcFyNhoouCzYq/Vwu1ijN1rSTqdBpMdRwZn5x8bMMuxLIE/NllL8mQEf9wvoNfwqTFL+IT944i2E8H4YMfjo/XDxW/M3w6n5NkZbAPHI+nedXBXTUL7yfyPPvINdm82HOFvm9jaHuAnlivibOHwAd9T795NlsWbBxpruyWONLbVRzWbDBuV2sYqN9tGYidznBYtByVn4mp8ORJ+bLOH91gI76pfHbv3Hk5r0ku4/gn/bS7ItV82HK8twus18nzT4j/n1JlGz/xOrqkWsx7ZXAfHA+vc0v9UvhtxLn63Xz4cJW+cXFt06cOLH7nS/yxHx5yR901Pv2A2XBRv2tvchMLdjVlb0BQteK5ERp8RrtZApOtBg0nKlfw+lw5In5sspfFaCjfmn91tePnNXtZ/tF/n9ZfBr8O14EpZ8fDa3m14Dz5hel+TfFpvom8e+jOuvZuUy1EXTzwb/DejYAdP3++rXlryQkdfNmStWnLX5RUtxFxst8ecsf9dPww78o2DizXV1/xYsQyYJdfc68AfSZVjBWfmJD+JRuIn0sBpUz96s5HY48MV/W+dMBOuqDnwJcL4GvYPDdN94oigL+Ze1cCVrPbwPnw2/wBfHvG1bS7JdWN7Y2NzbSqzB1VkDemf/a+TAFdNQvnZ+Yi+f5m99R6tqZ0puf2BfVeBW2cj5I/HfzNWNoIIiCje+w575Yn7v/+A10k+htMZCz9qs7HY48MV/W+dMBOuqDXzNOX9nI7oRNI4rzZ4m5ehtuG2s7v01so9/wkZVJ/lbxCee3e8ngfmubW+c75G8M0FHvMh9jgI76pfRbSwcXucyvyvJaaZdf/kI1XrK180Ga/RoLjQSrf5jdAOpbk5yoXxwuT6DfxdASv4rT4cgT8+WUvzKgoz74OQA66Nc2B3felw7uvtIf/Fo3yf9SzOEHbE+nSzqtlxpa+8XFt6I4e694Q/JyUZwf1+1nRbSxfRsRbvhd9Bz44bS4erW4La3XSwN9+3XjwX+o8Qq2ej74t5mfaOhUqAF0ws7cOi8BOuqd/XDnI3XyfC+GtvjpbpaCPDFfzvlTAR31wc8B0FFf6beSHrtRJ9nev3PryPwJUVr8fifO/l4UxY/ik7mY65Hfhruulypq/US/naT48PC3z2I8uAlRJ83v04nzJIqP35hDHwPiYnyV8doAOuqDnwOgo37Mj2d/tGuijtr1omlnymn4idh+0O8fvpEaL1NiBeioH8sfm1gBOuq9+FkBnbAzL51DR/1EfmLSXqlOXpnyNRe2yq90Rx/kifmaKH8S0FEf/BwAHfUT+aVpesa+zeyGqxuDu+zbyI4K3mslyX4B3wn3+tmviYL6ZDw+sZMWzxRvVp8vCv2LxSf3PxZr5JXi0++fiH9fJviSKM1/D3e6Ev/+Bgqw0D2q1x88dLU/uPfKxiBfTY8m+9Njd7zz4Yusn/gGIC7GN1G8EtBRH/wcAB31Wj+xZo7t7iWGnGi/0nCafljPary2gI56bf5sAR31XvysgE7YmZfOoaN+Yj/x6eRj5cmTLE+wDdvll32Q4Q6BPDFfE+cPgI764OcA6KgPfg6Ajvrg5wDoqK/063YHV+/ExXf1+8s4J9uvxjl1v2RwPzVeG0DHfFXmzwbQUe/FzwrohJ156Rw66if2w+84xeSdHJs8wfIE27BtfuKT0jMYcusXF3TUBz8HQEd98HMAdNQHvxJwkxrd/lLmpPtVmXvhJ/bIX2OYVkCemK/G/JkAOuq9+FkBnbAzL51DR70Xv5V+saGbPN0km7KNfvJB7cgT8+Ulf9BRH/wcAB31wc8B0FEf/BwAHfVGft0kf7huf1HpY79SuVd+UZL/GcM0BvLEfBnlrwnQUe/FzwrohJ156Rw66r359frZY3ST58qyVxv8xLvib9/2yBHc3Kb180F98HMAdNQHPwdAR33wq8D56/k5uj1G0sd+pXIv/cQn7P9kmEZAnpgv4/zVATrqvfhZAZ2wMy+dQ0e9V7+dW0eOT54LVZ82+UVp/kYZr6DX/NEn+FkCOuqDnwOgoz74OQA66q39xAeA/9XtM772K8m998suV29RWgfkifmyzp8O0FHvxQ8QWrNfc6ETdualc+io9+4XJdkH9ZNnR3URtM0vSvL/J+NV2Mr5UBj8DAEd9cHPAdBRH/wM0I2LF5b3GJ/7FTgrv/OTrdsxzEogT8yXU/7KgI56X37y1uHNejQSxB1YfHU+teRsbvZviN/fVU2eKcsLoW1++5NBhHgVtnI+FAY/Q0BHffBzAHTUBz9DdJLsYnV/8b1fzdKvl+Q5w9QCeWK+nPOnAjrqffmhWOMOpM0FGw0EcW9TdQBtCmbEb2Uzu6Bu8kxYXght84vS/P9kvGRr54MMfoaAjvrg5wDoqA9+FuheeOLa3TS7AvuL7/1q5n5J8WiGOQbkifmaKH8S0FHvyw/FWj7fo75gszGeGoJP13IQbQpmzG+lP7i3dtIMab0YGjgNv246eKUSc6vng38HPwNAR33wcwB01Ac/B3ST/N/Le9XOfqPfi0zYCr8k/wOGOALkifnykj/oqPflh/qLC4vx9ExZsPXfYeMFQTyPE5+uZcFuUzBavygpHq+dNAM6LYYaTs2vnz9cxivY6vkQDH4GgI764OcA6KgPfg6AbiXNX6juVaBuHzJl2WtmfnHxzwxzF4iX+fKWP+p9+qH+grsFmy+PQrwgK/uZgrJgmz1FRAPoBPckOWKCXjI2YQZ0XgwVnKbfvv7WARkvU2IF6Kgfyx+bWAE66oOfA6CjPvg5ADrqg58DoIN+tT+4r7rP6PYhU6o+M/eLs68y1CFkvIJe80cfX344s436Kwv2GXx5FOIFec4cDWXBbn4+ZwWgE9yz5HTT/J+0k1bDiRaDhlP1iwdfw1N2ZLy2gI750ubPFtBRH/wcAB31wc8B0FEf/BwAHfXX3nfw8B2971ct8cPDdcrxKmzbfMiaKws26nHlqXBcjYaKLgs2Kv3cLNZOXHxRN2FV9LEYVE7bL0oHf63GawPomK/K/NkAOuqDnwOgoz74OQA66oOfA6CjftevF2ef1O1DpizvV6CunSl9+UVJvqGLl3+3aT7k2WxZsHGmu7JY40ttVHNZsMG5Wax3Wd86WzdZVfS1GCT3wi9Ks8cxXCsgT8xXZf5sAB31wc8B0FEf/BwAHfXBzwHQUT/iJ/YdPOFNux81Ubdf6dqZ0qdflG4/SBevYNvmA5QFG/W39iIztWBXV/YGCN1MFmvUz/fpJktHn4sB3Cu/KClihmsM5In5qs2fKaCjPvg5ADrqg58DoKM++DkAOurH/Lpp/oDyXmTCqv3KlVPwe7YuXqbECtBRPw0//IuCjTPb1fVXvAiRLNjV58wbQJ9pBVPr10m376mbrDLLCwHUtTPlnvnFxfdw/3CGawTkiflqzJ8JoKM++DkAOuqDnwOgoz74OQA66rV+vfT4ncv7URMr9ytHTscve7UuXltAR702f7aAjnrVq/maMTQQRMHGd9hzV6yBTpL/qm7CVJYXAqhrZ8q99cv/iaEaAXlivozy1wToqA9+DoCO+uDnAOioD34OgI76Or/TcVW1uifVsX6/sue0/Lrp4J0V8RoDOurr8mcM6KhX/cx+jYVGgtU/zG4A9dMOptavm2Z/qps0SXURnJpIfVsT7rVflBTPZqiNQJ6YL+P81QE66oOfA6CjPvg5ADrqg58DoKO+0a+TFn+n7jtVbNqvbDlNP94dci7nQwvR0KlQA+iEnbl1XgJ01Fv5iYX2b7qJA9XJk9S1M+Us/Dppfh+GWgvkifmyyl8VoKM++DkAOuqDnwOgoz74OQA66o38xD7zm+W9p0yT/cqG0/aL0sH3zzvvzkZP7SoDeWK+jPLXBOio9+JnBXTCzrx0Dh311n6iYH/DZPJAXTtTzspvJS3OZ6iVQJ6YL+v86QAd9cHPAdBRH/wcAB31wc8B0FFv7BfF+U/p9h9J0/3KlHvlJ3+LbQPkifkyzl8doKPei58V0Ak789I5dNRb+62tXXKd8sSBVZPnytn5ZZd3u5demeFqgTwxX9b50wE66oOfA6CjPvg5ADrqg58DoKPeyu/89fwc/R403/tpJ817DNEIyBPzZZW/KkBHvRc/K6ATdualc+iod/LTXd1YN3kunKVfJy3ew1C1QJ6YL6f8lQEd9cHPAdBRH/wcAB31wc8B0FHv4HfZlcR+8+3yHmSzX5lwz/36WcEAG4E8MV8O+RsHdNR78bMCOmFnXjqHjnpnv6hfHFYnp3HyLDlrv06SvYihjgF5Yr6c86cCOuqDnwOgoz74OQA66oOfA6Cj3tkPD8xQ9x/b/aqJs/CL4vzBDK8WyBPz5Zw/FdBR78XPCuiEnXnpHDrqJ/LDBVlyYkwmz4Zt8IuS4kEMdQTIE/M1Uf4koKM++DkAOuqDnwOgoz74OQA66ify66b5c+Xe47Jf1XFWfp2keArDqwTyxHxNlD8J6Kj34mcFdMLOvHQOHfUT+4kF9gRMiunkmbItfp1kez9D3QXyxHxNnD8AOuqDnwOgoz74OQA66oOfA6CjfmK/KC3uh33Hdb+q4kz9kvyPGJ4WyBPzNXH+AOio9+IHCK3Zr7nQCTvz0jl01HvxE59An281eQZsj192Rbc7uDpDHQJ5Yr685A866oOfA6CjPvg5ADrqg58DoKPei1833uq571d6ztoPvy9neGNAnpgvL/mDjnpffvLW4c16NBLEHVh8de49OeIT9htsJq+JZa+Z+sXFRxnqEIiX+fKWP+qDnwOgoz74OQA66oOfA6Cj3ptfmh65QS8dXOG0X2mo+szQ778Y4ggQL/PlLX/U+/JDscYdSJsLNhoI4t6m6gDaFMzQr5cM3ms5eZVUfVrhl+SvY7hTyx99gp8loKM++DkAOuqDnwOgo967Xy8e/K/TflWiuu/N0g9XvjPMXajxKmzTfKBYy+d71BdsNsZTQ/DpWg6iTcHs+kVx/jmbyauiugha45fkTy/Hq7CV86Ew+BkCOuqDnwOgoz74OQA66odevST/W6f9SmF575u1Hx7BzHDnYT5Qf/HUTDw9UxZs/XfYeEEQz+PEp2tZsNsUzK7fwYP968rTN7pJMmV5IbTGLy5+Vo1XYSvnQ2HwMwR01Ac/B0BHffBzAHTU7/qtpNnuleIu9Lb/kT78oqS4S1W8/LtN84H6C+4WbL48CvGCrOxnCsqCbfYUEQ2gE5xaci7Y2DrHZfJUlhdCm/x6/SxW4yW95Y8MfoaAjvrg5wDoqA9+DoCO+qn6iQ8KTs/GBn3uf6AvvyjevkjENg/zgTPbqL+yYJ/Bl0chXpDnzNFQFuzm53NWADrBqSZndWNrUzc5pvS1GCR9+8Xx1i3VeAW95o9/Bz8DQEd98HMAdNQHPwdAR/3U/Xr97VS3HzXR9/7n068TF3djfGPxMiVWgI56n36y5sqCjXpceSocV6OhosuCjUrf6sUVJfnFuskxoc/FAPr2i+L8S+V4BVs9H4LBzwDQUR/8HAAd9cHPAdBRr/Xbt5ndULcn1dH3/ufbbyXJ/l9VvLaAjnqffvJstizYONNdWazxpTaquSzYYOsXV5RuP1A3OU30vRim4SfejMgHr08tf2xiBeioD34OgI764OcA6KgPfg6Ajvpav26cfVW3N+lY3vtAXTtTTsNvJc1+qS5eU0BHfW3+TAEd9aAs2Ki/tReZqQW7urI3QOj2dHF1k/zxugmqY3khgLp2ppyeX/bScry2gI56bf5sAR31wc8B0FEf/BwAHfXBzwHQUd/o10mKd+r2pzLVfU9S186U0/ITBfu36uI1AXTUN+bPBNBRL31QsHFmu7r+ihchkgW7+px5A+gzrWC0fvjZk26SqqguAkldO1NO1a8/eGI5XhtAR31l/mwAHfXBzwHQUR/8HAAd9cHPAdBRb+Qn9qOXlPenMkf2K1LXzpTT9Osm2e/VxdsE6Kg3yl8ToKNe9Wq+ZgwNBFGw8R323BRrAE+y0k2Ujurk7U6ipp0pp+230h/cqxyvKaBjvmrzZwroqA9+DoCO+uDnAOioD34OgI56Y78oyS/T7VOS5f0K1LUz5bT9RMF+VV28dYCO+TLOXx2go171M/s1FhoJVv8wuwHUTzsYrV+UFK/STVaZ5ckbTqCmnSn3wi9Ks1WGaQXkiflqzJ8JoKM++DkAOuqDnwOgoz74OQA66q38orT4Gd1eBer2K107U+6FXyfN3sjQrIA8MV9W+asCdNS7+YmGToUaQCfszK3zEqCj3sgviou/1U2YSt3k6dqZcq/8zl+/5PoM0xjIE/NllL8mQEd98HMAdNQHPwdAR33wcwB01Fv7iU/YG+W9Cqzar1y5V37iA9A7GJoxkCfmyzp/OkBHvRc/K6ATdualc+ioN/brxsW7yxOmsmryXLlXfrp73zYBeWK+jPNXB+ioD34OgI764OcA6KgPfg6Ajnonv976xbc23a9cucd+H2BoRkCemC+n/JUBHfVe/KyATtiZl86ho97KrxNnH1MmZIQNk2fNvfTrxMWHGKIRkCfmyyp/VYCO+uDnAOioD34OgI764OcA6Kh39ut2L73y8PG+BvuVC2fg92mG1gjkiflyzp8K6Kj34mcFdMLOvHQOHfXWfqJgf6k0KUMaTJ4V99wvyd7EEBuBPDFf1vnTATrqg58DoKM++DkAOuqDnwOgo35iv25cfMpov7LkTPzi4lsMqxbIE/M1cf4A6Kj34mcFdMLOvHQOHfVOfuKT6A/KE2M0eRacjV/+AoZYC+SJ+XLKXxnQUR/8HAAd9cHPAdBRH/wcAB31Xvzwva/ZfmXOWfqlaaq/LzeBPDFfXvIHHfVe/KyATtiZl86ho97J75z0xDXLE2IzeSaclV+UFI9jmJVAnpgvp/yVAR31wc8B0FEf/BwAHfXBzwHQUe/Nr5sOXmmyX5my7LX3foPrMbwxIF7my1v+qPfiZwV0ws68dA4d9c5+q+nxm6mTYT959ZylnyjY92CYWiBPzJdz/lRAR33wcwB01Ac/B0BHffBzAHTUe/XD3cFM9isTqj6z8os2tm/DEEcg4xX0mj/6TOwHCK3Zr7nQCTvz0jl01E/k10uP31lOhMvk1XHWfqJgxwxzDMgT8zVR/iSgoz74OQA66oOfA6CjPvg5ADrqvfv1+vnDTfarJqr73iz9RMHuMsxdqPEqbNN8yFuHN+vRSBB3YPHVubdguun2OibBdfKq2Aa/lfXt2zPMESBPzNfE+QOgoz74OQA66oOfA6CjPvg5ADrqp+K3kg5+ymS/qmN575ulX6dfHGSoQ5TjJds0HyjWuANpc8FGA0Hc21QdQCuC6V544tqdpHjqJJOnY1v8VlePXIuh7gJ5Yr4mzh8AHfXBzwHQUR/8HAAd9cHPAdBRPzW/fcngkG5/MqXr/lfFif2S/Olra5dcpype/t2W+UCxls/3qC/YbIynhuDTtRzETIPBFX5Ruj0QiX9lJy6+O/HkldgWP8TGkHeBPDFfzvlTAR31wc8B0FEf/BwAHfXBzwHQUT9Vv+5GcTvdHmVC1/2vir78Omn2fdymdCXNfzqOL7q+Gq9gW+YD9RdPzcTTM2XB1n+HjRcE8TxOfLqWBXtmwQwvLhs+lSv7vEy6r8mTbJVfnH+CoQ+BPDFfTvkrAzrqg58DoKM++DkAOuqDnwOgo37qfrpf4phwov1Pw+n5ZV9cSbNn7k+OnIN4mRIrQMd8jeWPTawAnSDqL7hbsPnyKMQLsrKfKSgLttlTRDSATtApmG46uEM3Ll6Md0T6ZJ+i+rot2+Yn4v0XpmBaiyH4BT8jQEd98HMAdNTPrR9uOKLbp6o46f5X5l747dzPI/vTXpJ3GLYRkCfmqzJ/NoBOEGe2UX9lwdb/dly8IM+Zo6Es2M3P56wAdILWwSBpIrGvVm+LV5fschsbttMvey3ygDwxX1b5qwJ01Ac/B0BHffBzAHTUBz8HQEf9nvp10vzj+n1qnH72v1OchV+UFq8xKdzIE/NVmz9TQCcoa64s2KjHlafCcTUaKros2Kj0e7a49m1mN8QnapG0k+UkgibJtmFb/fCMb+SJ+TLOXx2goz74OQA66oOfA6CjPvg5ADrq99yvm+b/pNunyvS1/0nO2O9kN8n/qHvB4BZMwwiQJ+arMX8mgE5Qns2WBRtnuiuLNb7URjWXBRvck8V1zjknrtJNikd30uIbmsQNaZnsRrbZT7zDezLzZZS/JkBHffBzAHTUBz8HQEd98HMAdNTPxK+T5H+h26dU+tz/wLb44QLgKMl/6bZHjlyV6ZjmfICyYKP+1l5kphbs6sreAKGzCiaKs03xqfqjumRJuia7im33W+lvPdI0f02Ajvrg5wDoqA9+DoCO+uDnAOion5lflBS/q9unJH3vf630EzUKzwdHnpgv4/zVATrqpQ8KNs5sV9df8SJEsmBXnzNvAH2MgknT9GrdJP9tkQzt6W9JL8lWOA9+q/3BfZvyZwLoqG+cDxNAR33wcwB01Ac/B0BHffBzAHTUW/mJgv0k3V4Flvc+UNfOlC33O7mSZi+64MLNm9jkrwrQUa96NV8zhgaCKNj4DnvqxXolLc4Xi+A/NQkZoedkz43fvjS7pC5/JoCO+sb5MAF01Ac/B0BHffBzAHTUBz8HQEe9tV+UFo+s269U6tqZco78Pra6OUhN86cDdNSr82H2ayw0Eqz+YXYDqDdaDKJQP6KbZpfrEqJSSc4ude1MOU9+K+ngUFX+TAAd9Y3zYQLoqA9+DoCO+uDnAOioD34OgI56Jz9RsO9Xt19JltvYcP78BqKG5Q9niqyAvDP/TvMBA6dCDaATdlbb+fAUeFq8rJwIHceTs1yLYbU/6JXzZwromP/a+TAFdNQHPwdAR33wcwB01Ac/B0BHvbOfKEyX1O1XoPq6LefbL/vTcw8dugZT1Qjknfl3ng9noBN2Vtt5FB+/MW4GoiahivXJsedc+iXbN2XqrIC8M/+182EK6KgPfg6Ajvrg5wDoqA9+DoCO+on8xJ50Ye1+pexntlwQv/f14uLmTFclkHfmf6L5cAI6YWe1nXfSvNeNi09pghyjYXKMOa9+a2uXnMn0GQN5Z/5r58MU0FEf/BwAHfXBzwHQUR/8HAAd9RP79ZLBSt1+5cpF8hMfSD8TxYOIKRsD8s78Tzwf1kAn7Ky2czzKrJMW39YFWKZNckw4t35x8T2mzxjIO/NfOx+mgI764OcA6KgPfg6Ajvrg5wDoqPfi10mO/XjlfuXIhfSLs2+WH98JIO/Mv5f5sAI6YWe1nYt3HEfwo3NtYCU6JaeG8+2XfZ4pNALyzvzXzocpoKM++DkAOuqDnwOgoz74OQA66r35raWDW1fvV/Ysey2Un/jA1e1nBdPnfT6sgE7YWW3nnaTIhgPXBVTiRMnRcAH8/otpbATyzvzXzocpoKM++DkAOuqDnwOgoz74OQA66r369ftHr1ezX1lR9VlUPzxIJEry4zJ/gl7mwwrohJ3Vdt5JsovLT9eqoo/kqFwEvygt3sVU1gJ5Z/5r58MU0FEf/BwAHfXBzwHQUR/8HAAd9VPxi9L8O7r9yoblvW+R/VAD96WDn5T5I53nAxBas19zoRN2Vtu5GGjf5DfWoM/kgAvk9xamsxLIO/NfOx+mgI764OcA6KgPfg6Ajvrg5wDoqJ+aXy8dfEGzVxnTYv8z4lz4xfn3VvuDLeZwkvmQtw5v1qORIO7Asjt5gmOdn7+en9ONs6/qBl9mOTGgrp0pF8mvkxSvZ0q1QN6Z/9r5MAV01Ac/B0BHffBzAHTUBz8HQEf9VP26cf4J3X5lQpv9z4Tz5Bel+TfW+oMLRA5d5wPFGncgbS7YaCCIe5uqEzi2GHZ+Z138r27wZarBSOramXLR/KIk/zOmdQzIO/M/cjAJtvpgFwx+BoCO+uDnAOioD34OgI76MT/xQeLDuv2qibb7XxPn0i8uPtU5kN+EaTaGyDuKtXy+R33BZmM8NQSfruUkji2Gc9IT1xTF+j26wZepDUbTzpQL6Zfkf8TUjgB5V+Zh5GBiEytAR33wcwB01Ac/B0BHffBzAHTU74lfFGfv1e5XNXTa/2o4135J9q/d7uDqw2QbQOQd9RdPzcTTM2XB1n+HjRcE8TxOfLqWBVu7GLpp/gp10FWsDcaBC+sXFy9kaneBvDP/2oPJFtBRH/wcAB31wc8B0FEf/BwAHfV75if2+X/S7lcVdN7/KrgIfp00+2OmsxbIuyDqL7hbsPnyKMQLsrKfKSgLtvYpImISH1AelI4mwdhwkf06afFMpncI5F1wTw9OG0BHffBzAHTUBz8HQEd98HMAdNTX+om96S3lvaqKk+x/Oi6WX/YQplQL5F0QZ7ZRf2XBPoMvj0K8IM+Zo6Es2Nrnc3bWs3NNboxiF0wzF90vSvOnMsULc7CbAjrqg58DoKM++DkAOuqDXwm4GFa3X5U56f5X5qL54edeuF030zoC5F1Q1lxZsFGPK0+F42o0VHRZsFHpxyYPTycxuQjBNpgmLoNflOSXIcfIu+BMDk4TQEd98HMAdNQHPwdAR33wcwB01Bv5RUnxKt1+pdLH/qdyUf2iNPtI+QlfyLugPJstCzbOdFcWa3ypjWouCzaon7y0+H3dQFS6BlPFpfFLil9E3gVndnA2ATrqg58DoKM++DkAOuqDnwOgo97YDxfDavcr0tv+Ry68n3KtEvLO/IOyYKP+1l5kphbsysqOm5trB6Bw4mBKXCY/8Qn7/4ncWx1MdYCO+qXxu2DjotuuJNnxXj9/+Ep/8Ju9dPBi8Sbzpdh0ojT/vU6a/6b4+5G9fnHXbjq4w4kTJ/QXdAjAj+PxNj7qg58FMEeYKzFnl/T62eO7/ezZvTR76UqavaKb5i8Xn1peIP59cpRuP7CbZIdW0mM3orQWGAfHM9H4JKCjfqH8UGB0+xXoc/8Dl8VP6C5E3pl/OQ8o2DizrS/WgHgRIlmwK8+Zp2l6NTFxH9V1LlkOBNS1M+Xy+WUPF/m3OpiqAB31C+23unrkWt10++6iIL8sSgefsp4PPGUnzf9a6B/eW7/41rRdmvxJQEd9K/yije3bREnxiE5a/A3mCHNVntu6+cW9IaI4f6loc7cDB/Ifpe0uMA6Ox2l8ZUBH/cL5deL8ebocl+cC1LUz5VL5xdn/HLhw80aludBeMzYCNBBEwcZ32JWVvZsUv67tmPQajOAy+olPhr8g5sDqYNIBOuql16L5nS4281hsyC8XbyKHD5opzwVYzrER4+KfRQG/7+Zm//oTjG8E0FEf/GqADwW9JL9vJyneWZ6XSeYXF8jiZzXijd266AZnFMN8WPiJY+3Z5ZxOMh86LqPfSpo9U5kP7a+xxoBGgtU/zBbopcfvXPdQj3IgoK6dKZfWrz/4eTEPVgdTGdBRLxfCIvmd3ku2j4q1+C/a/ClUX7fljsfgC71+9oQkOXRji/GNATrqXeIdA3TUL4wfLsIRHwge3U1z7T2ry3ML6toZUbwhW9kYHO/3188yHV8doKPeON46QEd9a/zwc1M1h17nQ3BZ/aJ08P3V5Ng+q/kQDavPlxNRkr9V1zGoBiGpa2fKZfZbTQcPtj2YVEBHvfPBqQI66mfu10m279hN8jfX5U+y3MaGZS+cZo+S7Z8UQ2g8TspAXIzPOl4doKN+UfxOH36dERef0s0FWJ4PUNfOlKc8srfs3zzWaxhfLaCj3jTeWkBHfav8xCfsZ5Tzp1LNry2X3i/J3mo7H7UQn2aO6DoGxzoX1LUz5dL79bMHuk4edDwYJzo4JaCjfqZ+uOAoSrPH6p4EN5Y/wXIbG9b5RWnxmnP3H78Bh9UIxMX4rOKtAnTUL4QfLgxr+n1v3Xy4cMwvzr+HX2bUXXhYBcTF+IzibQJ01LfODxdqavMnWM6xDYPfjh/OGjLVE+P0Tlr8W7njYScVnbsy+IHFvZl3K+Ag5ME48cEJQEf9TP1QIMX6+zvT/OnamdLErxMXn+v1t1MOrxKIi/FZxVsF6KhfCL8ozjarTn9LmsyHDWv9kvzN569fcn0OrxGIi/EZxdsE6KhvpV+U5k+rzZ8Dg98pv06cv/+00y5zmpsRRMn2T6idStZ17sLgt+MXxflPMfXGwEHIg9HLwQkd9TP127luQv8UuKr8udLGrxNnPxSftu/HYY4BcTE+q3irAB31C+G3c0vj7ApdbiVt5sOERn5x/gk8JpjDrATiYnxG8TYBOupb6yfm7Lca82fBslfww096i3sw5W5I0/QM3JWlbGzSuQ2D3yk/8d93Y/qNgIOJB6O3g5P6mfp14+3Vquerl3MH6tqZ0tUPp+k53F0gLsZnFW8VoKN+IfzEG50n6nKp0nU+qmjp95VeMljhcMeAuBifUbxNgI76Vvv1+tnTDfPXSNUn+J0i7h460afsXlrcq2xq2rkpZ+0nPsF9u5NmnxHv+D8o/n5b+Z2/rV8Tm/xwRoPpb4Q8mAS9Hpz0mZkf7rUr5uXral4km/Jny8n98sdw2As7H1WAjnojP5GrJ+hzeIqTz8confzi7GtRPIg47F0gLsZnFG8ToKO+9X7ddPAbxvmroToPNn44q9WJix+U/7+rXxVn7Sdet/rANgKxaY4857rcMai+bss998MntuEt9rKHdPvZBXiWN0PdhVgYXzL2s6SZX34Jh1IL9WBS2MqDXWGjn3jzdFtxYH5xPC/tXX/iDcZ9EBfjs4q3CtBRvxB+3bj4OV3uVPqaD8nJ/PIvhJvonPJb6WdPtcvfOMtzYeeXvV0M63RcqCj+7ot9/BGCr+/Fg2+5+Y1T9ZmVH2ruzgyMQ8xD9a9UcIMB1cil8zruld/w+8Y4fzmuwut2L70yw6uE2Hw/XufnSlM/Uazw86FalA8msrUHO9nohzuWifg/pMuLaf5M6dMP7/xX+9lFtvFWATrqF8IvSvINHIe63En6nA/Qk98H8KYecTE+o3ibAB31c+MnCuZvaPJjzMnnI3sthzcyvgOHNm+w2h/co5dkfyXanRzXmXHy8Y1yEj9ckMlQhxBxyluHV89npDydZZLOddwLv+Hpk7h4IW5vyJCMIN7h/JvOT9evKW38mi46w6QJTvXg5N977Yc7l/2JLic2+TPhNPyidPDZCzYO39oiXi2go942f1pAR/1M/HCVv9hsP6/Lm2R5LkBdO1P69BPH4x8xPqN4mwAd9XPl102Kp+jyY0If8yHm4aV14xO80sr69u1F25cJWhVuH+NTOamf2Af/cjgZAiIuFGvcgbS6YHcvGNxCviOetPMy98IPd8HC87oZjjGQkCjN31b20/VryrJXo1+SV/6sC+MTnPrBKbjnfiIvd9Plo5w7UNfOlFP2+wvTeHWAjnrr/OkAHfWz8sObsL/U5U2ylL8hde1MOQ0/8Qnu3obx1gI66k3zVwvoqJ+6n9gXnT5hl+cC1LVrolhHz8Y4OJ6x8TGEIXDBqviU+l6dT5m+xifpww+1txcXNxdxoVjL53tUF+wO30356FzltP2iePBD3ATB5Uo7JEPwWuLTwOtVT12/plR9TP1wH2UOaQRyfIK1i9UU0FE/c7+1tUuuo/ve2iV/ddwLP7GxFAzLCsgT82WdPx2go35mfrgeQ5c3SV3+dO1MOS0/8enu8+sXHsMGOtfzUQfoqNf6RWnxW7oc1VGdB0ldOxN2kvxJHI92fGXs/MKp+DWxn1f+fNDn+ECffowXT83E0zNlwdZ+h326+IT6yZ4Q+eocLHt594sHXxPvrI4wBiuIROwu1pUkf5n01PVrypGxWfhFyfalHNYu1PEprFysTYCO+lb4dUr3KQZd81fFvfIT744/dtsjR67K0IyAPDFfTvkrAzrqZ+Yn3oSdKeZV+xt6sCp/rpy2n9j4n1UXbx2gY76M81cH6KjfMz+Rk98p56iO5fzt5FDf1oQr/cFj68ZXhaifb4l1+I2yn+/xefeLs/9dX99/pohxt2AzpFF00+14KPDZeclrCn6fWdnI7sgQrIBJF9xdrL1+9hx46vo1ZWlsQ+ra6Sg+oT2IQxuiPD7SaLHqAB31rfDrbFx8S/EGceShMpPkT8e99sMzmRleI5An5sspf2VAR/1M/cSnm19Qc6KyKX+23BO/uPjeanr8ZgzPGMgT82WVvypAR/2e+om8PKecpypq86dpZ0roV/rZg+rGV4dekndw1b/qV6bany2n5SdixoWsKNhnMJRRiBdOFw2fV/50rTM1peozDT+crvJVrEGRpCfp+jVleXygrl0VoyT/eQ5vYQ72OuC7KTX+SfNX5iz88MkSp+QYYiWQJ+bLOX8qoKN+pn7nnHPiKjv3NhjNC2iSPxvusd/vMEQjIE/Ml1X+qgAd9XvuJ47T31XyUMmG/FlTeqxubN2jbnxNEDXiTvjaTR2XpK5fU07TL0oGLxax4tO1/udcW1tHr9pNsy+oBVtnakq182n4RWn+zZV02/riMgCTzskfWawi/ofq+jZheXygrl0tk/zhdeMTnLuDvQrdC09cWxS3b8vYveRP4Wz98hMMUwvkiflyzp8K6KifuR9+6aDLiV3+mrnnfnH2zQMH8h9lmLVAnpgv6/zpAB31M/HrJNmLRnKhYWP+LKn6rCRbx+rG1wTo9qVH+6JmfEf11fVrStVnGn5RXHzp+PHtyk/XP9JLBsOncu0UbL2pKcudg7p2ptT44dL94xy+FTB5nPyxxSreSd6j3LcJNePTtmuiWFCPwjg4nrHxMQQrQEd9q/xw6ljG7St/krP266T5XzPMMSBPzNdE+ZOAjvpW+In431bOh23+mjgrv7p7yEsgT8yXU/7KgI76mfl14+LFunxImubPlGWvtX6GZ0ZPHC+u+Jeeun5NqY5tOn47/7/TLw4yjFMQgQx/mC0aPa9s5MLxzqeRnPzXOXwrqJOncHexdmoeJVpF/fj0bZso3jA8nuPRjs8W0FHfOr9umr0dMfvMH9gOv+wK3aM4kSfma+L8AdBR3wo/fM9bzoVb/qo5S79OWvwdQ9UCeWK+nPJXBnTUz9Rv5w6R+pzY5M+EOr/O5rGbcyhWQFyMbzfelTR7rq5fU+rGp2tnyjq/KM6fxVBOgUHhmcP/pRq5sK5zF+r88Bs7k+8Iy2CctYt1+NAJzTiqqBufrp0phf5pdeOzAXTUt84PjzIU8Z4s5w4s58SGrfKLi59luEMgT8zXxPkDoKO+NX64aFLNwUT503D2ftkV+Bkiwx0B8sR8OedPBXTUz9xPxP7n47nYu/nArw44FGMgLsY3Em+nU1xNfDD7j3LfJqwanysN/P6L4ZyCCOJKq0l+q1JDaxp0bkWdH+5ghqv+OHRjIEZMFidtd/IERxarmMjb6saio258unamHHr0s+fUjc8U0FFfG68poKPei1833b5HOXegLi+mbKHfKxlu6+cDOuon8ovS4jUyfg/5G2Fb/HQP6EGemK+J8icBHfWt8IvS/I3lPLjmr4pVfmJP/g6HYQzExfi08eIBQ3jzVR5DHavG50pTv7G7dSIIMSE/q2tsStPOTVnlJ97BP4PDNgbi42RpJ0/FXda3zi6PRceq8bly1yPJfq9ufCaAjvrGeE0AHfXe/FbS/IVq7oaxa/JiyrJXG/xwpbSMl/nylj/q2+Z3eocPz/GRP5Vt8iufpkSemK9J8zcEdNS3xi9K8reqOZgkfzrW+UVx9n8chhEQF+Orjdf0ynewbnwutPETtfn+HPIORCC4Wcof6xqb0KZzE1b5ddLi6ysHt6/LYRsBk8TJqp08Bdh0ZvagAlHIXt4wvlpAR71pvLWAjnqvfr00+xc1bl1eTKn6tM1vbXNwY+bLa/7o0yo/3NIYMfvMH9g2v06c/wNDnpvjjT7OflFavEvGP2n+yjTwex+H0QjExfga48XXcrqbqpRpMD4rOvi9jEM+hU5cfFbTsJEOndeyzk8smidyuEbAJHGyGidPhe42mZJ143PhuF/26qbxVQE6xmcVbxWgo96rX7+/fpZ41/gNGbMuL6YczV37/FaSrQHz5i1/9GmdXy/Z3vKdv5b6fQXxIk/Ml5f8QUd96/zEm5T3I3ZP+duliV/ThX4SiIvxGccrPmXX3nfDZHw2dPEbO8OAu03pGjbRpfM61vrF2Tfx+EUOuRGYJE6W8eRJdJLiw+q4JGvH50C9X/Y6DsMKiIvxWcerA3TUe/db6190y1Px6nNjQjVvbfVbSbKHIGaylfOhcCK/Xj9/mO/8tdVv5cKLz2a+vOWP+lb64bHDPvMHWvjtXgtSBcTF+KzixRnbblx8S9OnzfiMOInfeZtbN+WQTzut288KXaM6TtK5js1++XM53EZgkjhZVpMnIQr2P472vYfxJvmbOQxjIC7G5xRvGdBRPxW/1f7W6m68jiznrr1+2a8y7tbOBzmx30o/+w3/+Wun3/5kEDFv3vJHn1b6RXH+OZ/5K3vV+YlP98/jULTA+BifU7y677JtxmdCD37bHO7wgoJf0jSopIfOR2jmN7gDh1sLTBIny2nyADGBI48ENBufOev8ojR7B4dhBMTF+JzjVQEd9VPzW0mzC9V82LIufy6cpp+IFQ+NaPV88O+J/cQb/+f7zl9b/VY3tg4iZrKV86FwYr9ePPi6z/yVqWun8Fc4nDHI8Qk6xyv2+7uo/TmMr5Y+/KK0+DUOd/gbu1eXG1TRR+cqTfzEJ953cqi1wCRxspwnD1Bvw2cyPhs2+eE35hxGIxAX45soXgnoqJ+q30q/2FBjtmFT/mw5db8ke76IudXzIejFT8T7+4hZlxdTjuSO1LUz5bT8eKvMVs8H6cWvlw4u95k/lbp2o8wewiGNQB2fQqd4u0n2r+jLbXzV9OeXvZZD3fl+Qt9olP4636G5X/4wDrUSmCROlofJK34d/ZqPz4xGfnHxUQ6jFoiL8U0cLwAd9VP366bb62NxG9AofxbcE78kt3pghATyxHyN5Y9NrAAd9VPzw0/1dHkxpTZ/mnamnKbfvo1jhxl/6483NrECdNRf+8ChzRv4zp+NXycufpLD2oU6PoXO8YpP2Y90HV8Vffrhsb0ittNPw83sxf/Afbm1DSV9dg5a+J1seqwdJomT5WXyxBuEh1uMz4imfrhCncOoBOJifF7ihY76PfGLNra7utjraJo/U+6dX/7kYVIsgDwxX9r82QI66qfqJ96c/GY5L6aszp8bp+23lm6ti5jn4nizBXTUD732Jcdu6zt/Nn5RnG1yaEOUx0dOFO/+JDvXdXw6lr0m9cNNXg4cOH710zrr2bn6Bqfou3MbP/ycgHnVAsnmZHmbvF4yuJ/p+ExY9qrz66TZ9zkULTA+xuctXur3zO+8tLC6q55N/ky4p358+popkCfmqzJ/NoCO+qn7iTcnj1HzYsra/DlwL/x661u3YkqsgDwxX2P5YxMrQEf91PxW+0c6uryYUpc/Xbsq4jtmDm+q8fbSwYddxlemGqcPP8nOen6n06J+vqV7UdJ357Z+nbR4JnM7BjXZCieevJUkO246viaqPqZ+VffNleMT9BovffbM77ZHjlxVxNl4Vgd0yV8d996v/jGbKpAn5qs2f6aAjvo98euk2/ccjb2Zzfmz4974ZVd0u5deeZgUCyBPzJc2f7aAjvqp+kVpZvV8BZX6/OnbVnHfZnbDuvEJeom3l2QvcBmfynKsk/qpFLV6gMem/ZzuRdB3505+/axgfkdQTjbpZfL297fWjcdXQzVOG7+V9NiNOKxdqONT6CVehXvm142z/9HFrtI1f1WchZ/pfe+RJ+bLKH9NgI76PfMTG8q+cvx1NMmfDffKD9f8MGRjIE/MV2X+bAAd9VP3E3E7/aqjnDtQ166e2RUnTpz4EYyD4xkbH0OwAnTU7/qt9Af31o/BjH7irWP2UHzZrr3Ty84zsf11XvYy9escyG/CHO9Cl2z+7WXy9veP/bjp+KpYjtXGr7eR/ziHNkR5fKS3eMk99cNVj7rYJSfJn46z8MMtbtM0vRpDrgTyxHwZ568O0FG/p37npCeuKeJe+DMnnST/C4ZsBOSJ+arNnymgo35P/Hr94q5qbkxYlz875l/AODge7fhsAR31I3770u3hrXVd6C/eHe7U39H/10mKp+A5p3+g/k9Jv52Pepn66S7AEomd+uQdPNjHA0CMNh4dXePdZby9yuHt+cFpC+iot/IT6+7x2tgFJ85fiTPzi4t3M9xKIE/Ml1X+qgAd9TPxk7ewrKNx/gy5535J8YsMtxHIE/NllL8mQEf9nvn1kvy+I/E3sDF/FsR64ngqx2cD6KjX+tXdlrqKPuMFT/mUXhO1+rQoLv525H8Klj9dl1+3oerj4PeWYZYJJJXJ1SbbFtBRP+bnMnHghPHuMMkONY1vGIAloKN+5n7dfnaBLnYv+VM4U78kfzrD1QJ5Yr6s86cDdNTPzC9Kimdrc0Fa5c+As/DDqX+GWwvkifkyzl8doKN+T/3Ep9yHlXNQRZP82VDo39Y0PlNAR32lXzfN3q4bRxXLsYK6dqYc9Rp9DY843f3BuEq1YJdfs+Fo5w5+cfFC5nHPF6vJJ4UyJ453l/klGAfHox2fLaCjvhV+aZqe0UmzL6tx+8vfDmftFyX5BsMdA/LEfDnlrwzoqJ+pn5jTI7pcgLb5a+Is/PBGHt+pMtxKIE/Ml1X+qgAd9XvuJ+L+5XIedDTJnw2HHkn2qqbxmQA66mvjjdLi93Vj0bEcK6hrZ8omr+ENxMR/fKD8wk7BHv1/tix3rhtAE3HLVCQRSWVya5NtCuior/TTnXmoo494JcU7qUs5nsrx2QA66lvlJ+L8PRmzz/yBs/fLPo83JQx1BMgT8zVR/iSgo37mfuecc+IqIv6vlPNhn796zsovSrPnM9RKIE/Ml3X+dICO+pn44Zc6ulyoNM2fKU/5ZM9tGl8ToKO+MV7T23SrcUrq2ply3G+8jXiz+CG8o/jv8guTcrxzt2Dw3QmSyuQ2JtsE0FFf69dN8j/SjUlHX/FKikX6K03jMwV01LfOL0qKeCde3/mbvZ84rn6LYY4AeWK+Js4fAB31rfHrpvlz1Vy45K+Os/QTBXuNYWqBPDFfzvlTAR31M/OL4vzlulxI2uTPhCNe/cETm8ZXB+ioN4o3SrYv1Y1J5cj4SF07U1r4fRpfsn9O84IzLTpvZj/bZnKNkt0E6Khv9OsmxdO0YyrRa7yCOx677yr3LF4TQEe9F7/zzrvzj0Rx9gH/+Zu9X/lKfwB5Yr685A866lvl10uP33k3D475q+JM/ZL830V4p+9EOQ7kifmaKH8S0FE/Uz/xCftvtPkQtMqfAcf8+vn9m8ZXBegYn3G8TVfEj41PUNfOlFZ+cfZVTMbXtS860KpzA64kw4JtnOw6QEe9kZ/JhRa+45UeK2n2iqbxNQE66o3ibQJ01Hv1W0m2Hug7f7P2i5LiLxnmLmS8gl7zR5/W+XXj4k2u+avirP3EvN6D4Y0BeWK+vOQPOupn7ocHEunyYZu/Jur8on4xvADXFoiL8VnFG8XbF+nGBurGp2tnSmu/OPsmLhL5vvZFS1p33kDoV9PBUZtkVwE66o0nT/R/N924JNU4JXXtTDnilQze3DS+OkBHvXG8dYCOeu9+Bw/2ryti/pjX/JG6dqacxK98sxQ1XoWtnA+FE/ntS7IN1/zpWPbaaz98f1h1sRniZb685Y/6VviJN1+fKufDNn9NrPLrJNt35DCMgbgYn3W8os++Oi7JqvG50skvLr6HATr/3ljSqfMaSo+VdHDIJtk6QEe91eR14jzRjQ1U45TUtTPlmF8yeG/T+KoAHeOzircK0FE/Nb/VfnZ3XV5MOZY/QV07U07o9xKGOoQuXv698PMrjt8/d8jfGNV5mJlfUhxjiCNQ41XYyvlQaOWHQqHmwil/NazzOy8tzuIwjIC4GJ9TvLp9v258LnT3yy6f+BO2e+d6qj78hD2TxSryctum8Unq2plS59dJi//lMKyAuBifdbw6QEf9VP3wXbbI9xt0+WmiLn+6dqacyC/OvqreVlbENpfzIejF74KNrXOiNP+mNleGnGg+NHTx033FAZTjJVs7H6SVH5/muJsLl/zVsc5P7IPf5jCMgLgYn3O8nX5x0HR8LpzILy6+he+wv6190YATda5h2WtlY3DcJtkqoONkOU0eHsDRND6w3MaGVX6ieH2HwzAG4mJ8TvGWAR31e+LXi4ubo+CVc1THqvy5cmK/JL/3MFgBxMX4tPHaAjrq58ovSov7a3NlwInno0RHv6+ct7l1U4a5CxHbUsxvb/3iW8tcOOavkgZ+/8VhNAJxMb6J4u0kRSb7NxifFSf2w0Vntpuk5MSdl6jz68TF3ZhHK2CSOFkTTZ4Y1+7vSXXjk6+5sNGvO7g6h9EIxMX4JopXAjrq99QPN4wZyUENG/NnyUn9xJusPxYhDK8gRlyMrzZeU0BH/Tz6nS4+ob5Kl7M6TjofZU7gt70T5SkgLsani9ca0FHfOj/5QJcJ8qeliV8nzv6ew6gF4mJ8E8fbS4t7mY7Phn788i9oLyhoop/OT7HKL4rzBzOPxsAkcbImnjx5t7Oq8bnSxA/PjOYwaoG4GN/E8QLQUT8TvyjNf6OcizJN8mfDif2S/N/PPXToGhg/4mJ8RvE2ATrq59YPp1VF0f5Pbe40nHg+SnT3y5/MEHaBuBhfZbw2gI76Vvp10+3cPX96Wvi9jMOoBOJifF7iFev0kRbjM6Ivv+FT4sR/jN3prI6+Opes9UuKX2cejYBJ4mR5mTzxZmZmP03pxcUBDqMSiIvxeYkXOupn6HfZlUT8ryznQ9I0f6ac1C+Ks/9bTY/fDCNHXIzPIt5qQEf93Pt1Ni6+pcjXp8v5K3PS+SjT1U9s3H+CtcjhD4G4GF9jvCaAjvrW+ol8/bxL/qpY9qrzE2/en8qhaIHxMT6f8T7bdHwmLHtN4tdJi39DUfpn3Ys6+uwcbPLbOWjMgGRzsrxNXjcdvKRufLYse9X75ZdwKFpgfIzPW7zUz9xveHvLJH9dOSd2+WvmpH6duPisvEEK4mJ81vHqAB31C+OHn+jU3ahp0vko090vey3WIIc9BOJifMbx1gE66lvtt9LPnmqfPz1VHzO/7CEczhjk+AS9xivG9Drz8dVT9fHhFyX5W/El++t1L5bpu3MTP/GO4j3MZy1ksgU9T97g1+vGZ0PVx8RPTM7PczhjkOMT9BovfVrht3NP6vwVMh+2+WvipH64pS9+SYCxIi7G5xyvCuioXzi/lfXt23fj/BPlfE46H2W6+g1vw9m99Moc7hCIi/FZx6sDdNS33m8lyXc/tOjyZUp1Hiz8xq4fANTxKfQSb5QOdu+8qBmPMdU4ffiRrx55AEMVfXdu6oerpZuejKMmW6GXyRPvLh9UNz5TqnGa+lWdDlLHp9BLvApb4nfZlcQbypF3+Kb5q+OkfmJM/3ju/uM3wAgRF+PzEO9y+OGnb3jykMznpPNRpqufeBP2a+E0+KhfN8mHXwvq8mXK8lyY+kXxIOKwdlEeH+kl3n7/6PXEh7TLTcdXxXKsk/qdYv48fMn+JP2LO/Tdua1f3Z1u1GQr9DJ54MrGYHjBhW5cpizHauon3um/lMPaRXl8pLd4ydb59dKte4q8fdUmf1WUHip17aoojpdnyNOlcnyCXuOlz0L73fbIkauKNT72faHtfJTp4ic+GHy5288KDm0XiIvxTRwvAB31c+MnPji8T5czU7rMxykOrsehDaEbH//2Eu/+/ta63fjGWY51Ur9R5k9EwX6Q/kX/nbv4ddL8PszvCMrJJr1NHrjaP9LRjcmULvEqfAuHNoRufPx7KTaPfQcP31EsWKebq0hOMh+4QhM3VeDwln4+yIn9VtOtbTGvn7CdDx3VebXwe3XnQH4TDmsXcnyCXuOlz9z4TfJwKMf5GLJ80xQxlqnH2+s3Pz+ijpPEa8Jekv/sab1k+6j2xSH9dV72MvWL0uL3meNd6JLNv70u1n4/vboYg9OtW13jlcS9izm8PYtXsNV+w6d79fOtbpp9UJezOrrOh9g4vt5N8senaXo1Di/Mh2e/fv/wjXpp9mQ83EA3Bya0nV/xQeU/xSfrIxzSCMrjI1ubP9K738ZGehVxrF2hy18TbeejzFnsf2I91D5GtI6TxlvmTv0d/X9Rkm/sXARSegH02/mol42f2DBHbtMpEruni9/lHeYk8UqiUJiMzxbQUT+3friuoZtu313k6N90uSvTZT7EvH9R/PvLa2uXXIfdDoFxcDyV47MBdNQvvR9OgXaS/Fc7cfal8nzU0WZ+xZp5j5jbn1zWB3mUAR31Y374uaIuh020mY9KJtmbmsY3DMAS0FE/4ocPA+LNyee1Y2mgl3gVnvIZ/f/4aSSvxh19F1X+dK2+ZkvVx9VPfo8tErvni1+86/oX3Ziq6CNeyXP7F2MsexqvDaCjflZ+p3fj7VWRq+dUvbGymo+4+F6UFq/Bho7vWNnHLjAOjsd0fLWAjvrgpwBnM8Q83U3sS68VvFw7V2R5bsFyGzGfnxWfqJ/dSfOesF+651lXATrqtX7yLmc2NJkPM+YvwDg4Hu34bAEd9WN+uMBNP456+ot3h6Ne6mvZ5bsXRIp3tB8bFZ0Sqv/flqOdu/uJg+1xSCqTO5bsYRCWgI76Wj+xcHZ/WtREX/FK7t88tto0PlNAR/2C+l12pWhju9tN8oeL3L1SbNIfkld8Vs5HXHxKbOJ/jRv04GlM8m5lOmAcHI/j+EYBHfXBrwaYE3wN0kmKpwznSrn5Snlud+Y3uxxzPzxuxVrYudJ49MpvHTAOjsdqfFWAjvq59cMb191jxYD6+dC3bWKUZtjz9yxe0eevlMfQRJ/xgg1eHxgGAnSS/C/UF3cK9khja5Y71wzAmHiAOpOrTbYtoKO+0c/kVpmgz3hB6Fc2Bj/RND4TQEd9Y7wmgI76VvvF8eGz1w8eud1qcmzf6uYgWUkG+zrr2bl40IjuE3QVpJ+g1/HRJ/hZAteW7E+P3VHM6wVr6dENMbfpSnJsZW1zcGOT4lwGxsHxeBkfdNTPtZ94E/to3d6kY3nvA3XtTLnSz+/bND5TQEd9pZ94Q/hh3Tiq6Dvecb/R18WH1lM3ERP/41fVFyfleOeTB7N/8+haVbJtAB31lZOnQrxTf4BuTCrLsYK6dqY85ZM9oml8TYCOeqN4mwAd9cHPAdBRH/wcAB31wc8B0FHf6NeJ8+fp9qcy1X1PUtfOlNCvpINDTeMzAXTUV8Y7PDOnGUcVy7GCunamNPGL0uyxHC7eXWR3LTdwpUnnNtz16GfP1iXbBtBRXzl5ZUT94rBuXJJqnLtj1bQzpeqzkmbPbBpfHaCj3jjeOkBHffBzAHTUBz8HQEd98HMAdNQb+UVp/kbdHqVS3a8kde1MKT0u2Ljo9k3jawJ01NfGK4rh83Vj0VGNU1LXzpTGfklxjMM97bR96fbNtY0sady5IVWfKB18ZXOzf/1ysk0BHSerdvLKOD/Zup1ubKA6PkldO1OO+2WvbhpfFaBjfFbxVgE66oOfA6CjPvg5ADrqg58DoKPe2E/sSbUPhhrfr/zsf+KNwndMxlcH6KivjXd19ci1OmnxDd14ylTjlNS1M6WN38rB7etyyDvBuTxmU6VN5ybU+YmJvJRDtgLi42TVTp4OuK9w+Sr6qvGV29hQ54cr1DkMKyAuxmcdrw7QUR/8HAAd9cHPAdBRH/wcAB31Vn6iJnxLt1eBuv1K186UIz5J/iGT8VUBOuob443S4hd04ylTHd/uODXtTGnlFxcf5XB3IAJxesi8pFXnBqzyi9LsI7YXlWCSOFmNk1eF8sMKqsbnyiq/Tlx8jkMwBuJifM7xqoCO+uDnAOioD34OgI764OcA6Ki38sNv4sv7lGTVfuXKca/8DU3jqwJ0jK8x3uGHMc2DaMosj29njPq2JrT1092mWkxQ9hBd4ybadt7EJj/x99045EZgkjhZjZNXB9HvW5T+xyhfc2GT39raJWdyGI1AXIxvongloKM++DkAOuqDnwOgoz74OQA66q398Jt1dR+SbNqvbKnzE596n8FhWAFxMT6jeHtJfl/dmFTqxqdrZ0onv7j4WQ75FHBzEm3jGjp1XkMTP1x+3/QELwCTxMkymrw6dJLsRabjs6GJ3/nr+TkcRi0QF+ObOF4AOuqDnwOgoz74OQA66oOfA6Cj3skvSrZ/orwXmexXNqzyi5LiQRyGMRAX4zOKFz/rbPp0XTU+V7r6nZcWt+KwRyFe3L0pQRNdO6+ilV9c/ByHrAUmiZNlNHlN6CbFY6zGZ0BjP81ThMpAXIzPS7zQUR/8HAAd9cHPAdBRH/wcAB31zn64f766DxnvV4as9UuyQxyGERAX4zOOt5vWP+ijdnwOdPXDQ4c45HGISfoDnahM186raOuH73a7F564Noc9AkwSJ8t48uoAXS/d+imb8TWx7FXnF6XFIzkULTA+xuctXuqDnwOgoz74OQA66oOfA6CjfiI/sfe8RO5BNvuVCZv8VuPsxziMRiAuxmcc7/D7+Tj7qtqnyqbx2XISP/wWnsMeR5Tkx3UilZN0rqOrXyctnslh7wKTxMkynrw6QAf9vv7WAdvxVVH1MfPLX8DhjEGOT9BrvPQJfpaAjvrg5wDoqA9+DoCO+on9ojR7B/Yf+/2qns1+2eUmX3kCiIvxWcUbpfnvjfZ5is3js+OkfniaJoc9Dty3V3x6/a5OCE7aeZmT+HXi7Ie8mf8QmCROltXkVQE66vn4v/ykzfh0VOOU1LVTKeL8ew5pBOr4FHqJV2HwMwR01Ac/B0BHffBzAHTUe/ETHxS+4LJf1dHML/sgh1ALxMX4rOIVn1gT0Y/2kclm4zPnxH5x9k31FsoitvEH13ST/HU68cSdl+jDTyT//XjaGCaJk2U1eVWAjvpdvygdzOZ36nHxKQ5rF7rx8e+F3Dygoz74OQA66oOfA6Cjfin8Vi68+Gzn/aqCpn54Wh6HUQnExfis4u12B1cXffxXuU/QdHym9OGHn1pj3CKu0wV/RBtfFOc/VRb66FylT78ozZ/GybKavCpAR/2In3gjs/vTLltOGO9JLDQOr/UHO3TUBz8HQEd98HMAdNQHPwdAt9LfSifYr8ZY9qr1S4qncShaYHyMzzpeUQCfrevTanwG9OXX6xd3FXGhWJ8hqC/Yw9PiafHtXZGnziX9+w2uWE2OZSIYL4uV+rHFIPp6TrlvE/qIF0+YahrfMABLQEd98HMAdNQHPwdAR33wcwB01Hv1W+3nPzvJfqVS9THy0/3mmJDjE7SON0q3B7r+rMfXQF9+ogZ/vd9PzxSxXZnUF2wAj/KCyFfnktPyi+L8c7xZ/NQWv+jvoeX+m6jGKalr18z8BMbB8WjHZwvoqA9+DoCO+uDnAOioD34OgI56737iQ9BTJtuvdqjue8Z+/ewCDmkE6vgUGsXb2bj4lp04+1K5L6fx1dCrX5L/gYjtqoL4ylcW7PHvsIFukl2080xsT50Llr18+4kx/8PGRnoVhmAFkYjGxYDfBurGUcXy+IZj1LQzYZTkv8zxVI7PBtBRH/wcAB31wc8B0FEf/BwAHfVT8RN71V9Msl+B6r4nqWs3zsH1OKxdlMdHGsWbpunVxKf2d5f7cR+fnr79Vvr5pojvaoK7BZshjUK8cPr6+trVevHgE746V32m65c/l2EYQ8RrtBh6cWH8RLPq8blR6P+iaXymgI764OcA6KgPfg6Ajvrg5wDoqJ+an9hvPqTbh0yp7nuSunZl4lMwh7UL3fj4t1G8wvdl5X5cx1dF/36DD4v4cDpcFuwzGM4oxAv4ghvV/Gq9JPtlP537DqbeT3wa/XmG0wgRp9ViwGX2al86No3PlvTABFov1jKgo94o3iZAR33wcwB01Ac/B0BHffBzAHTU7/r1+8eu00mz7+v2IhOq+56krp2OnTj/Bw5tCN34+LdRvOID3BPKfUwyPh2n4beSZo8RMcqCjXqsPxUuXsDVaKjoV7vg4NFb9uL8ezpTU5YDAXXtTGnml12BG8AwpEqIGK0Xg+7Uikqz8ZlTekTp4Pv9/tHrNY2vDtBRbxxvHaCjPvg5ADrqg58DoKM++DkAOupH/FY3BnfR7UUmVPc9SV27SsbFCzm8iePtJvm9y/4Tj6/EqfjF+bdX08M3FXGiYOM77MpijS+1Uc2HBRsUQe/ens6W5UBAXTtTWvnFxfdE0d5gaGMQsTkthigt/lDbn6DV+AxY9tq/eWy1aXxVgI7xWcVbBeioD34OgI764OcA6KgPfg6AjvoxP91DP0xY3q9AXbt65g9rGt8wgAZ0kiLrxMUPVG8/4zvF6fllLxRxXkMQNbiyWMsfZsuCPazs+DmRzryJahCSunamdPHrpMU3ojRbY4i7EHE5L4ZuUvyiri+X8dVR75f/BIdhBcTF+Kzj1QE66oOfA6CjPvg5ADrqg58DoKNe6yf2n18p70dN1O9X+rZ1jPrFYYyD49GOrwmdfnGwfLdOX+OTnKLfyZX+kfNErFcX1BdrAMkQlAV75Jx5N8nepOukikrnu9S1M+UkfvgtW9TP9zGUiRe/7rd8k4xPxxq/X+YwjIG4GJ9TvGVAR33wcwB01Ac/B0BHffBzAHTUV/pFSf5n6n7UxJr9ypr70u1bcDyV46tDFGebnTT7jurpc3zglP1eI2LFp+v6eNFAEAUb32GPVPZeXBzQdaRjqfMhde1M6cNvWLSTImaMzosBwFNkVG8f41PZ4PfnHIYREBfjc45XBXTUBz8HQEd98HMAdNQHPwdAR32tXycpPqzsObVs2K/sGGdf43hqx1cFPCRjzou1eMOydYFRvGgkWPnDbGHeeFvOcuegrp0pffrhzm29/tbFIj6nxaDg9G5cfAuePscHNvlFafHfHEMjEBfjmzTeIaCjPvg5ADrqg58DoKM++DkAOupr/XCXS1y0q+47VWzar+w5eGfT+KqAG0vhKV+qn+/xTdsvSvI3mcaLCa0+Xy6Au8/oOpUsdw7q2plyGn5RnP9gNR08wHYxlIErxctjA3X9mtLU77y0OIvDqATiYnxOi78M6KgPfg6Ajvrg5wDoqA9+DoCO+kY/07OppvuVKaFfSfKXNI1PB1GoH1J+k1EeG6i+bss98DsZpdkqQ/IDkZTXGnY+1saG0/fLft10MZQBnfD447Knrl9Tlr0a/PocihYYHxd748FpAuioD34OgI764OcA6KgPfg6AjnojvygpHqTZc0ZouV818pTP4BebxjeKy64kxvuMar9TLLex4d745a9kUP7QS4/feQHeyQz/fyfN/hi3rGNoRsAiwmISBf/xZT9Xqj5mfjs/e9BBjk/Q6OBsAnTUBz8HQEd98HMAdNQHPwdAR72xXyfJXqTfd3Zov1/VU/VZTbcGTeOTOHAg/1HdB0jVT7LcxoZ74Yeb1AjelqH5hTqhus7Vwdlyz/2S7F9xu1GGVgssIi6ma69sDC7W+llSHZepXxTnL+WQRqCOT6HR4tcBOuqDnwOgoz74OQA66oOfA6Cj3soPe6Ju3wFd9qs6lr0ObBz+sabxAecnW7cTxfqDTX5guY0N98qvkxbPZGj+cf76JdfvxtlXqzp35az8OnHxxSjevojhaYFFxMU+XPhrydE7VPmZsjw2U79OnL+fw9pFeXzknh/sVYCO+uDnAOioD34OgI764FeDNE3PwA2ndPuO635VxbKX+CDy+abxAeID48X41U+TH1huY8O988u/YHJdkjOQ1F46eLS+czeWvfbeb3ia/1dOnDgx9jQUxMvFPrL4dY9qM6X9+E4Rd+9ZW7vkTA5vYTePKkBHffBzAHTUBz8HQEf9wvlFSaG9Jekk+5WOOr8oLt7MYWhxzjknrtJN8t829dO1M+Ve+kVpfn+G6B+YdEz+wYP963aTwX+UO3ehGsSs/XDj+e4Fg1sw3NrFLwr23+s8mjjJ+CRxJWfT+IYBWAI66oOfA6CjPvg5ADrqg58DoKPeyS9Ki/uN7TUe9iuVVX64eIzDGMPK+vbtxafq95S9wCo/V+6lXycp3okL5ximEcRc1v6aaxeYdE7+cCGsbg7SKB78UB2cLeuCcaEXvzj7Gm4aX46X3F38UZw/S6uvoZfxDZk/DOPgeLTjswV01Ac/B0BHffBzAHTUBz8HQEe9s1/5gjN/+9UO6/zEm4Wf4TBUnC722QfjHhqqj2Sdnwv30g8XmuEibsbZCDGP8tbhzfOJRoLXFBxZDCLJY5fUm7IuGBf69ouS7HUXbFx0ezVewd1k6d6N1tHv+PJXcjwj8yG4MJuHCuioD34OgI764OcA6KhfaD9cHyP3GL/7VbOf+PAXcRhDnJcWtxIfnN6stlHZ5GfLvfaLkuJJDLURYh5RrHEH0uaCjQaCuLepuiCGi6HbHVw9SrOPqAMxYVMwtpyi31dX+tmDZLxMyRC4P7lOq6Pit0tdO1P24uyTnAfng1MCOuqDnwOgoz74OQA66oOfA6CjfiI//kxq+JNd7/tVg18nzn546ie2l11J/L+HduPsm2oblU1+ttxrP7wxuu2RI1fdibceYh5RrOXzPeoLNhvjqSH4dC0Xxchi6Mbbq+XHmNWxKRhb7oWfWFBv7W3kP86Qh8CblfJv0nXU+enamVJ6rB88cjvdfNgAOuonOtgloKM++DkAOuqDnwOgoz74WQIPzVD3F5XlPciGJn6ifnwIYzh/4/h5nTT7l/LrKk38bLj3ftnliHOY9AaIeUT9xVMz8fRMWbD132HjBUE8jxOfrmXB1i4GMRCjx7E1B2PHvfXLLo/S/KnnpCeuybAR93+den2c9X72VH1WN7buUTUfJoCO+okPdgA66oOfA6CjPvg5ADrqg58DoqQYuRmUpG4fMqWpXyfN/xrXBOGTtu51SVM/U87CL0qzxzLltcA8CqL+grsFmy+PQrwgK/uZgrJgg9rFgN/viYG8ozw4lSbB2HBmfnHxqU66fU8R9uni7z8fe5009jPkuFf+W1Xz0QToBFu7eUBHffBzAHTUBz8HQEf90viJ/eQN43uMfi8yYdkr+A1P/f+97qfDZWA+BHFmG/VXFuwz+PIoxAvynDkayoLd+HxO3DFMDEj722STYGzYCr+4eHc3yV+ne83Jr4Y6v06c/yNTbwXMo2CrNw/qg58DoKM++DkAOuqXxq/fX792FA++rO4vun3IlKpP8NthJy4+t5IeuxHTXgnMh6CsubJgox5XngrH1Wio6LJgo9IbLYaon2+JwZ1UB2oSjA2Dn/TLLne9F7qgt4Od+uDnAOioD34OgI764OcA6KDf1986ML6/uFH1CX6S2RXi/1/ItFeC8yHPZsuCjTPdlcUaX2qjmsuCDVotBlyuLgdaDgQcDcSOwW/Mr/bJXSowj4Kt3zzoE/wsAR31wc8B0FG/dH69ZPDIiv3Fiuo+FfxO0eR7a2U+QFmwUX9rLzJTC3Z1Za/H6eIdxetMgzFl8Bv3i5L8Mua8FmIel3YzUhj8DAEd9cHPAdBRPxd+Ym/5C93+YkN1n5LUtTPlAvk1PjazNB/4FwUbZ7ar6694ESJZsKvPmTcAPnF84U176eDDBsEYUfUJfqfYSYu/Y9orgfkQnNrBTgY/Q0BHffBzAHTUBz8HQEf9rh8evKHbX0xpul+ZcoH83nfuoUPXYOq10MwH/rvxmjEpRMHGd9jOxVpw2Pn+dHBeL82+WBOMEcuJCX6n2Emz7+Cm+Ez/GNT5UIi/w+ZmAOioD34OgI764OcA6Kifmt/a5rGubm8xpc1+ZcJF8RN782eaHt2smw/Byl9jjQCNBKt/mN0A6kc6X+0fO4iiogvIhKbJMeUi+nXiPOEUjEA3H/w7bG4GgI764OcA6KgPfg6Ajvqp+nXj4gG6fcWELvtVHRfGL86+2UvyDlOvhcj9ZPMrGjoVagCdsLOxzqMkP46r5LSB1dA4OYZcVL8oLX6N07AL5J35H5sPNrECdNQHPwdAR33wcwB01Ac/B0BH/Zif2ENeXd5TTOi6X1VxUfxw10/8Woqp1wJ5Z/7H5oNNpgd0ws4qO5/tgzIW3C8u3s00D4G8M/+V82ED6KgPfg6Ajvrg5wDoqA9+DoCO+jE/3PCqkxZf1+4rNZxov9JwgfxO8qZalUDemf+x+WCT6QGdsLPGzrtJ8WhNgGO0SI4RF98PZy8G10OOkXfmv3E+TAAd9cHPAdBRH/wcAB31wc8B0FGv9eum2+v6PaWak+9Xo1wkvyjJf36Y+Aog78y/dj6mCnTCzow7x+lbXaCSNskx4fL4bd8deWf+jeejDtBRH/wcAB31wc8B0FEf/BwAHfWVfuo9M0zob7/a4WL55U9gWrVA3pn/yvmYGtAJO7PuXHzS/nVdwHbJaeZS+SX5S5h/6/nQATrqg58DoKM++DkAOuqDnwOgo77WT3yAepd2P9HQ634luEh+4pP1LzGlWiDvzH/tfEwF6ISdOXeOp16pAdskx4TL5offUcbx4bNd50MFdNQ7z68K6KgPfg6Ajvrg5wDoqA9+CvZtZjc0vRjY9361SH5NN69C3pn/2vmYCtAJO5u4cxHs8JGcNskx4bL6raWDiyaZDwA66ieeXwA66oOfA6CjPvg5ADrqg18J4kPT/XX7SZnqPiWpa2fKhfJLil9kOrVA3pn/xvkwhdCa/ZoLnbAzL51Dt5IOHmecHAOWvZbJbyXNnjXpfFDvbX6pX0q/fn/9rAMXbt7o/APFjc7df/wGO89Rv8zYu+xHtjZecuiHRwiurh651s6nuMH11tYuOZOySkBH/Zgfm1gBOuqDnwadNHuDbk9Rqe4vkrp2plwgv5NRuv1AplIL5J35N5qPJgidvHV4sx6NBHEHFl+d7wazkmQPieLBD2uSY8RyopfOL84/7mM+FHqZX4UL44enpHXj7VVc7BclxeO6cfGibpL/vShOH+2lgy+Iufi2mJORp9btMi6+142zr0Zx9n+dpHhnN81fId6pPy2K8weLTWCwspHdaWMjxb38WxOvBOLGDSGiJL+4188eI94kPk/E+Trxae3fxb+fEXF9TRSC7+vi7sTZDztp8Y1OXHxW/P0BFoznRGnxyF6/uGQ1Obb/8OGD15lkfBLQUT9RvBLQUb8QfgcO5D86XIeaeZLc3VcU6tqZclH8sL73+qdbQodijTuQNhdsNBDEvU3VAUzS+VgwK/3sJ8TB/G1dgkxommxTzqtfLz1+Z6bZGLr54N97snk0ATrqZ+bX28h/vJPm9xHF+XfFOn1PuShVzYcro3TwHVEY3yV8XrCSbD1w/+ax1dvc5t76B9g3AHExPuN4AdzyVsTcE28oHiAK8u+JNyT/jptCYHzlWMFyDDaUHqKf74g3A+8U//0cwbuft7l1Uw7HGIiL8VnFWwXoqF8YvyjZ/gndPEjK+VCpa2fKRfHDG85ukh1iGrVA3pl/4/mog9ChWMvne9QXbDbGU0Pw6VoOYpLOK4PpJYMVsTl8QZeoOpom25Tz7Nd0tWIZyDvzPzYfbGIF6Kifaz+cwt15tnv+XPEJ8L/V/JdZNx8urPSLs2+KgvZG8d8PPT/Zuh2HWgvExfhq4yVO3/n0XDyum2ZvF7xcHZdk5fgc2eQn3hz9h4j7qbgFL270wbFqgbgYn0m8jYCO+oXyE3P8J2qOVTbNhy0XxU+sw8+spMX5TKEWyDvzbzUfVRA61F88NRNn3GTB1n+HjRcE8TxOfLqWBXuSzhuDWY2zHxOL6T91CdPRNNmmnHe/KM0+wlQ2Anln/ivnwwbQUT+Xft3upVfu9rNCvOn5M/FJ8rtqXqvYNB+2tPHrxNnHxL/PGb6x6A6uPgxSAeJifNp4gbusb50tYv1J4fMSnqrW9iVpMz4TWvvhqwTxaZ/3zx/ZuBAX46uM1wbQUb9QfjgdLoqP9vkO1vPRwIXxS/J/714wuAVTqAXyzvxbzUcVoBNE/QV3CzZfHoV4QVb2MwVlwTZ7iogG0AkaBTNcUEnxem3iFBon25AL4xdvrzKVlUDemf/G+TABdNTPnR/eNUdx/izbszvG82HISfzwdZLYhP+4l2wfxQVfiIvxjcWLswfC+26i/RvkKW4TlscG6tqZclK/Tpp/XPz7K+elxa0QF+Mbi5dLwArQUb9wflFa/Ew5l+Ck81HmAvm92uERmcbzoQN0gjizjforC7b+7JJ4QZ4zR0NZsJufz1kB6AQtg7nsSrgQR5O8IS2SbcRF8ouS4tlMohbIO/NvMR/VgI76ufFbXz9ylsjVccG3qbkzpc18mNCnnyjCn1tJs+eupkcTGW+/v35t8SnhYDcuXiyK+0LdOxoXs3WT7C/3bRw7rMxxq9cf/56Jn8jZW8o59Dkf4IL4ncSdOUXKan9Ghbwz/07zUQZ0grLmyoKNelx5KhxXo6Giy4KNSj+jxZVfgu/u1EQaJtuYi+YnNuwv4vQuUzgC5J35d5qPMqCjfi78Njf7N1zpbz2Sp5O1+Wui7Xw0ccp+H+6lA1Gks0/q2pqw5Dekrp0pp+kn9ov3rKT5T5933p31pw4bUF4vZGvXM2nst5oev1n5Zilq/k7lcTTHNlwIvzj7Gr4eY9oqgbwz/07zUQZ0gvJstizYONNdWaxxOg3VXBZscKaLtZNs31EUoQ8ZJ9uCi+rXSbKLmb5dIO/M/0TzIQEd9a33i+OLrt9Ls0f04vzTunyZ0nU+qhj8puPXifP39/rFXcUyqP10pEJdLwpbuZ4VWvntXFDYnD9XLoIf1o7JRZ3IO/PvPB8qoKMelAUb9beyWONUuFqwqyt7A4TOazDn9i++lnj3/PKmZNuw7LVIfp20+Dumbgjknfn3Mh/QUd9qP9yudfg7/zT/P5v86Viei+Bnx1n4RXH23m5SHOOyqIRcL4Je1x99WuGHK+zx23+ZG5P82XAR/MQHnRfpLuAsA3ln/p3nQwV01EsfFGyc2a6uv+JFiGTBrj5n3gD6TCWY1XTwgF48+JYu2TYsT9wi+snfZKv5U+hlPhS2zm9lY5CLvP2na/5USg+VunamDH576yfewP5N1T0K5HoR9Lr+6NMav+HXi8yHbf6aOO9+uK5D/L+7MVW1QN6Z/4nmQwI66lWv5mvG0EAQBRvfYbeuWEvuTwaRzVNmyixPHKhrZ8q2+kVp9nxd/vj3TDcPCeio9+a32j96fi8d/NWk+ZNUfYKfPdvih4vTcEzIZ8cDWC9cb97WH/Wt84uS/K3Ig2v+qjj/fvk/9dYvvjXTVAvknfmfeD4A6KhX/cx+jYVGgijYrS3W/PtKw9M7SX6Zzc9SwObJs2Ob/fBTn7V0cGtd/phiK0BHfSv97nz4ojNX+tlTenH+PR/5A1Wf4GfPNvp10uzL3bj4WVyYxvXmZf1BR33r/KJ4ECF2H/lTOc9+Yh18H9/p42eQTFMtkHfmf+L5AKCj3s1PNHQq1AA6YWdunZcAHfW1fju3Tsw+qE5SFesmz4Vz4dfPnlOXP1NAR33tfJgCOuq9+HXj7MKd+3h7zl/wc2bb/aI4+6fV5Ng+H+sPOuq9rGfoqPfiJ4rTG33nb579hhclJnmH6WkE8s78e5kP6Kj34mcFdMLOvHQOHfVGfrc9cuSq3TR/ct2n7brJc+G8+EXp4LtrydE71OWvCdBRbzQfTYCO+on9zkuLs8SnpT9Q8yapy4spg99y+Inj4/sr/eyp/f4xPHRk4Y4PQHyKvEDNm6QuL6acVz98qhb//jLuk8/0NAJ5Z/69zAd01HvxswI6YWdeOoeOems/3LGqkxb/pk4cWDV5rpw3v26S/Z5J/nSAjvm3ng8doKN+Yj882UrE++lyvMOYNXkxZfBbSr/34bQxl5YxsG65fidezwB01HvzE/G9TROvM8tec+MXF+8Wb17uwtQYAflj/r3NB/Ve/KyATtiZl86ho97ZD99HRGnxC6Jwf6N28hw5n37ZFZ1kez9TZAzknfl3ng8V0FE/kV/3whPXjuL8pdXx6nNjwuC3vH44Q4c7W1XddKgMrFuu34nWswR01HvzW+1nl1bF68Ky11z44SYoafFQ0++qJZA/5t/bfFDvxc8K6ISdeekcOuq9+HU2j91cTNZfjE2eZpJNWfaaJz/ceAbPMmZ6GoG8M/9e5gM66ifyEzFdKH9LWo4VlPG6MPgFPxC/3W56TC3WLdfvROtZAjrqvfmtHzxymygdfKUpXlOqPvPiJ97Y/8lKeuxGTIsxkD/m39t8UO/FzwrohJ156Rw66r37raZb22LiPozJ002yKdVFIKlrZ8pZ+DXdY1xCzZ/Cmc7v8HGXwwd0FCcRi0m8Ngx+wW+EcfE98Wn7kXimAZfgLrBuuX6d17MK6Kj35rdzr/zsb4zjbaDqMyd+78e985kSKyB/zL+3+aDei58V0Ak789I5dNRPze/gwf7ZYlJ/XvAr6iSbUrMYtO1MOUu/KNm+lKnSQpc//j2z+cUvATpJ8WEZg028Jgx+wa+K+O1yZ+PiW3Ipzs3+1+1nz3aJV0fVp+1+4k39l3r9/GF5PjD6WqMMmT9Br/NBn4n9AKE1+zUXOmFnXjqHjvo98Vs5uH1dMcG/U/Vgfh3VxSCpa2fKWfvxKsn+MGElIE/MlzZ/toCOeic/Xo/wRPXqf9t4mxj8gl8Td+6CVdwL65br12k9lwEd9V79VtPBgyeJV6Xq02q/OP8e3qSs9S+6xaT5E/Q6H/Tx4SdvHd6sRyNB3IHFV+czS85qnP2YeCf2cjHhw9OrVVQXlaSunSnb4teJsy9FG9u3YTqGQJ6Yr8b8mQA66p38cPN98an6neq4XeOtYvALflZMsr9cP3j0x1zWcxnQUe90fJQBHfSiWB/tpYPLfcSr5q29foMrekn2p/uTI3f2kT9Br/NBHx9+KNa4A2lzwUYDQdzbVB1Am4Jx8hNFqxul+RurF4PvxdUevyjNPiIvxkCemC+r/FUBOuqd/KKkeBDu0qaOd9J4ywx+wc+G0kO80f+cKIonbNZzGdBR73R8lAEd9Pv6WweiePBln/Gq1LUz5XT8Bn+1spld4Ct/9PE2H/Tx4YdiLZ/vUV+w2RhPDcGnazmINgUzsR9+8tRN8jePLgbfi6uNftkH1zYHN2a+nPOnAjrqrf16cXHzTpr/dXmc/uLdYfALfjbU+Yk3+r+/unrkWly6xsBxwOPB+vjQATro928eXeul2Rfl+HRxmFKNs41+4sPGW/alg03mzkv+6NNGP9RfPDUTT8+UBVv/HTZeEMTzOPHpWhbsNgXj1a+Tbt/L9+Jqu594o/LB/enWbXzkDzrqredDbID3x3eF5fH5jjf4BT8b1vl10uyTUb84zCXcCBwHPB6sjw8doIMet1cVnza/UB6fC9U42+i32h/ch3nzlj/6tNUP9RfcLdh8eRTiBVnZzxSUBdvsKSIaQCfY6uTgpxDyuck+Fpfq02Y/UbQ/JD5pz+Q7IHxXrZ7d0I1Ppa6dKYNf8LOhhd9L1CeA6YDjgMeD1fFRBeigX+1v9aM4/3zD+IyoxtlGP/HJ+n/6/fWzmDsv+aNPW/1wZhv1VxbsM/jyKMQL8pw5GsqC3fx8zgpAJ9j25Az9VvrZb/hYXOoinQ+/7IuicKZMiRXU/CmsnQ/cy5dXgH/XbHzLNh/Bz4az9sOFnJ00vw+X9whwHPB4MD4+6gAd9Ctpdj9RrL9rMr4mqnG21W+lP/g15s5L/ujTVj9Zc2XBRj2uPBWOq9FQ0WXBRqVf5OTs+q1uHLuLWBy1V483UV2kkrp2ptwrv52ffOUPYGqMUM4fWTsfvX5x125cfLQ8Lsmq8bky+AU/G07kFxf/3IuLA1zqU9mvDh++8OxeP/sdp/FpqPq01S+KBz+8YGPrHB/5o97bfFDv00+ezZYFG2e6K4s1vtRGNZcFG1zk5Iz5RXGhPUVrwvJCBXXtTDkLP1w5j5++MU2VELmymo8oKeJOUvxjuT+VJuOzYfALfjb05Rcl+Z+J9X4nHg9Gx0cToFvpb6Xi+Pz3Sccnqfq02a+bDv7aR/6o9zYf1Pv2A2XBRv2tvchMLdjVlb0BQjcvyRnzEwvkbrrF00S5uFTq2plyln7i0/Z3xKftx3S7g6szZSNAnpivsfyxicTpeKpWU6EGbcZnwuAX/Gw4Bb+T3TR7vXJFs/N+tZJu3UAcj787/N2xv/GNUdfOlNP127rnJPmDjvqm/coI0FE/DT/8i4KNM9vV9Ve8CJEs2NXnzBtAn2kFM3U/PF9bFKwv6xZRFUcX1w517UzZIr+vdNL8N89Li1sNkyeAPDFf2vwBK+vbtxefMJ4UpcV/azzHOMH4tAx+wc+G0/brxoP/EMfDI6P4+I15iBjh/I3j5+F57+p31buemn5NWfZqt9/gC4v8vHLoqFe9mq8ZQwNBFGx8h72UxVpCHFzP1i0kHUcX1w517UzZUr+TnTh/v8jLc3pJ9jO4oxJ+TnKgf+xOa/3BBd14+wjuUy4K9B+KAv9xjb6Snsa3y+AX/Gy4x37yOPrdbpLfO4qzTTwZDPcgEP/vLuL1vvh/Py2K9IvlcdTgZ8158+um2bPK+7MpoOP+XrvfmwI66qfpZ/ZrLDQSrP5hdgOon7fkaP1W0uJ83WIqs7y4dhaYvq0Jg1/ws2HwC342nEc/8UbmTtyWrYB9nft7435vAuiob4efaOhUqAF0ws7cOi8BOupn5ocrPnWLSlK3uHTtTBn8gp8Ng1/ws+Gc+r2d27EVsK9zfzfe7+sAHfWt9LMCOmFnXjqHjvqZ+okFU3nxWXlhgbp2pgx+wc+GwS/42XBe/aIkP87t2BjY17m/W+33VYCO+lb6WQGdsDMvnUNH/cz90jQ9I4qz/zNdXK4MfsHPhsEv+NlwXv1wweppp11mte9jX+f+br3f6wAd9a30swI6YWdeOoeO+tb4DX/aZLC4XBn8gp8Ng1/ws+F8++UP4zZsBOzr3N+d93sV0FHfSj8roBN25qVz6Khvld/a2iXXkY96rF9c9gx+wc+GwS/42XCe/cSe+w2bp6FhX+f+PtF+LwEd9a30swI6YWdeOoeO+lb6RWn2grrF5cLgF/xsGPyCnw3n3U8U7Gdy+20E9nXu7172e+iob6WfFdAJO/PSOXTUt9ZvtT/oiQV1smpx2VL1CX72DH7Bz4bBb978siuije3bcAuuBfZn7u/e9nvqW+kHCK3Zr7nQCTvz0jl01Lfer5cM3qxfXHZUF2nws2fwC342DH7z5xelxWu4BddC3Z8VtrJ+KJzET946vFmPRoK4A4uvzucq2StJdly3uGxYXqjBz47BL/jZMPjNrV+f23Alyvsz2dr6QU7ih2KNO5A2F2w0EMS9TdUBtCmYPfHrxsW7NYvLiBaL1YjBL/jZMPgFPxvOyg8PCeI2XAmxF89l/RB09UOxls/3qC/YbIynhuDTtRxEm4LZM79ekue6RdZE08VqyuAX/JqIZ5vj1w242rYXD74epfk3Bb8TxfkPXPxUlscW/OwY/Kr9on5xmFuxFtiHuR+P7c9sYgXoqG+rH+ovnpqJp2fKgq3/DhsvCOJ5nPh0LQt2m4LZa7/Tozh7r26hVdFmsZow+C2PX5QOvtuLhw+AeHs3zV8hiu2zxL9PEHyA4AnR5sJuvL3aTQd3WE2P3+wu61tnp2l6NaxTLFasW67fkfV83nl3/pG1tUvOXDm4fd3uBYNbrGxkd4rSbE0U+SPddPvu3TR7iOjzlztx/jzx76tFX//UjfNP4E2AOj5JOXYXBr/gJynW179g3VZBrN15rx9WgE4Q9RfcLdh8eRTiBVnZzxSUBdvsKSIaQCfY9uQ0+vX6xV11i01Hm8VqwuC3eH6duPhclORv7cbFi1bS7Jf2bWT3Wt0cpOsHj/6Ybv2ZAjrqa9ezKaBbXz9y1v7+sR9fSQeHxFjv10uzJ4s3EX/USYp3duPsa7r46ugjfyqD33z74bn5XG5jwPrj+vW2nqlvsx/ObKP+yoJ9Bl8ehXhBnjNHQ1mwm5/PWQHoBNueHFO/0/F4PN2CU2m7WJsY/ObbTxTmH3ST/N+jtPh98fdDxRpKcFMeLCisM643k/XXCOio31O/8za3bopTmt2keHSUFH8i4vwvwZMyBypt89fE4Dfffp20eA+X0RiwzrjeatefKaCjvs1+subKgo16XHkqHFejoaLLgo1Kv8jJsfITi6/yoSBgeaGCunamDH5z6IdPnEn+OlG8frGbbq93u4Orc/mMAOuM6814/dUBOupb4YfT9DjVLgr4k3gm4XtG+bNg8FsIv20umRFgnXG9Oa2/MqCjvs1+8my2LNg4011ZrPGlNqq5LNjgIifHwe+yK4lPTB/SLDrXxVrJ4DcvftkVUZq9QxTpx/eSwcqJEyf03zUpwDrjerNcf3pAR31r/TY3+9df2RjkK2n2TJGrD53KnxvLcxH87NgSv/eJ5TFWkLBeuN68rT/q2+4HyoKN+lt7kZlasKsrewOEbqGT3Um371leeI6LtZLBr91+uOJafGJ8k+DPdtPB9bg0jIB1xvXmtP7KgI76ufJbWT9+W/Hp+xFRWrxLl/M6lucD1LUzZfCblV9+gstkF2JtLPPxgX9RsHFmu7r+ihchkgW7+px5A+gzrWBa4odP2ae+y3ZfrHoGv1b7fXilP3hsFF98Qy4GK2Cdcb1NsP5OATrq59rv/GTrdmLzfrLI96fL+S+zNB9D6tqZMvjNyC/J/lVM/Uidwbrg+qhdL6aAjvp58cN/N18zhgaCKNj4DjsU6wZ0k+IYFp3zYq1g8Guj3+CKbpq9fiXZOua6XgDoqJ94/QHQUb8wfngOvSjclwx/TlY5H77nN/i5chK/KM42Oe1DYF1wfRivlzpAR/08+Zn9GguNBKt/mN0A6uctORP5RWn+NtfFqmPZK/jZcQp+J3tJ9qrV/taqj/VCvbf1R/3C+ok56Is3Sm9X5mOM8jUXBr8Z+sXFX3Gah8C64PpwXi8qoKN+Mf1EQ6dCDaATdubWeQnQUd9qP7GR98Ui3X2Sl3ZhGlJ6qNS1M2Xwm8yvmw7eid9Ec620cv3RZ+H9ekleRPHgv33Ob9kr+NlxMr/siigp7sLpDcfHhH5WQCfszEvn0FE/F35iof6Z3WIdp7roJXXtTBn8JvCLi2+t9Ae/0O+vnyXnmPO9FOuZbJ3f5mb/hri6HF9PTDS/guW1Evzs6MHvJZzecHxM6GcFdMLOvHQOHfVz47c/yc4V7xgv1yxKI3pY/CMMfhP5fWBt81hXnV/BVq8//r00fr2NQdaJiy9q5s6IntdL8LP0E3P33V5c3Lxqfvl3OD58A52wMy+dQ0f93PmJhfg75YVpwkkXf5nBz90vios3p+nhm+jml0vACtBRH/wcAB31Y37npcWtojT7iG4e6+hzvYDBz94vSvPfaJrf4QKwBHTUL4WfFdAJO/PSOXTUz6Xf8EEKlvdT9rH4VQa/ifxetb5+0XWr5tcW0FEf/BwAHfWVfuevX3J9/CxIM5dael4vwc/Br5NmXxZvtvBVU1jPE/hZAZ2wMy+dQ0f9XPtFSf7/dItURx+LX2Xwc/eL4vxP8GCLpvk1BXTUBz8HQEd9o9/q6hGj59T7XC9g8HP1yx+AeeR8Ns6vCaCjfin8rIBO2JmXzqGjfu79cDtKsSjfN75IR+lv8e8w+Ln7RUnxmoMH+2ebzK8JoKM++DkAOuqN/XB2q+6BPD7XCxj8HP3EG6vb3ObeuMfHTNdLHaCjvpV+gNCa/ZoLnbAzL51DR/3C+A0f9FDxhCLQ2+Ing5+7Hx4LiXtb28xvHaCjPvg5ADrqrf3wPPBOXHy2PMc+1wsY/Fz9sitWksE+zqf1/OoAHfXL4idvHd6sRyNB3IHFV+cLm+woLf5Qt2j9Lf4dBr8J/OLiUxdsXHRbl/nVATrqg58DoKPe2S/q5/twBbKcY6/rRTD4TXS8vYjz6Ty/KqCjfln8UKxxdqK5YKOBIO5tqg6gTcG0yu/c/cdvUL4AzeviFwx+7n6dOPvhaj+7yHV+y4CO+uDnAOion9hPfJJ7CObY53oBg99Ex9uXLjh49FY+5heAjvpl8UOxls/3qC/YbIynhuDTtRxEm4JppZ9YqA+VC9bn4geD38R+T590fiWgoz74OQA66r35RUn+Js/rZYy6dqZcQr+fV+Z24vmlfln8UH/x1Ew8PVMWbP132HhBEM/jxKdrWbDbFExr/XABWhRn7y0vfFC3qE0Z/Cbz6yb5B/v9o9ebdH4B6KifeL0A0FEf/BwAHfRrydE7RGn+TR/rRV13wc+eUTr41zg+rF7UufDrjz6+/FB/wd2CzZdHIV6Qlf1MQVmwzZ4iogF0gm1Pjle/1f6xg/I2ij4Wv+oT/OwJPZ+21cr1Qn3wcwB01A+9VvqDx/lYL2Xq2ply2fyiePDD1fRoIueE8xPWswGgE8SZbdRfWbDP4MujEC/Ic+ZoKAt28/M5KwCdYNuTMxW/lTR7no/Frx5Ewc+e9Hgt5oRs5XpRGPwMAR31u374qV4nzj6mWwsmVNedpK6dKZfRr5tmz1DmpNXrhX+3yU/WXFmwUY8rT4XjajRUdFmwUelDsg0AHfVDryQ5dGM8ZUi3qE1ZPpB2DgZ9WxMuqd/Jtf5gP+elteuFDH6GgI76Mb9ukt9btx6aqK47SV07Uy6n3+DDBw5t3kCdD06ZFaCjfmx+2cQK0FHfZj95NlsWbJzprizW+FIb1VwWbDAk2wDQUT/it9IvNsRCrvxtdh3LBxKoa2fKJfZ7g5wPwbCeDQAd9XPpN7yRUZz9j25dVFFZL7vUtTPlcvoNrljtH7uwPB+2gI567fzaAjrq2+4HyoKN+lt7kZlasKsrewOEblmTrfXrpvlzdQu8juUDCdS1M+Uy+62mW9vqfNgCOuq182sL6KgPfg6Ajvpav25S/KJubeiorhdJXTtTLq1fP3tO1XyYAjrqa+fXFNBRPw9++BcFG2e2q+uveBEiWbCrz5k3gD7TCmYu/c5JT1yzG+ef0C10HdWDSFLXzpTL7CfeLH2i31/HnCzt+rMBdNTPvV83HVyvExc/0K0Rlep6ObVu9G1NuMR+H+v3D9+oaj5MAB31jfNrAuionxc//HfzNWNoIIiCje+wQ7E2AHTUN/p1k+yQbrGXqSz+XeramTL4Zb+tmw8TQMf5bJxfE0BHffBzAHTUG/t10uJvdOtEcny9hOPNhorPydV0cLRpPuoAHfXG81sH6KifJz+zX2OhkWD1D7MbQP28JWdP/bpx8ULdopdUFv8ude1MGfyEXz/bz/RbAfPI+TSe3zpAR33wcwB01Fv5iWPu53RrBdSuF007Uy61X5K9wGQ+qgAd9VbzWwXoqF9MP9HQqVAD6ISduXVeAnTUL5Tf8NR4WvxXeeGDI4uf1LUzZfATfnH2NVx8xPQbA/PI+bSa3ypAR33wcwB01Fv7nZ9s3c54vWjamXKZ/bpJ/qE0PYKrwsP6MwB01HvxswI6YWdeOoeO+oX066R5r5Nm369a/LsHgfK6LYPfjl8nKV7PtBsD88j5dJrfMqCjPvg5ADrqnf3EWvi0yXpx5TL7RXH+3dV0a81mPlRAx/l0nl8V0FG/FH5WQCfszEvn0FG/0H7dpHi0bvFLqgeHLYPfKb8oKZ7ElBsB88j5nGh+JaCjPvg5ADrqJ/ITb5DfYLJeXBj8Bo+2nQ8J6DifE82vBHTUL4WfFdAJO/PSOXTUL4Pf6d0kf8v44g+bhw2b/MTfd2O+G4F55Hz6mN/g1yI/8Qb5aSbrxZbL7helg792mQ8AOs7nxPMLQEf9UvhZAZ2wMy+dQ0f90vitJUdvH8X5l9QDQHeQmFL1CX47jNJsjSmvBeaD8+ltfqkPfg6AjnovflFSPNhkvdhw2f3E3vW5KL74hkyxFTCPnE8v8wsd9UvhZwV0ws68dA4d9Uvnty8d3F0eALqDxJTqgRT8TvG8tLgV014JdT4UtnK9KAx+hoBudSO7p8l6MWXZawn9Tnbj7SNMsRUwH5xPb/NL/VL4WQGdsDMvnUNH/dL6raR57U+9mqg5mLTtTLlIfqJgn8XUa6GbD/4d1rMBoKO+1X6rybHMZL2YUPVZVr8oLZ7BFFtBzoeg1/mlz8L7AUJr9msudMLOvHQOHfVL7XfnwxedKQ6Ad+kOlibqDiZdO1Mumh9+Rsf0j0HkPqy/JfHDY1VN1ksT1XW3rH6dpPjHbvfSKzPNxlDnQ2Er14vCNvnJW4c369FIEHdg8dV5mDzFrxcXN+/ExRd1B00VdQeTrp0pF9HvwIH8R4cTUALyzvxr58MW0FEf/BwAHfVT8cMduEzWSx3La28Z/cQe9dkoPn5jptkY5fkgW7teyDb5oVjjDqTNBRsNBHFvU3UAbQpmIfw6/eJgJ85+qDt4ytQdTLp2plxUv/M2t27K9O4CeWf+a+fDFNBRH/wcAB31U/PrpdlP6taHKV3XXxXn0Q/3ZO/EecI0G0M3H/w7rGcDCB2KtXy+R33BZmM8NQSfruUg2hTMQvl10/wxugNIpe5g0rUz5SL7RUlxF6Z2COSd+TeajyZAR33wcwB01E/Vr5vkP61bHyacZP3pOLd+Sf5wptkYIvdh/U3mh/qLp2bi6ZmyYOu/w8YLgngeJz5dy4LdpmAW0k8cHK9WDyCVlQeTIxfdr5MUGdMa1t8S+0VJ/ku69dHESddfmfPrl7+CaTYG8s78j80Hm1gBOuqXyQ/1F9wt2Hx5FOIFWdnPFJQF2+wpIhpAJ9j25LTCb3X1yLWiNPuI+cHkxuXwyx+GnCLvzL/1fOgAHfXBzwHQUb8nflFa/KF+fVTTz/o7xTn2+0DdxZs6IO/Mv3Y+bAEd9cvkhzPbqL+yYJ/Bl0chXpDnzNFQFuzm53NWADrBtienVX4rG9mdOmnxDYODyYnL4hfF+UuRd+bfeT5UQEd98HMAdNTvmV8nzt+vWx9V9LX+JOfWL86+1tvIf5xpNALyzvxXzocNoKN+mfxkzZUFG/W48lQ4rkZDRZcFG5U+JNsA0FE/sV83KY7hIrTKg8mRy+SHMxXM/8TzAUBHffBzAHTU75nfzhPysit060NHn+sPnFc/XGQm/vtCptEIyDvzXzkfNoCO+mXyk2ezZcHGme7KYo0vtVHNZcEGQ7INAB313vxW+tmjdAeTK8tey+B3oH/sTr7mg3pv80t98HMAdNTX+kXp9kC3NnQsrz1Q186Uc+0XFz/HFBoBeWf+a+fDFNBRv2x+oCzYqL+1F5mpBbu6sjdA6MLkefLDndDGDiYHqgflMvmtJNkvII/MZ1h/BoCO+rn366b5c3Xro0x13Unq2plynv2ixO5OZsg78984HyaAjvpl9MO/KNg4s11df8WLEMmCXX3OvAH0mVYwS+d3+PDB6/SS/G/Vg82WdQenC+fM723MZ1h/BoCO+rn3S9P0DNzsQ7dGVJbWy5C6dqacZ78oLV5z2mmXGc8N8s78N86HCaCjfln98N/N14yhgSAKNr7DDsXaANBRP1W/tbXDZ4l3vf+pHnSmrDs4XTiHfid761uNDwHRQeQ+rL859jM5Ha5ZL9p2ppxnv05a/Nu5hw5dg+lrBPLO/BvNRxOgo36Z/cx+jYVGgtU/zG4A9fOWnLnx62xcfMtumn1ePfiaWHdwunBe/cSnhl8bJtkCyDvzr50PW0BHffBzAHTUG/t1kuL15TWismq9uHLO/T6tuzNgFZB35t94PuoAHfXBzwSioVOhBtAJO3PrvAToqA9+Cjpp3uvG2TeVg6ySDQenNefZr5NmXw6fHKoBHfUL44efRoq5PynXQJl168WFc+0XZ187f+P4eUxdI5B35t94PuoAHfXBb9pAJ+zMS+fQUR/8NMA9x7tx8T31YCyz9uB04GL45Y9hCmuBvDP/RvPRBOioD34OgI56Kz/cnWt8DezQbL2Yc579OnHx3SgpYqatEcg78281H1WAjvrgN22gE3bmpXPoqA9+NRCb0SVVvy2tOzhduDB+4lNENx1cjynUAnln/q3mowrQUT8Tv2hjuxul2w/srV98a/6vEUBHvZFfE6CjfqZ+UT/fp10DgsbrxZDz7De8z0OS50xbI5B35t9qPqoAHfXBb9pAJ+zMS+fQUR/8DIDfSaoHJlh3cLpw0fw6SfYipm8MyDvz7zQfZUBH/Z77nZcWZ3WS/C924xYbc5QWv3XixIndexFDR32jnwmgo36mfoix6vnytuulifPuJ/L0M0xbI5B35t9qPqoAHfXBb9pAJ+zMS+fQUR/8LCAOuCeaHpy2XFS/KM42mb5dIO/M/0TzIQEd9Xvu1+1eemXxZu6ftbGnxe+jDXTUN/qZADrqZ+6H+8frYnddL1Wcd78ozR/FlDUCeWf+redDB+ioD37TBjphZ146h4764OeAKM6f3XRw2nKR/Tpp8b9ra5dch+lbuPXSTfLH6+KW7CV5Qb2RXxOgo37mfp317Fx8JzsW8wTrRce590vypzNljUDemX/r+dABOuqD37SBTtiZl86hoz74OQC6fn/92itp9seVB6clVZ/F9cteK9KHO/st1Ho5fz0/p/GCxDj/9IHNw7cw8WsCdNQbja8J0FFv7Yd7hneS4sNj8XpZL6c4/375C0S6jH4RhLwz/9bzoQN01Ac/Rwit2a+50Ak789I5dNQHPwdAR/214/jw2eLA/JPxg9OO6kEuqWtnylb7JcVTZP4UtnJ+FVb6nXPOiavgxhfaWEmZs5U0f0WTXxOgo95ofE2AjnprP3xv3UmzN1TFq7Lcxobz7sdrOEKx1gA66tvqJ28d3qxHI0HcgcVX52HyPPutrx85q5vkL1MPUBs2Hey2nAc/5T7j3ueDf++ZHy4q08UpWc7daj+7tM6vDtBxPMbjqwN01Dv5RWn2/KZ4wXIbG867H69fCMVaA+iob6sfijXuQNpcsNFAEPc2VQfQpmCCH/2GV8jG+cvVA9WETQe7LefI7+RKP3uQzB9TbAXoqB+bDzaxAnTUG/vt/MxPHyuoxLvLblx8K0qKu9DCGBgHx2M8vjpAR72TXyctnmkUb6mNDRfA7yWm9wdH3pl/p/koAzrqg58DhA7FWj7fo75gszGeGoJP13IQbQom+JX8ULS7afanpQO2kgYHuxXnz29whSh4D2T6rIC8M/+V82ED6Kg39usleQfFVxcrOB6vkr84+5/z1y+5Pq0agXFwPMbjqwN01Dv4XXYlMW9jT+KqjdeB8+4n3sC/NBRrPaCjvq1+qL94aiaenikLtv4sCV4QxPM48elaFuw2BRP8KvzwlCJxsL5SPXB1bDrYbTnXfknx6yJ1Zhd0CCDvzH/jfJgAOuqN/c5Li1t10uwzaowqa+Mlo7R4Fy7YomUlMA6Ox3h8dYCOemu/tbVLzlR/Zy5pEq8NF8DvZaFY6wEd9W32Q/0Fdws2Xx6FeEFW9jMFZcE2e4qIBtAJtj05C+XHol35nbbBwW7FhfBL8tfdZX3rbKawEsg78288H3WAjnpjv15c3LyT5h8fi4E0ipfspMXfoQjSegwYB8djPL46QEe9td/5ydbtxJjfV47BJl4Tzr1fXLxYvVFOHZB35t96PnSAjvrg5wDoBHFmG/VXFuwz+PIoxAvynDkayoLd/HzOCkAn2PbkLKrf6Z04f97IgSzYeLBbcpH8xCfO/667tzLyzvy7zMcYoKPe2C/a2L4NxqkbP2gTr2SU5G9dXT1yLXaxC4yD4zEeXx2go97ar5cW9xJvLr5eHrtLvHVcAL/fEekKF5hpAB31bfaTNVcWbNTjylPhuBoNFV0WbFT6kGwDQEd9q/w6SfEUeTAbHOxWXFC/k7iYqXyqGHln/ieaDwnoqDf227lPdvVjVh3jlXwfPrmzq9asZzz2Eb+d14x30njHuAB+v8K0NQJ5Z/6t5qMK0FEf/BwAnaA8my0LNs50VxZrfKmNai4LNhiSbQDoqG+lXzfNH2twsFtx0f3w/XAUZz8t0oezTjOf3yjN7193Y5RJ491h9nncvhXj4HiMx1cH6Kg39kvT9Gpi3T6h6qI6P/Ge4tz7JfnDmbpGIO/Mv/F81AE66oOfA6CjHpQFG/W39iIztWBXV/YGCF2YvBb69fr5w8VBflJ7sFtSeqjUtTNly/3et9If3KvfXz+Lc7Gn84unjEVJ8SrNuHbpN97sil6aPePAoc0bmIyvCdBRbxTvTqEuHioK9afGx7ZDv/HOu192hcjVzzJ9jUDemX+j+WgCdNQHPwdAR730QcHGme3q+itehEgW7Opz5g2gz7SCCX6WgI76oddKmt0vSgffVzcDW9ZvHvacH7/Bh3vJ4JFpevgmvuaD1M7v8Cd6cfFznbj4om5ckmqckrp2plR8PrayMfiJqvGZADrqG+PtHMhvIvr/5bor30FlfLvUtTPlfPtll0fJ9k8whY1A3pn/xvkwAXTUBz8HQEe96tV8zRgaCKJg4zvsUKwNAB31c+UX9YtDO8+HHt0YTFi/edhzLv3i7JvdJP8DkcfDuBqf6W6EyL3R/OI2o1Fa/Izu3thlasenaWdKvV/2D6KIHhFDs9oXEBfjq4y32x1cXfRxNxHva8Qbkx/oxqSyPDZQ186Uc+73lV5/O2UqG4G8M/+V82ED6KgPfg6AjnrVz+zXWGgkWP3D7AZQP2/JWVq/Xnr8zmIT/qRy8DeyYfOw5iL44dMv7i6H77pxcdQw+Rog78y/dj4ETu/G26udNP9N4fm5cj86mozPhk1+UZp9JEqKx62sb9+eY64E4mJ8I/Ged96df6SbDu4g/B4q3pC8vpMW31b7qGPT+Gw5135x9j+dZPuOTHcjRO7ner+yBXTUL6afaOhUqAF0ws7cOi8BOuqDnwOgo77WD6cgRaF5r7pJVLF283DgAvt9euemHvkTBC9BEe6tb90qji+86eZm/4YXXLh5k/WDR263mm6tieJXdJPi0TvfT+df0HhVcoLxaWnrx9+Av0TwoVG8fdFOER5cD5+Y+/306vs3jtz8gvToufs2jh1eSQb3F35PF5q/Ee2/Ij1sWB4bqGtnyrn2i4t379vMbsjDuBE47nn81+4HpoCO+uDnAOio9+JnBXTCzrx0Dh31wc8B0FFv5HfgQP6jYgP4K3WzKLN283Bg8At+Ngx+ql/22nMPHboGD99G4Ljn8W+0HzQBOuqDnwOgo96LnxXQCTvz0jl01Ac/B0BHvZUfvofFo/fUTUOyfvOwZ/ALfjYMfqpf/lzTu5cBOO55/FvtB1WAjvrg5wDoqPfiZwV0ws68dA4d9cHPAdBR7+yH33F24uyHZpuHPYNf8LNh8Nvx27kYL3sID1Mj4Ljn8e+8H6iAjvrg5wDoqPfiZwV0ws68dA4d9cHPAdBRP7FfN8kOiU3iK1WbhyuDX/CzYfDb8Rte1JjkGzw8jYDjnsf/xPsBAB31wc8B0FHvxc8K6ISdeekcOuqDnwOgo96b377NrfPEp+0PlTcPV6o+wc+ewW9p/d6HJ7Lx0DQCjl8e/972A+qDnwOgo96LnxXQCTvz0jl01Ac/B0BHvXe//ZtHcf/m1yubhxPLG1Hws2PwW04//HLA5uIyQD1+FbZyf1EY/CwgtGa/5kIn7MxL59BRH/wcAB31U/Pr99ev3UnyJ4kN5KS6yZiyajNyZfALfjacU7+TUVo8URyS3m9SYwPoqA9+DoCOel9+8tbhzXo0EsQdWHx1HiZvjvyidHvQSbMvlzecOlZsRs4MfsHPhvPo14mzL/WS7aPDg9ICOE55vGqPX1tAR33wcwB01PvyQ7HGHUibCzYaCOLepuoA2hRM8NsDv87GxbfsJMU7dZtPmbrNSNfOlMEv+NlwHv2iNHvHanr8ZjzcjIHjlMdr7fFrCuioD34OgI56X34o1vL5HvUFm43x1BB8upaDaFMwwW8P/brdS68sNpzfKW9AKnWbka6dKYNf8LPhPPp10vy3bO5JL4HjlMer0fHbBOioD34OgI56X36ov3hqJp6eKQu2/qsSvCCI53Hi07Us2G0KJvjNyK+TZBfrHh6i24zKbWwY/IKfDefQ76u9JC94WFkBxymPV+vjVwfoqA9+DoCOep9+qL/gbsHmy6MQL8jKfqagLNhmTxHRADrBticn+Fn49dYvvnU3yf61ZjMa27BsGPyCnw3nzS9KB/96fprfmoeTFXCc8nh1Pn5VQEd98HMAdNT79MOZbdRfWbD1Z2DEC/KcORrKgt38fM4KQCfY9uQEPwc/nCLvJMVTe+ngivKGpNu0TFn2Cn52DH7t9YviwQ/Fv0/f2EivwsPICjhOebxOfPwC0FEf/BwAHfU+/WTNlQUb9bjyVDiuRkNFlwUblT4k2wDQUb9Ufqvp4Ggvzj4pNyXdpmVK6aFS186UwS/42XCaft00/8TqxrGLJj3eBL0ev/QJfpaAjnqffvJstizYONNdWazxpTaquSzYYEi2AaCjfin9kuTCm/WS7E91m5Yp1c3t1Canb2vC4Bf8bDhNv5U0f3maHr6Jr+ONbO1+QAY/Q0BHPSgLNupv7UVmasGuruwNELoweUvq14mLnxSblfUzj9XNTVLXzpTBL/jZcFp+UTr4ymp/8NPy+OAhZAXoqB873tjECtBRH/wcAB310/DDvyjYOLNdXX/FixDJgl19zrwB9JlWMMHPEtBRv2d+521u3bSTFK/XbWQ6qpukpK6dKYNf8LPh9PyyN+07ePiO5ePDBtBRX3m82QA66oOfA6Cjflp++O/ma8bQQBAFG99hh2JtAOioD34adNLte+LuTbpNTVLdJCV17UwZ/IKfDafhF8X5l1bS7H5Nx0cToKPe6HhrAnTUBz8HQEf9NP3Mfo2FRoLVP8xuAPXzlpzgZwjoqLfyO3f/8RuIjeyV5Y0NLG+UoK6dKYNf8LPhNPxW0sGf70+3bmN6fFQBOuqtjrcqQEd98HMAdNS3w080dCrUADphZ26dlwAd9cHPAdBR3xq/KMmPd+Lis+rmVqa6+dky+AU/G07B7zOrG1v3cD0+VEBHvfPxpgI66oOfA6CjvpV+VkAn7MxL59BRH/wcAB31rfO7y/rW2VGa/77Y3E6qGyWo2wRNWfYKfnYMfpP4ZVf00uzFBzYP32LS4wOAjvqJjzcAOuqDnwOgo76VflZAJ+zMS+fQUR/8HAAd9a32W0kHh7rJ4D8m3yxDsQl+dvTp10mL9+xLsg0eF96OD/oEP0tAR/1S+FkBnbAzL51DR33wcwB01M+F3+HDB6+z0s8epbsnuSl9br5g8At+Royzr0ZJ8eD19SNnyfVMtvZ4I4OfIaCjvpV+VkAn7MxL59BRH/wcAB31c+e3kh67kdgEXza2KTbQ2+ZLBr/gZ8IoLf7w/APFjbh+x9Yzl7wVoKM++DkAOuqXws8K6ISdeekcOuqDnwOgo36u/Xr97bSTFv+m2yTL9LX5Sga/4NfI4YNuttexbrl+a9ezKaCjPvg5ADrql8LPCuiEnXnpHDrqg58DoKN+Qfwuu5L4BPMzYoP89NiGSXrZfBUGv+BXxyjO/q+XFvcSixN3ggzHb/AzBnTUe/EDhNbs11zohJ156Rw66oOfA6CjfuH8ut3B1aMkv6wbF99SN89JN98yg1/wq2ScfbOb5k9YW7vkTKxJrFuuX+v1rAN01Ac/B0BH/bL4yVuHN+vRSBB3YPHVeZi84NeIzoH8JqJov3jnpzMTbL4aBr/gp2d2RSfJXoRrK7gMw/Eb/KwAHfW+/FCscQfS5oKNBoK4t6k6gDYFE/wW3G+tn+0TG+nr7TdfPVWf4GfPBfU7GSX5n52/np/DpTcE1h/Xr7f1TH3wcwB01C+LH4q1fL5HfcFmYzw1BJ+u5SDaFEzwWyK/1f5Wv5cM3qzZbI3puJlXMvjNv1+U5m+M4kHEZbeL8vojW3t8kMHPENBR31Y/1F88NRNPz5QFW/8dNl4QxPM48elaFuw2BRP8ltSvE+eJ+MT9dt3mW0eXzbyOwW++/cQn6rd2+9kFXHIjwDrjehtbf2xiBeioD34OgI76ZfJD/QV3CzZfHoV4QVb2MwVlwTZ7iogG0Am2PTnBb878onj7IlG8/0G3GZdpu5k3MfjNrx8KtXj9Qi6jMWCdcb3Vrj9TQEd98HMAdNQvkx/ObKP+yoJ9Bl8ehXhBnjNHQ1mwm5/PWQHoBNuenOA3x35RUsTduPgr3eYMljdyUNfOlMFvPv3wjPZeXBzgstEC64zrzXj91QE66oOfA6Cjfpn8ZM2VBRv1uPJUOK5GQ0WXBRuVPiTbANBRH/wcAB31Tn69JO+I4v0qXOkrN2nTzdyUwW/e/LAWsj89f+P4eVwmlcA643pzWn9lQEd98HMAdNQvk588my0LNs50VxZrfKmNai4LNhiSbQDoqA9+DoCO+on9ehv5j4tP3C+K0sF36jdzO5a9gp8d99KvkxbfjtLs+ecnW7fjsqgF1hnX28TrD4CO+uDnAOioXzY/UBZs1N/ai8zUgl1d2RsgdGHygp8xoKPeq9/6waM/1kuzX+3F+WfUzdyF5cIQ/Oy4V364M1k3zR+ztnbJdbgcGiHXi6DX9Uef4GcJ6KhfRj/8i4KNM9vV9Ve8CJEs2NXnzBtAn2kFE/wsAR31S+vX7x+7Tjfdvkcnzf6lvPGbsKo4uDL4TcEvLv65Exc/maap/uKcCujWC/8Ox5sBoKM++DkAOupVr+ZrxtBAEAUb32GHYm0A6KgPfg6Ajvo984vSbK2b5H+AU6a6YlCmtjho2pky+Hn0i/Nvi39f2ouLNU6vFbAuuD4q14sNoKM++DkAOuqX2c/s11hoJFj9w+wGUD9vyQl+hoCO+oXw61544tpRUjxIFO9/1xUGcKQ4kLp2pgx+fvyiNH/fSn/wsDi+8KZV89sE6Lg+jNZLE6CjPvg5ADrqg58JREOnQg2gE3bm1nkJ0FEf/BwAHfXBzwDdeHsV948Wn7q/US4OKtXiYcvgN5mfKNLfWEnyl+Bud7bzWwZ01DutlzKgoz74OQA66oPftIFO2JmXzqGjPvg5ADrqg58l8GQmUVjuHiX5m6I4/4GvYqP6BD9zdtLs+900e91Kmv/05mb/hpPOLwAd9ROvFwA66oOfA6CjPvhNG+iEnXnpHDrqg58DoKM++DkAOuqvfcHG4Vuv9LceJYr3u3XFxJS+ipfkUvjFxT/j64p9B/Pry/lQ2Mr1ojD4GQI66oPftIFOBPEFOTqUNPvCXAPoqA9+DoCO+uDnAOioH/M7Ly1u1U3yh3eS4h9FQTk5VmAq6KV4KVxcv+yKnVvM5g9DrpvmYzhhloCO+uDnAOioD34OgI56L35WQCeCuPQcHUo2X4peAeioD34OgI764OcA6Khv9Ivi4zeO0u0HRnHxt524+IG+AIVi3eSH092dNP9r8Wn65/ZtZjdkeodA3pn/xvkwAXTUBz8HQEd98HMAdNR78bMCOhHEj7rRoeSkty8NfsHPCNBRP3M/XGkuPhVeEqX57+3csGOnGNkWryYuip8o0p8UBfqFnSS7eHX1yLWYxhEg78y/9XzoAB31wc8B0FEf/BwAHfVe/KwgOpGP3MQ9TiWrb5/WAOioD34OgI764OcA6Kj34tdLj98Fd9nqxvlbRBHHb4RF8RovXDaEfrQY6tuZck/94uJb+BQdJcUjVjayOzFNlUDemX8v8wEd9cHPAdBRH/wcAB31XvwAYy0aCuJ2pehQcpLblwa/4GcM6KifC784Lq7Z6w/iblL8Ip4S1Y2zr6mFzpQ7BXGHutdtOVW/OPtqN8lfJ96sPCrq5/ts7jpWzh/Z2vklg58hoKM++DkAOkHcJ6X5kzkby3uMS05y+9LgF/yMAR31c+x32ZXwxKgoLe6Hh1J00uI9Oz9bGi+C88Hs8l6SvaebDl7YTfMHdONBdP3rP0T/gP0GIE/MV03+zAEd9cHPAdBRH/wcAB31Pv1wB9Lmgq00RoeSk9y+NPgFP2NAR/3C+d32yJGr9pLBSpRsXyqK4O8MTx8r34W3hfjueXiBmBgjxirecOxbWRnge7iZ5k8H6KgPfg6Ajvrg5wDoqJ+Gn1HBRqMR8iUnBL/gZ4Nl9Ftf3zprJdna10uLnxKfXh8bJcXviqL5BvHvf6p3Y/NF4fn1He/sDZ04f97we/h0++54M3FOeuKaHNoQ5VhBvuSE4Bf8bBD8ago/XhTEVW6STu8SJIJf8LNB8NP7nXPOiavg52W99Pide/3tFFddi+J+r+Hp9jh/sCi4D9spuvljBR8u/t9DxGv3F23uPWwrNNDCA160bYTp+EwR/IKfDYIf/E477f8HaOBVPQfksOYAAAAASUVORK5CYII=');


