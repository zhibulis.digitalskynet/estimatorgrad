CREATE TABLE public.blockers (
	blockerid serial NOT NULL,
	taskid int4 NULL,
	taskblockerid int4 NULL,
	CONSTRAINT blockers_pkey PRIMARY KEY (blockerid),
	CONSTRAINT blockers_blockerid_fkey FOREIGN KEY (taskblockerid) REFERENCES tasks(taskid) ON DELETE CASCADE,
	CONSTRAINT blockers_taskid_fkey FOREIGN KEY (taskid) REFERENCES tasks(taskid) ON DELETE CASCADE
);