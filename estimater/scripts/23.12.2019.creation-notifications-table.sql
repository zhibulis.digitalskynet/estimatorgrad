CREATE TABLE public.notifications (
	id serial NOT NULL,
	userid int4 NULL,
	projectid int4 NULL,
	isconfirmed bool NULL DEFAULT false,
	isdeleted bool NULL DEFAULT false,
	description text NULL,
	createddate date NULL,
	updateddate date NULL,
	CONSTRAINT notifications_pkey PRIMARY KEY (id),
	CONSTRAINT notifications_fk FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE,
	CONSTRAINT notifications_fk_1 FOREIGN KEY (projectid) REFERENCES projects(projectid) ON DELETE CASCADE
);
