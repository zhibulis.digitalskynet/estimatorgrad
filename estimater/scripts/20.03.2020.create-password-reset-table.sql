CREATE TABLE resetpassword (
	id serial NOT NULL,
	userid int4 NULL,
	passwordresettoken varchar NULL,
	CONSTRAINT passwordreset_pkey PRIMARY KEY (id),
	CONSTRAINT resetpassword_fk FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE
);