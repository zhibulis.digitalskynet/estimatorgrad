create table notes(
	noteid serial NOT NULL,	
	note varchar,
	projectid int4 not null,
	CONSTRAINT notes_pkey PRIMARY KEY (noteid),
	CONSTRAINT notes_projectid_fkey FOREIGN KEY (projectid) REFERENCES projects(projectid) ON DELETE CASCADE
)