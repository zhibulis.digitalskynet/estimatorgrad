const estimateChartController = require('../controllers/estimateChartController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.get('/api/estimate-chart/tasks/:projectId/get', estimateChartController.table_get);
  app.get('/api/estimate-chart/get-projects/:idToken', getJwtKey.jwtKey, estimateChartController.projects_list_get);
  app.get('/api/estimate-chart/get-all-projects', estimateChartController.projects_all_list_get);
};
