const homeContoller = require('../controllers/homeController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.post('/api/home/check-projects/:idtoken', getJwtKey.jwtKey, homeContoller.check_projects_post);
};
