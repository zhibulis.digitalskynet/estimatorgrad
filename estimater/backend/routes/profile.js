const controller = require('../controllers/profileController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.get('/api/profile/member-info/:idToken/get', getJwtKey.jwtKey, controller.member_info_get);
  app.get('/api/profile/tasks/:idToken/get', getJwtKey.jwtKey, controller.tasks_get);
  app.get('/api/profile/projects/:idToken/get', getJwtKey.jwtKey, controller.projects_count_get);
  app.put('/api/profile/avatar/:idToken/put', getJwtKey.jwtKey, controller.set_profile_avatar_put);
};
