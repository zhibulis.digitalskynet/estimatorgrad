/* eslint-disable global-require */
module.exports = (app) => {
  require('./login')(app);
  require('./dashboard')(app);
  require('./home')(app);
  require('./registration')(app);
  require('./estimator-table')(app);
  require('./create-project')(app);
  require('./header')(app);
  require('./members')(app);
  require('./estimate-chart')(app);
  require('./project-settings')(app);
  require('./user-in-project')(app);
  require('./dashboard-statistic')(app);
  require('./dashboard-project-statistic')(app);
  require('./profile')(app);
  require('./password-reset')(app);
};
/* eslint-enable global-require */
