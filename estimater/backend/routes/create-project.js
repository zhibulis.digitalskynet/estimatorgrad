const createProjectController = require('../controllers/createProjectController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.post('/api/create-project', getJwtKey.jwtKey, createProjectController.create_project_post);
  app.post('/api/create-project/delete-exist', getJwtKey.jwtKey, createProjectController.project_delete_create_new_post);
  app.post('/api/create-project/restore', getJwtKey.jwtKey, createProjectController.project_restore_post);
  app.get('/api/create-project/get-status', getJwtKey.jwtKey, createProjectController.project_status_get);
};
