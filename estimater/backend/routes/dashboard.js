const dashboardContoller = require('../controllers/dashboardController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.post('/api/dashboard-data/:idToken', getJwtKey.jwtKey, dashboardContoller.dasboard_getData_post);
  app.delete('/api/dashboard/delete-project/:projectId', dashboardContoller.dasboard_deleteProject_delete);
  app.delete('/api/dashboard/user-project/:idToken/:currentProjectManagerId/:projectId/delete', getJwtKey.jwtKey, dashboardContoller.leave_project_delete);
  app.get('/api/dashboard/get-projects', dashboardContoller.dashboard_all_projects_get);
  app.get('/api/dashboard/load-users/get', dashboardContoller.load_users_get);
};
