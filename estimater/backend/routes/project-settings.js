const projectSettingController = require('../controllers/projectSettingsController');

module.exports = (app) => {
  app.get('/api/project-settings/:projectId', projectSettingController.project_info_get);
  app.get('/api/project-settings/project-statuses/get/:projectId', projectSettingController.project_status_check_completed_get);
  app.get('/api/project-settings/project-managers/get', projectSettingController.project_managers_get);
  app.get('/api/project-settings/milestones/get', projectSettingController.milestone_statuses_get);
  app.get('/api/project-settings/milestones/get-all/:projectId', projectSettingController.milestones_get);
  app.get('/api/project-settings/notes/get/:projectId', projectSettingController.notes_get);
  app.get('/api/project-settings/blockers/get-all/:projectId', projectSettingController.blockers_get);
  app.get('/api/project-settings/members/get-all/:projectId', projectSettingController.members_get);
  app.get('/api/project-settings/check-project-exist/:projectId', projectSettingController.project_exist_get);
  app.put('/api/project-settings/milestones/insert', projectSettingController.create_milestone_put);
  app.put('/api/project-settings/notes/insert', projectSettingController.create_note_put);
  app.post('/api/project-settings/update', projectSettingController.update_project_info_post);
  app.post('/api/project-settings/blockers/insert', projectSettingController.add_task_blocker);
  app.post('/api/project-settings/blockers/delete', projectSettingController.delete_task_blocker_post);
  app.post('/api/project-settings/notes/delete', projectSettingController.delete_notes_post);
  app.post('/api/project-settings/milestones/delete', projectSettingController.milestones_delete);
  app.post('/api/project-settings/members/delete/:projectId/:projectManagerId', projectSettingController.members_delete);
};
