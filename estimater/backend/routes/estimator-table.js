const tableContoller = require('../controllers/estimatorTableController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.get('/api/table/working-hours/:projectId', tableContoller.available_working_hours_get);
  app.get('/api/table/:projectId', tableContoller.table_get);
  app.get('/api/table/milestones/:projectId/get', tableContoller.milestones_get);
  app.get('/api/table/get-project-name/:projectId', tableContoller.projectName_get);
  app.get('/api/table/get-users/:projectId', tableContoller.get_users_by_projectId);
  app.get('/api/table/:token/:projectId', getJwtKey.jwtKey, tableContoller.check_user_project_get);
  app.put('/api/table/create-task', tableContoller.create_task_put);
  app.put('/api/table/project-status/:projectId/update', tableContoller.project_status_update);
  app.post('/api/table/update-start-date', tableContoller.update_start_date_post);
  app.post('/api/table/switch-tasks', tableContoller.switch_tasks_post);
  app.post('/api/table/delete-task', tableContoller.delete_tasks_post);
  app.post('/api/table/change-task', tableContoller.edit_task_post);
  app.post('/api/table/add-blockers', tableContoller.blockers_on_task_post);
  app.put('/api/table/import-table', tableContoller.create_inserted_tasks);
};
