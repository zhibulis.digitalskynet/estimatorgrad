const headerController = require('../controllers/headerController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.get('/api/header/:token', getJwtKey.jwtKey, headerController.user_data_get);
  app.get('/api/header/get-notification/:idToken', getJwtKey.jwtKey, headerController.notification_count_get);
  app.get('/api/header/get-role/:token', getJwtKey.jwtKey, headerController.role_name_get);
};
