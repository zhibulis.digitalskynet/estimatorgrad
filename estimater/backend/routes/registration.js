const registrationContoller = require('../controllers/registrationController');

module.exports = (app) => {
  app.post('/api/registration', registrationContoller.registration_post);
  app.post('/api/registration/check-username/:username', registrationContoller.check_exist_username_post);
  app.post('/api/registration/check-email/:email', registrationContoller.check_exist_email_post);
  app.get('/api/registration/roles', registrationContoller.registration_roles_get);
};
