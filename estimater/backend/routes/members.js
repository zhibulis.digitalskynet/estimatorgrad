const membersContoller = require('../controllers/membersConroller');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.get('/api/members/get-all', membersContoller.all_members_get);
  app.get('/api/members/get-roles', membersContoller.roles_get);
  app.post('/api/members/delete-members', membersContoller.delete_users_post);
  app.put('/api/members/add-member', getJwtKey.jwtKey, membersContoller.add_members_put);
};
