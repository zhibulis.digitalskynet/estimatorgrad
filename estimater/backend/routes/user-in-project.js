const userInProjectController = require('../controllers/usersInProjectController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.post('/api/user-in-project/invite-user-in-project', getJwtKey.jwtKey, userInProjectController.invite_user);
  app.post('/api/user-in-project/delete-user-from-project', userInProjectController.delete_user_from_project);
};
