const controller = require('../controllers/passwordResetController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.get('/api/password-reset/generate-token/:email/get', getJwtKey.jwtKey, controller.generate_reset_password_token);
  app.get('/api/password-reset/check-token/:resetPasswordToken/get', getJwtKey.jwtKey, controller.check_token_valid);
  app.post('/api/password-reset/set-new-password/post', getJwtKey.jwtKey, controller.reset_password);
};
