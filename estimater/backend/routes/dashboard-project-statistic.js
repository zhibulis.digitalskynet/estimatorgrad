const controller = require('../controllers/dashboardProjectStatisticController');

module.exports = (app) => {
  app.get('/api/dashboard-project-statistic/project-name/:projectId/get', controller.project_name_get);
  app.get('/api/dashboard-project-statistic/pie-data/:projectId/get', controller.pie_data_get);
  app.get('/api/dashboard-project-statistic/tasks-hours/:projectId/get', controller.tasks_hours_widget_get);
};
