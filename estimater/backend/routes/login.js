const loginContoller = require('../controllers/loginController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.post('/api/login', getJwtKey.jwtKey, loginContoller.login_post);
};
