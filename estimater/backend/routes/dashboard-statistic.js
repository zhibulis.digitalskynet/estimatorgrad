const controller = require('../controllers/dashboardStatisticController');
const getJwtKey = require('../middleware/getJwtKey');

module.exports = (app) => {
  app.get('/api/dashboard-statistic/get', controller.dashboard_statistic_get);
  app.get('/api/dashboard-statistic/pie-data/get', controller.pie_data_get);
  app.get('/api/dashboard-statistic/project-status/get', controller.projects_status_widget_get);
  app.get('/api/dashboard-statistic/tasks-hours/get', controller.tasks_hours_widget_get);
  app.get('/api/dashboard-statistic/:idToken/get', getJwtKey.jwtKey, controller.dashboard_statistic_where_exist_get);
};
