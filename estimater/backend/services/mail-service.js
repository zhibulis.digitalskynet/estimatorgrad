const config = require('config');
const nodemailer = require('nodemailer');

const transportConfig = config.get('Estimator.mailerConfig');

exports.sendInviteMail = (email, username, setPasswordToken) => {
  const transporter = nodemailer.createTransport(transportConfig);
  const mailOptions = {
    from: transportConfig.auth.user,
    to: email,
    subject: 'Welcome to Estimator!',
    text: `Dear ${username}\n\n`
      + 'Welcome to Estimator!\n\n'
      + 'Follow link below to set your password\n'
      + `Username : ${username}\n`
      + `https://estimator.k8s.digitalskynet.com/password-initialization/${setPasswordToken}\n`
      + 'If you have any questions about your account or any other matter, please feel free to contact us!\n'
      + 'https://estimator.k8s.digitalskynet.com',
  };
  transporter.sendMail(mailOptions);
};

exports.sendPasswordResetUrl = (token, email, username) => {
  const transporter = nodemailer.createTransport(transportConfig);
  const mailOptions = {
    from: transportConfig.auth.user,
    to: email,
    subject: 'Estimator password reset',
    text: `Dear ${username}\n\n`
      + 'Please, follow url below to set new password\n'
      + 'Notice that url would be active only one hour\n'
      + `https://estimator.k8s.digitalskynet.com/forgot-password-reset/${token}`,
  };
  transporter.sendMail(mailOptions);
};

exports.sendInviteToProjectMail = (email, message) => {
  const transporter = nodemailer.createTransport(transportConfig);
  const mailOptions = {
    from: transportConfig.auth.user,
    to: email,
    subject: 'Estimator',
    text: message,
  };
  transporter.sendMail(mailOptions);
};
