const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models').user;
const Role = require('../models').role;

function sendError(errorText, res) {
  res.json({
    error: errorText,
    success: false,
  });
}

function checkPass(req, user, res) {
  return bcrypt.compare(req.body.password, user.password).then((valid) => {
    if (valid) {
      if (user.isConfirmed) {
        const payload = { subject: user.userId };
        const token = jwt.sign(payload, res.jwtKey, {
          expiresIn: 43200,
        });
        res.json({
          token,
          roleName: user.role.roleName,
          success: true,
        });
      } else {
        throw new Error('Your account still unconfirmed by administrator');
      }
    } else {
      throw new Error('Invalid username or password');
    }
  });
}

exports.login_post = (req, res) => {
  User.findOne({
    where: {
      username: req.body.username,
    },
    attributes: ['isConfirmed', 'userId', 'password'],
    include: {
      model: Role,
      attributes: ['roleName'],
    },
  })
    .then((user) => {
      if (!user) {
        throw new Error('Invalid username or password');
      } else {
        return user;
      }
    })
    .then((user) => checkPass(req, user, res))
    .catch((err) => {
      sendError(err.message, res);
    });
};
