const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const path = require('path');
const fs = require('fs');
const Role = require('../models').role;
const User = require('../models').user;
const UserImage = require('../models').image;

const saltRounds = 10;

function createUser(req, hash) {
  return User.create({
    username: req.body.username,
    password: hash,
    email: req.body.email,
    roleId: req.body.roleId,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    registerDate: req.body.registerDate,
  }, {
    fields: [
      'username',
      'password',
      'email',
      'roleId',
      'firstName',
      'lastName',
      'registerDate',
    ],
  });
}

function insertImage(userId) {
  const image = fs.readFileSync(path.resolve(__dirname, '../images/default-avatar.png'), 'base64');
  return UserImage.create({
    userId,
    image,
  }, {
    fields: ['userId', 'image'],
  });
}

async function hashPassword(password) {
  const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
      if (err) reject(err);
      resolve(hash);
    });
  });
  return hashedPassword;
}

exports.registration_roles_get = (req, res) => {
  Role.findAll({})
    .then((roles) => {
      const resRoles = roles.filter((role) => role.roleName !== 'Admin' && role.roleName !== 'Project Manager');
      res.json(resRoles);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.registration_post = (req, res) => {
  hashPassword(req.body.password)
    .then((hash) => createUser(req, hash))
    .then((user) => insertImage(user.userId))
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
};

exports.check_exist_username_post = (req, res) => {
  User.findOne({
    where: Sequelize.where(
      Sequelize.fn('lower', Sequelize.col('username')),
      Sequelize.fn('lower', req.params.username),
    ),
  })
    .then((user) => {
      res.json(!!user);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.check_exist_email_post = (req, res) => {
  User.findOne({
    where: Sequelize.where(
      Sequelize.fn('lower', Sequelize.col('email')),
      Sequelize.fn('lower', req.params.email),
    ),
  })
    .then((user) => {
      res.json(!!user);
    })
    .catch((err) => {
      throw err.message;
    });
};
