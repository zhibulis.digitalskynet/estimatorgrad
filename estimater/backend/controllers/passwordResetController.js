const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models').user;
const PasswordReset = require('../models').passwordReset;
const { sendPasswordResetUrl } = require('../services/mail-service');

const saltRounds = 10;

async function checkNoteAlreadyExist(userId) {
  return PasswordReset.findOne({
    where: {
      userId,
    },
    attributes: ['userId'],
  });
}

exports.generate_reset_password_token = (req, res) => {
  User.findOne({
    where: {
      email: req.params.email,
    },
    attributes: ['userId', 'firstName', 'lastName'],
  })
    .then(async (user) => {
      if (!user) {
        return true;
      }
      const noteExist = await checkNoteAlreadyExist(user.userId);
      const payload = { subject: req.params.email };
      const passwordResetToken = jwt.sign(payload, res.jwtKey, {
        expiresIn: 3600,
      });
      const fullName = `${user.firstName} ${user.lastName || ''}`;
      sendPasswordResetUrl(passwordResetToken, req.params.email, fullName);
      if (noteExist) {
        return PasswordReset.update({
          passwordResetToken,
        }, {
          where: {
            userId: user.userId,
          },
        });
      }
      return PasswordReset.create({
        userId: user.userId,
        passwordResetToken,
      }, {
        fields: [
          'userId',
          'passwordResetToken',
        ],
      });
    })
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
};

async function hashPassword(password) {
  const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
      if (err) reject(err);
      resolve(hash);
    });
  });
  return hashedPassword;
}

function deleteNote(passwordResetToken) {
  return PasswordReset.destroy({
    where: {
      passwordResetToken,
    },
  });
}

exports.reset_password = (req, res) => {
  PasswordReset.findOne({
    where: {
      passwordResetToken: req.body.passwordResetToken,
    },
    attributes: ['userId'],
  })
    .then(async (note) => {
      if (!note) {
        throw new Error('Invalid Action');
      }
      return hashPassword(req.body.password)
        .then((hashedPassword) => ({ hashedPassword, userId: note.userId }))
        .catch((err) => {
          throw err;
        });
    })
    .then((updateData) => User.update({
      password: updateData.hashedPassword,
    }, {
      where: {
        userId: updateData.userId,
      },
    }))
    .then(async() => {
      await deleteNote(req.body.passwordResetToken);
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
};

exports.check_token_valid = (req, res) => {
  jwt.verify(req.params.resetPasswordToken, res.jwtKey, (errVerify) => {
    if (errVerify) {
      deleteNote(req.params.resetPasswordToken)
        .then(() => {
          res.json({
            error: true,
          });
        })
        .catch((err) => {
          res.json({
            error: true,
            errorMessage: err.message,
          });
        });
    } else {
      PasswordReset.findOne({
        where: {
          passwordResetToken: req.params.resetPasswordToken,
        },
        attributes: ['userId'],
      })
      .then((note)=> {
        if(!note){
          throw new Error('Invalid');
        } else {
          res.json({
            error: false,
          });
        }
      })
      .catch((err)=> {
        res.json({
          errorMessage: err.message,
          error: true,
        });
      });
    }
  });
};
