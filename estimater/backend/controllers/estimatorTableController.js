const { Op } = require('sequelize');
const jwt = require('jsonwebtoken');
const UserProject = require('../models').userProject;
const Task = require('../models').task;
const TaskStatus = require('../models').taskStatus;
const User = require('../models').user;
const Role = require('../models').role;
const Milestone = require('../models').milestone;
const Project = require('../models').project;
const Blocker = require('../models').blocker;

exports.check_user_project_get = (req, res) => {
  jwt.verify(req.params.token, res.jwtKey, (err, decoded) => {
    UserProject.findOne({
      where: {
        userId: decoded.subject,
        projectId: req.params.projectId,
      },
    })
      .then((userProject) => {
        res.json(!!userProject);
      });
  });
};

exports.get_users_by_projectId = (req, res) => {
  Project.findByPk(req.params.projectId, {
    include: {
      model: User,
      attributes: [
        'userId',
        'firstName',
        'lastName',
      ],
      include: {
        model: Role,
        attributes: ['roleName'],
      },
    },
  })
    .then((usersProject) => {
      res.json(usersProject.users);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.delete_tasks_post = (req, res) => {
  // req.body contains tasks id array (multiple delete here)
  Task.destroy({
    where: {
      taskId: req.body,
    },
  })
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
};

exports.create_inserted_tasks = (req, res) => {
  Task.bulkCreate(req.body, {
    fields: [
      'taskName',
      'optimisticTime',
      'pessimisticTime',
      'dateStart',
      'projectId',
      'taskOrder',
      'completed',
      'taskStatusId',
    ],
  })
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
}

exports.create_task_put = (req, res) => {
  Task.findOne({
    where: {
      [Op.and]: [
        { taskName: req.body.taskName },
        { projectId: req.body.projectId },
      ],
    },
  })
    .then((task) => {
      if (task) {
        throw new Error('Task with this name already exist!');
      }
    })
    .then(() => {
      const defaultTaskStatusId = 2;
      return Task.create({
        dateStart: req.body.dateStart,
        optimisticTime: req.body.optimisticTime,
        pessimisticTime: req.body.pessimisticTime,
        taskName: req.body.taskName,
        projectId: req.body.projectId,
        completed: req.body.completed,
        taskOrder: req.body.taskOrder,
        taskStatusId: defaultTaskStatusId,
      }, {
        fields: [
          'dateStart',
          'optimisticTime',
          'pessimisticTime',
          'taskName',
          'projectId',
          'completed',
          'taskOrder',
          'taskStatusId',
        ],
      });
    })
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
};

function findTaskStatus(name, taskStatuses) {
  return taskStatuses.find((taskStatus) => taskStatus.taskStatusName === name).taskStatusId;
}

function completedColumnUpdate(req, taskStatuses) {
  let taskStatusName;
  if (+req.body.value === 100) taskStatusName = 'closed';
  else {
    taskStatusName = req.body.assignedId ? 'doing' : 'backlog';
  }
  const taskStatusId = findTaskStatus(taskStatusName, taskStatuses);

  return Task.update({
    [req.body.field]: req.body.value,
    taskStatusId,
  }, {
    where: {
      taskId: req.body.taskId,
    },
  });
}

function defaultTaskUpdate(req) {
  return Task.update({
    [req.body.field]: req.body.value,
  }, {
    where: {
      taskId: req.body.taskId,
    },
  });
}

exports.edit_task_post = (req, res) => {
  TaskStatus.findAll()
    .then((taskStatuses) => {
      if (req.body.field === 'completed') {
        return completedColumnUpdate(req, taskStatuses);
      }
      return defaultTaskUpdate(req);
    })
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
};

exports.table_get = (req, res) => {
  Project.findByPk(req.params.projectId, {
    order: [
      [{ model: Task, as: 'tasks' }, 'taskOrder', 'ASC'],
    ],
    include: {
      model: Task,
      as: 'tasks',
      include: [{
        model: Blocker,
        as: 'taskBlockers',
      }, {
        model: Milestone,
        attributes: ['id', 'name'],
      }],
    },
  })
    .then((tasksWithBlockers) => {
      res.json(tasksWithBlockers.tasks);
    })
    .catch((err) => {
      throw err.message;
    });
};

function insertBlockersOnTask(taskId, taskBlockersId) {
  const bulkCreateArray = [];
  taskBlockersId.forEach((taskBlockerId) => {
    bulkCreateArray.push({ taskId, taskBlockerId });
  });
  return Blocker.bulkCreate(bulkCreateArray, {
    fields: ['taskId', 'taskBlockerId'],
  });
}

function deleteBlockersOnTask(taskId) {
  return Blocker.destroy({
    where: {
      taskId,
    },
  });
}

function editBlockersOnTask(taskId, blockersId) {
  return deleteBlockersOnTask(taskId)
    .then(() => {
      if (blockersId.length) {
        return insertBlockersOnTask(taskId, blockersId);
      }
      return 0;
    })
    .catch((error) => {
      throw error.message;
    });
}

exports.blockers_on_task_post = (req, res) => {
  editBlockersOnTask(req.body.taskId, req.body.blockersId)
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((error) => {
      res.json({
        error: true,
        errorMessage: error,
      });
    });
};

exports.projectName_get = (req, res) => {
  Project.findByPk(req.params.projectId, {
    attributes: ['projectName'],
  })
    .then((project) => {
      res.json(project.projectName);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.available_working_hours_get = (req, res) => {
  Project.findByPk(req.params.projectId, {
    include: {
      model: User,
      attributes: ['workingHours'],
      include: {
        model: Role,
        attributes: ['roleName'],
      },
    },
  })
    .then((projectUsers) => {
      let workingHours = 0;
      projectUsers.users.forEach((user) => {
        if (user.role.roleName === 'Developer') {
          workingHours += user.workingHours;
        }
      });
      res.json(workingHours);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.switch_tasks_post = (req, res) => {
  Task.bulkCreate(req.body, {
    fields: ['taskId', 'taskOrder', 'projectId'],
    updateOnDuplicate: ['taskOrder'],
  })
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
};

exports.update_start_date_post = (req, res) => {
  Project.update({
    startDate: req.body[0].dateStart,
  }, {
    where: {
      projectId: req.body[0].projectId,
    },
  })
    .then(() => Task.bulkCreate(req.body, {
      fields: ['taskId', 'dateStart', 'endStart', 'projectId'],
      updateOnDuplicate: ['dateStart', 'endStart'],
    }))
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: true,
        errorMessage: err.message,
      });
    });
};

exports.milestones_get = (req, res) => {
  Milestone.findAll({
    where: {
      projectId: req.params.projectId,
    },
  })
    .then((milestones) => {
      res.json(milestones);
    })
    .catch((err) => {
      throw err;
    });
};

exports.project_status_update = (req, res) => {
  Project.update({
    projectStatusId: req.body.statusId,
  }, {
    where: {
      projectId: req.params.projectId,
    },
  })
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: false,
        errorMessage: err.message,
      });
    });
};
