const jwt = require('jsonwebtoken');
const ProjectStatus = require('../models').projectStatus;
const Project = require('../models').project;
const UserProject = require('../models').userProject;
const Role = require('../models').role;
const UserRole = require('../models').userRole;

function makeUsersProjectsNote(userId, projectId) {
  return UserProject.create({
    userId,
    projectId,
  }, {
    fields: ['userId', 'projectId'],
  });
}

async function addAdminToProject(projectId) {
  return Role.findOne({
    where: {
      roleName: 'Admin',
    },
    include: {
      as: 'usersRoles',
      model: UserRole,
      attributes: ['userId'],
    },
  })
    .then((adminInfo) => makeUsersProjectsNote(adminInfo.usersRoles.userId, projectId))
    .catch((err) => {
      throw err;
    });
}

function createProject(projectName, projectStatusId) {
  return Project.create({
    projectName,
    projectStatusId,
  }, {
    fields: [
      'projectName',
      'projectStatusId',
    ],
  });
}

function sendAnswer(errFlag, message, res) {
  res.json({
    error: errFlag,
    status: message === 'Project deleted, but u can restore it or create new' ? 410 : 501,
    errorMessage: message,
  });
}

function checkProjectDeletedOrNot(projectName) {
  return Project.findAll({
    where: {
      projectName,
    },
  });
}

exports.create_project_post = (req, res) => {
  jwt.verify(req.body.userIdToken, res.jwtKey, (errVerify, decoded) => {
    checkProjectDeletedOrNot(req.body.projectName)
      .then((projects) => {
        if (errVerify) {
          throw new Error('Auth error');
        } else if (projects.length) {
          if (projects[0].deleted) {
            throw new Error('Project deleted, but u can restore it or create new');
          } else {
            throw new Error('Project already exist!');
          }
        } else {
          return createProject(req.body.projectName, req.body.projectStatusId);
        }
      })
      .then((createdProject) => {
        if (!req.body.isAdmin) {
          addAdminToProject(createdProject.projectId);
        }
        return makeUsersProjectsNote(decoded.subject, createdProject.projectId);
      })
      .then(() => {
        sendAnswer(false, '', res);
      })
      .catch((err) => {
        sendAnswer(true, err.message, res);
      });
  });
};

exports.project_restore_post = (req, res) => {
  Project.update(
    { deleted: false },
    {
      where: {
        projectName: req.body.projectName,
      },
    },
  )
    .then(() => {
      sendAnswer(false, '', res);
    })
    .catch((err) => {
      sendAnswer(true, `RESTORE PROJECT: ${err.message}`, res);
    });
};

function deleteExistProject(projectName) {
  return Project.destroy({
    where: {
      projectName,
    },
  });
}

exports.project_delete_create_new_post = (req, res) => {
  jwt.verify(req.body.userIdToken, res.jwtKey, (errVerify, decoded) => {
    if (errVerify) {
      sendAnswer(true, 'Auth error', res);
      return;
    }
    deleteExistProject(req.body.projectName)
      .then(() => createProject(req.body.projectName, req.body.projectStatusId))
      .then((createdProject) => {
        if (!req.body.isAdmin) {
          addAdminToProject(createdProject.projectId);
        }
        return createdProject.projectId;
      })
      .then((projectId) => makeUsersProjectsNote(decoded.subject, projectId))
      .then(() => {
        sendAnswer(false, '', res);
      })
      .catch((err) => {
        sendAnswer(true, err.message, res);
      });
  });
};

exports.project_status_get = (req, res) => {
  ProjectStatus.findAll({
    attributes: ['projectStatusName', 'projectStatusId'],
  })
    .then((projectStatuses) => {
      res.json(projectStatuses);
    })
    .catch((err) => {
      throw err.message;
    });
};
