const jwt = require('jsonwebtoken');
const Project = require('../models').project;
const User = require('../models').user;
const Role = require('../models').role;
const Task = require('../models').task;
const ProjectStatus = require('../models').projectStatus;

function findProjectManager(projectManagerId, users) {
  if (projectManagerId) {
    const projectManager = users.find((user) => user.userId === projectManagerId);
    return `${projectManager.firstName} ${projectManager.lastName || ''}`;
  }
  return null;
}

function fillData(projectsInfo) {
  const parsedInfo = [];
  projectsInfo.forEach((projectInfo) => {
    let endDate = 'Not set';
    let startDate = 'Not set';
    if (projectInfo.tasks.length) {
      startDate = projectInfo.tasks[0].dateStart || 'Not set';
      endDate = projectInfo.tasks[projectInfo.tasks.length - 1].endStart || 'Not set';
    }
    const managerId = projectInfo.currentProjectManagerId;
    const projectManagerName = findProjectManager(managerId, projectInfo.users);
    parsedInfo.push({
      projectId: projectInfo.projectId,
      projectName: projectInfo.projectName,
      startDate,
      endDate,
      projectStatusName: projectInfo.projectStatus.projectStatusName,
      projectManagerName,
      membersCount: projectInfo.users.length,
      developersCount: projectInfo.users
        .filter((user) => user.role.roleName === 'Developer').length,
      tasksCount: projectInfo.tasks.length,
    });
  });
  return parsedInfo;
}

exports.dashboard_statistic_get = (req, res) => {
  Project.findAll({
    where: {
      deleted: false,
    },
    order: [
      [{ model: Task, as: 'tasks' }, 'taskOrder', 'ASC'],
    ],
    include: [{
      model: Task,
      as: 'tasks',
      attributes: ['taskId', 'endStart', 'dateStart'],
    }, {
      model: User,
      attributes: ['userId', 'firstName', 'lastName'],
      include: {
        model: Role,
        attributes: ['roleName'],
      },
    }, {
      model: ProjectStatus,
      attributes: ['projectStatusName'],
    }],
  })
    .then((projectsInfo) => {
      const parsedInfo = fillData(projectsInfo);
      res.json(parsedInfo);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.dashboard_statistic_where_exist_get = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (errVerify, decoded) => {
    User.findByPk(decoded.subject, {
      order: [
        [{ model: Project }, { model: Task, as: 'tasks' }, 'taskOrder', 'ASC'],
      ],
      include: {
        model: Project,
        where: {
          deleted: 'false',
        },
        include: [{
          model: Task,
          as: 'tasks',
          attributes: ['taskId', 'endStart', 'dateStart'],
        }, {
          model: User,
          attributes: ['userId', 'firstName', 'lastName'],
          include: {
            model: Role,
            attributes: ['roleName'],
          },
        }, {
          model: ProjectStatus,
          attributes: ['projectStatusName'],
        }],
      },
    })
      .then((projectsInfo) => {
        const parsedInfo = fillData(projectsInfo.projects);
        res.json(parsedInfo);
      })
      .catch((err) => {
        throw err.message;
      });
  });
};

exports.pie_data_get = (req, res) => {
  User.findAll({
    attributes: [],
    include: {
      model: Role,
      attributes: ['roleName'],
    },
  })
    .then((usersRoles) => {
      const answer = [{
        name: 'QA',
        value: usersRoles.filter((user) => user.role.roleName === 'QA').length,
      }, {
        name: 'PM',
        value: usersRoles.filter((user) => user.role.roleName === 'Project Manager').length,
      }, {
        name: 'Developers',
        value: usersRoles.filter((user) => user.role.roleName === 'Developer').length,
      }];
      res.json(answer);
    })
    .catch((err) => {
      throw err.message;
    });
};

function findCompleted(tasks) {
  const tasksPart = [];
  tasks.forEach((task) => {
    if (task.completed === 100) {
      tasksPart.push({
        pessimisticTime: task.pessimisticTime,
        optimisticTime: task.optimisticTime,
      });
    }
  });
  return tasksPart;
}

exports.tasks_hours_widget_get = (req, res) => {
  Project.findAll({
    where: {
      deleted: false,
    },
    include: {
      model: Task,
      as: 'tasks',
      attributes: ['optimisticTime', 'pessimisticTime', 'completed'],
    },
  })
    .then((projects) => {
      let tasksCount = 0;
      let completedTasks = [];
      projects.forEach((project) => {
        tasksCount += project.tasks.length;
        completedTasks = completedTasks.concat(findCompleted(project.tasks));
      });
      res.json({
        tasksCount,
        completedTasks,
      });
    })
    .catch((err) => {
      throw err;
    });
};

exports.projects_status_widget_get = (req, res) => {
  Project.findAll({
    where: {
      deleted: false,
    },
    attributes: [],
    include: {
      model: ProjectStatus,
      attributes: ['projectStatusName'],
    },
  })
    .then((projects) => {
      const answer = [{
        value: projects
          .filter((project) => project.projectStatus.projectStatusName === 'Pending').length,
        name: 'Pending',
      }, {
        value: projects
          .filter((project) => project.projectStatus.projectStatusName === 'Completed').length,
        name: 'Completed',
      }, {
        value: projects
          .filter((project) => project.projectStatus.projectStatusName === 'Not Started').length,
        name: 'Not Started',
      }, {
        value: projects
          .filter((project) => project.projectStatus.projectStatusName === 'In progress').length,
        name: 'In progress',
      }];
      res.json(answer);
    })
    .catch((err) => {
      throw err;
    });
};
