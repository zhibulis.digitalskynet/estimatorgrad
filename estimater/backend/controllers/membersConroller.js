const jwt = require('jsonwebtoken');
const generator = require('generate-password');
const bcrypt = require('bcrypt');
const fs = require('fs');
const path = require('path');
const { sendInviteMail } = require('../services/mail-service');
const User = require('../models').user;
const Role = require('../models').role;
const Image = require('../models').image;
const PasswordReset = require('../models').passwordReset;

// Needed for hash rounds, salutRounds value higher - more hash rounds
const saltRounds = 10;

exports.all_members_get = (req, res) => {
  User.findAll({
    attributes: [
      'userId',
      'firstName',
      'lastName',
      'email',
      'registerDate',
      'isConfirmed',
    ],
    include: [
      {
        model: Role,
        attributes: ['roleName'],
      },
      {
        model: Image,
        attributes: ['image'],
      },
    ],
  })
    .then((notParsedUsers) => {
      const users = [];
      notParsedUsers.forEach((user) => {
        const name = !user.lastName
          ? `${user.firstName}`
          : `${user.firstName} ${user.lastName}`;
        users.push({
          userId: user.userId,
          role: user.role ? user.role.roleName : '',
          image: user.image ? user.image.image : '',
          email: user.email,
          name,
          registerDate: user.registerDate,
          confirmation: user.isConfirmed ? 'Confirmed' : 'Unconfirmed',
        });
      });
      res.json(users);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.delete_users_post = (req, res) => {
  User.destroy({
    where: {
      userId: req.body,
    },
  })
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      res.json({
        error: false,
        errorMessage: err.message,
      });
    });
};

function sendError(errorMessage, res) {
  res.json({
    error: true,
    errorMessage,
  });
}

function getUserRole(userId) {
  return User.findOne({
    where: {
      userId,
    },
    include: {
      model: Role,
      attributes: ['roleName'],
    },
  });
}

async function hashPassword(password) {
  const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
      if (err) reject(err);
      resolve(hash);
    });
  });
  return hashedPassword;
}

function insertImage(userId) {
  const image = fs.readFileSync(path.resolve(__dirname, '../images/default-avatar.png'), 'base64');
  return Image.create({
    userId,
    image,
  }, {
    fields: ['userId', 'image'],
  });
}

function createResetPasswordNote(userId, setPasswordToken) {
  return PasswordReset.create({
    userId,
    passwordResetToken: setPasswordToken,
  }, {
    fields: [
      'userId',
      'passwordResetToken',
    ],
  });
}

exports.add_members_put = (req, res) => {
  jwt.verify(req.body.creatorTokenId, res.jwtKey, (errDecode, decoded) => {
    let isConfirmed;
    let randomPassword;
    getUserRole(decoded.subject)
      .then((userRole) => !!(userRole.role.roleName === 'Admin' || 'Project Manager'))
      .then((isConfirmedParam) => {
        isConfirmed = isConfirmedParam;
        const passwordLength = 15;
        randomPassword = generator.generate({
          length: passwordLength,
          numbers: true,
        });
        return hashPassword(randomPassword);
      })
      .then((hash) => {
        const { roleId } = req.body.userBody;
        const isNewInvited = true;
        return User.create({
          username: req.body.userBody.username,
          password: hash,
          email: req.body.userBody.email,
          registerDate: req.body.userBody.registerDate,
          roleId,
          isConfirmed,
          firstName: req.body.userBody.username,
          isNewInvited,
        },
        {
          fields: [
            'username',
            'password',
            'email',
            'registerDate',
            'isConfirmed',
            'firstName',
            'roleId',
            'isNewInvited',
          ],
        });
      })
      .then((user) => insertImage(user.userId))
      .then((user) => {
        const payload = { subject: randomPassword };
        const setPasswordToken = jwt.sign(payload, res.jwtKey, {
          expiresIn: 604800,
        });
        return createResetPasswordNote(user.userId, setPasswordToken);
      })
      .then((setPasswordNote) => {
        res.json({
          error: false,
        });
        sendInviteMail(
          req.body.userBody.email,
          req.body.userBody.username,
          setPasswordNote.passwordResetToken,
        );
      })
      .catch((err) => {
        sendError(err.message, res);
      });
  });
};

exports.roles_get = (req, res) => {
  Role.findAll({
    attributes: ['roleId', 'roleName'],
  })
    .then((roles) => {
      res.json(roles);
    });
};
