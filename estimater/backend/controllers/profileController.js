const jwt = require('jsonwebtoken');
const User = require('../models').user;
const Role = require('../models').role;
const Task = require('../models').task;
const UserProject = require('../models').userProject;
const Project = require('../models').project;
const Image = require('../models').image;

exports.member_info_get = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (errDecode, decoded) => {
    if (errDecode) throw errDecode;
    User.findByPk(decoded.subject, {
      attributes: ['firstName', 'lastName', 'email'],
      include: [{
        model: Role,
        attributes: ['roleName'],
      }, {
        model: Image,
        attributes: ['image'],
      }],
    })
      .then((user) => {
        const resUser = user;
        resUser.dataValues.image = user.image.image;
        res.json(resUser);
      })
      .catch((err) => {
        throw err;
      });
  });
};

exports.tasks_get = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (errDecode, decoded) => {
    if (errDecode) throw errDecode;
    Task.findAll({
      where: {
        assignedId: decoded.subject,
      },
      attributes: [
        'taskId',
        'taskName',
        'dateStart',
        'endStart',
        'optimisticTime',
        'pessimisticTime',
        'completed',
      ],
      include: {
        model: Project,
        as: 'project',
      },
    })
      .then((tasks) => {
        res.json(tasks);
      })
      .catch((err) => {
        throw err;
      });
  });
};

exports.set_profile_avatar_put = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (errDecode, decoded) => {
    if (errDecode) throw errDecode;
    Image.update({
      image: req.body.file,
    }, {
      where: {
        userId: decoded.subject,
      },
    })
      .then(() => {
        res.json({
          error: false,
        });
      })
      .catch((err) => {
        res.json({
          error: true,
          errorMessage: err.message,
        });
      });
  });
};

exports.projects_count_get = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (errDecode, decoded) => {
    if (errDecode) throw errDecode;
    UserProject.count({
      attributes: [],
      where: {
        userId: decoded.subject,
        '$project.deleted$': false,
      },
      include: {
        model: Project,
        attributes: [],
      },
    })
      .then((count) => {
        res.json(count);
      })
      .catch((err) => {
        throw err.message;
      });
  });
};
