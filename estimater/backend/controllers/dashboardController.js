const jwt = require('jsonwebtoken');
const { Op } = require('sequelize');
const User = require('../models').user;
const Project = require('../models').project;
const Image = require('../models').image;
const Task = require('../models').task;
const UserProject = require('../models').userProject;
const TaskStatus = require('../models').taskStatus;
const ProjectStatus = require('../models').projectStatus;

/* Calculate the time difference of two dates using date2 – date1
Calculate the no. of days between two dates,
divide the time difference of both the dates by no.
of milliseconds in a day (1000*60*60*24)
-1 because we dont need to include current day */
function calculateDueDays(date1, date2) {
  const secondDate = date2 || new Date();
  const decDay = date2 ? 0 : 1;
  return Math.ceil(Math.abs(secondDate - date1) / (1000 * 60 * 60 * 24)) - decDay;
}

function checkTasks(tasks) {
  let total = 0;
  let closed = 0;
  tasks.forEach((task) => {
    total += 1;
    if (task.taskStatus.taskStatusName === 'closed') closed += 1;
  });
  return {
    total,
    closed,
  };
}

function buildUsersProjects(users) {
  const usersArr = [];
  users.forEach((user) => {
    usersArr.push({
      userId: user.userId,
      projectId: user.userProject.projectId,
    });
  });
  return usersArr;
}

function loadDashboardData(notParsedDashboardData) {
  const projects = [];
  let tasksProgress;

  notParsedDashboardData.forEach((project) => {
    tasksProgress = checkTasks(project.tasks);
    let total;
    let date;
    let startsIn;
    let live;
    if (project.tasks.length && project.startDate) {
      date = new Date(project.startDate);
      total = project.tasks[project.tasks.length - 1].endStart
        ? calculateDueDays(date, new Date(project.tasks[project.tasks.length - 1].endStart))
        : undefined;
      live = date <= new Date() ? calculateDueDays(date, null) : null;
      startsIn = date > new Date() ? calculateDueDays(new Date(), date) : null;
    }

    const usersFromProject = buildUsersProjects(project.users);
    projects.push({
      projectName: project.projectName,
      projectId: project.projectId,
      tasksProgress,
      currentProjectManagerId: project.currentProjectManagerId,
      projectStatusName: project.projectStatus.projectStatusName,
      startsIn,
      live,
      total,
      members: usersFromProject,
    });
  });
  return projects;
}

exports.dasboard_getData_post = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (errVerify, decoded) => {
    User.findByPk(decoded.subject, {
      attributes: [],
      order: [
        [{ model: Project }, { model: Task, as: 'tasks' }, 'taskOrder', 'ASC'],
      ],
      include: {
        attributes: ['projectId', 'projectName', 'startDate', 'endDate', 'currentProjectManagerId'],
        model: Project,
        where: {
          deleted: 'false',
        },
        include: [{
          model: ProjectStatus,
          attributes: ['projectStatusName'],
        }, {
          model: User,
          attributes: ['userId'],
        }, {
          model: Task,
          attributes: ['taskStatusId', 'dateStart', 'endStart', 'taskOrder'],
          as: 'tasks',
          include: {
            model: TaskStatus,
            attributes: ['taskStatusName'],
          },
        }],
      },
    })
      .then((notParsedDashboardData) => {
        const dashboardData = notParsedDashboardData
          ? loadDashboardData(notParsedDashboardData.projects)
          : [];
        res.json(dashboardData);
      })
      .catch((err) => {
        throw err.message;
      });
  });
};

function buildAllUsers(users) {
  const usersArr = [];
  users.forEach((user) => {
    usersArr.push({
      userId: user.userId,
      image: user.image.image,
      fullName: !user.lastName
        ? user.firstName
        : `${user.firstName} ${user.lastName}`,
    });
  });
  return usersArr;
}

exports.load_users_get = (req, res) => User.findAll({
  attributes: ['userId', 'firstName', 'lastName'],
  include: {
    model: Image,
    attributes: ['image'],
  },
})
  .then((users) => {
    const resUsers = buildAllUsers(users);
    res.json(resUsers);
  })
  .catch((err) => {
    throw err;
  });

exports.dasboard_deleteProject_delete = (req, res) => {
  Project.update({
    deleted: true,
  }, {
    where: {
      projectId: req.params.projectId,
    },
  })
    .then(() => {
      res.json(false);
    })
    .catch(() => {
      res.json(true);
    });
};

exports.dashboard_all_projects_get = (req, res) => {
  Project.findAll({
    attributes: ['projectId', 'projectName', 'startDate', 'endDate', 'currentProjectManagerId'],
    where: {
      deleted: 'false',
    },
    order: [
      [{ model: Task, as: 'tasks' }, 'taskOrder', 'ASC'],
    ],
    include: [{
      model: ProjectStatus,
      attributes: ['projectStatusName'],
    }, {
      model: User,
      attributes: ['userId'],
    }, {
      model: Task,
      attributes: ['taskStatusId', 'dateStart', 'endStart', 'taskOrder'],
      as: 'tasks',
      include: {
        model: TaskStatus,
        attributes: ['taskStatusName'],
      },
    }],
  })
    .then((notParsedDashboardData) => {
      const dashboardData = notParsedDashboardData
        ? loadDashboardData(notParsedDashboardData)
        : [];
      res.json(dashboardData);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.leave_project_delete = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (errVerify, decoded) => {
    if (!decoded.subject) {
      res.json({
        error: true,
        errorMessage: errVerify,
      });
      return;
    }
    UserProject.destroy({
      where: {
        [Op.and]: [
          { projectId: req.params.projectId },
          { userId: decoded.subject },
        ],
      },
    })
      .then(() => {
        if (req.params.currentProjectManagerId === decoded.subject) {
          return Project.update({
            projectManagerId: null,
          }, {
            where: {
              projectId: req.params.projectId,
            },
          });
        }
        return true;
      })
      .then(() => Task.update({
        assignedId: null,
      }, {
        where: {
          [Op.and]: [
            { projectId: req.params.projectId },
            { assignedId: decoded.subject },
          ],
        },
      }))
      .then(() => {
        res.json({
          error: false,
        });
      })
      .catch((err) => {
        res.json({
          error: true,
          errorMessage: err.message,
        });
      });
  });
};
