const { Op } = require('sequelize');
const jwt = require('jsonwebtoken');
const { sendInviteToProjectMail } = require('../services/mail-service');
const User = require('../models').user;
const UserProject = require('../models').userProject;
const Notification = require('../models').notification;

function buildMessage(username, projectName, invitingUsername, userid, projectid) {
  return `Dear ${username}\n\n`
    + `You have been invited to the ${projectName} by ${invitingUsername} \n\n`
    + 'If this was a mistake you can leave the project\n'
    + `https://estimator.k8s.digitalskynet.com/user-in-project/${userid}/${projectid}`;
}

function sendError(errorMessage, res) {
  res.json({
    error: true,
    errorMessage,
  });
}

exports.invite_user = (req, res) => {
  jwt.verify(req.body.inviterTokenId, res.jwtKey, (errVerify, decoded) => {
    let userData;
    User.findOne({
      where: {
        email: req.body.email,
      },
      attributes: ['userId', 'username'],
    })
      .then((user) => {
        if (errVerify) {
          throw new Error('Unauthorized');
        }
        if (!user) {
          throw new Error('This user doesn`t exist!');
        } else {
          userData = user;
          return UserProject.findOne({
            where: {
              userId: user.userId,
              projectId: req.body.projectId,
            },
          });
        }
      })
      .then((userProject) => {
        if (userProject) {
          throw new Error('User already exist in project');
        }
      })
      .then(() => UserProject.create({
        userId: userData.userId,
        projectId: req.body.projectId,
      }, {
        fields: ['userId', 'projectId'],
      }))
      .then(() => User.findByPk(decoded.subject, {
        attributes: ['firstName', 'lastName'],
      }))
      .then((inviter) => {
        const invitorName = `${inviter.firstName} ${inviter.lastName ? inviter.lastName : ''}`;
        const messageParams = buildMessage(userData.username, req.body.projectName,
          invitorName, userData.userId, req.body.projectId);
        return Notification.create({
          userId: userData.userId,
          projectId: req.body.projectId,
          description: messageParams,
          createdDate: req.body.createdDate,
        }, {
          fields: [
            'userId',
            'projectId',
            'description',
            'createdDate',
          ],
        });
      })
      .then((notification) => {
        res.json({
          error: false,
        });
        sendInviteToProjectMail(
          req.body.email,
          notification.description,
        );
      })
      .catch((err) => {
        sendError(err.message, res);
      });
  });
};

exports.delete_user_from_project = (req, res) => {
  UserProject.destroy({
    where: {
      [Op.and]: [
        { userId: req.body.userId },
        { projectId: req.body.projectId },
      ],
    },
  })
    .then(() => Notification.destroy({
      where: {
        [Op.and]: [
          { userId: req.body.userId },
          { projectId: req.body.projectId },
        ],
      },
    }))
    .then(() => {
      res.json({
        error: false,
      });
    })
    .catch((err) => {
      sendError(err.message, res);
    });
};
