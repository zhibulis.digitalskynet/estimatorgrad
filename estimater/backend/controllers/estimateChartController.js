const jwt = require('jsonwebtoken');
const User = require('../models/').user;
const Task = require('../models').task;
const Project = require('../models').project;

exports.projects_list_get = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (err, decoded) => {
    Project.findAll({
      where: {
        deleted: false,
      },
      attributes: ['projectId', 'projectName'],
      include: {
        model: User,
        where: {
          userId: decoded.subject,
        },
        attributes: [],
      },
    })
      .then((projects) => {
        res.json(projects);
      });
  });
};

exports.table_get = (req, res) => {
  Project.findByPk(req.params.projectId, {
    order: [
      [{ model: Task, as: 'tasks' }, 'taskOrder', 'ASC'],
    ],
    include: {
      model: Task,
      as: 'tasks',
      include: [{
        model: User,
        attributes: ['firstName', 'lastName'],
      }],
    },
  })
    .then((project) => {
      res.json(project.tasks);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.projects_all_list_get = (req, res) => {
  Project.findAll({
    where: {
      deleted: false,
    },
    attributes: ['projectId', 'projectName'],
  })
    .then((projects) => {
      res.json(projects);
    })
    .catch((err) => {
      throw err;
    });
};
