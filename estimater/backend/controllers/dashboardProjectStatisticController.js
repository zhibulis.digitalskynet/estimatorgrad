const { Op } = require('sequelize');
const Project = require('../models').project;
const User = require('../models').user;
const Role = require('../models').role;
const Task = require('../models').task;

function findCompleted(tasks) {
  const timesOfCompletedTasks = [];
  tasks.forEach((task) => {
    if (task.completed === 100) {
      timesOfCompletedTasks.push({
        pessimisticTime: task.pessimisticTime,
        optimisticTime: task.optimisticTime,
      });
    }
  });
  return timesOfCompletedTasks;
}

exports.pie_data_get = (req, res) => {
  Project.findByPk(req.params.projectId, {
    attributes: [],
    include: {
      model: User,
      attributes: ['roleId'],
      include: {
        model: Role,
        attributes: ['roleName'],
      },
    },
  })
    .then((project) => {
      const answer = [{
        name: 'QA',
        value: project.users.filter((user) => user.role.roleName === 'QA').length,
      }, {
        name: 'PM',
        value: project.users.filter((user) => user.role.roleName === 'Project Manager').length,
      }, {
        name: 'Developers',
        value: project.users.filter((user) => user.role.roleName === 'Developer').length,
      }];
      res.json(answer);
    })
    .catch((err) => {
      throw err.message;
    });
};

exports.tasks_hours_widget_get = (req, res) => {
  Project.findOne({
    where: {
      [Op.and]: [
        { deleted: false },
        { projectId: req.params.projectId },
      ],
    },
    include: {
      model: Task,
      as: 'tasks',
      attributes: ['optimisticTime', 'pessimisticTime', 'completed'],
    },
  })
    .then((project) => {
      const tasksCount = project.tasks.length || 0;
      const completedTasks = findCompleted(project.tasks);
      res.json({
        tasksCount,
        completedTasks,
      });
    })
    .catch((err) => {
      throw err;
    });
};

exports.project_name_get = (req, res) => {
  Project.findByPk(req.params.projectId, {
    attributes: ['projectName'],
  })
    .then((project) => {
      res.json(project);
    })
    .catch((err) => {
      throw err;
    });
};
