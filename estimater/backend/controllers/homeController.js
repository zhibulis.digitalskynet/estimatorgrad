const jwt = require('jsonwebtoken');
const Project = require('../models').project;
const User = require('../models').user;
const Role = require('../models').role;

function checkDefaultUser(userId, res) {
  User.count({
    where: {
      userId,
    },
    include: {
      model: Project,
      where: {
        deleted: false,
      },
    },
  })
    .then((userProjects) => {
      res.json(!!userProjects);
    });
}

function checkExtendedUser(res) {
  Project.count({
    where: {
      deleted: false,
    },
  })
    .then((projectsCount) => {
      res.json(!!projectsCount);
    });
}

function getUserRole(userId) {
  return User.findOne({
    attributes: ['userId'],
    where: {
      userId,
    },
    include: {
      model: Role,
      attributes: ['roleName'],
    },
  });
}

exports.check_projects_post = (req, res) => {
  jwt.verify(req.params.idtoken, res.jwtKey, (err, decoded) => {
    if (err) throw err;
    getUserRole(decoded.subject)
      .then((userRole) => {
        if (userRole.role.roleName === 'Admin' || userRole.role.roleName === 'Project Manager') {
          checkExtendedUser(res);
        } else {
          checkDefaultUser(decoded.subject, res);
        }
      });
  });
};
