const jwt = require('jsonwebtoken');
const User = require('../models').user;
const Image = require('../models').image;
const Role = require('../models').role;
const Notification = require('../models').notification;

function getUserRole(userId) {
  return User.findOne({
    where: {
      userId,
    },
    attributes: [],
    include: {
      model: Role,
      attributes: ['roleName'],
    },
  });
}

exports.user_data_get = (req, res) => {
  jwt.verify(req.params.token, res.jwtKey, (errVerify, decoded) => {
    User.findOne({
      where: {
        userId: decoded.subject,
      },
      attributes: ['firstName', 'lastName'],
      include: {
        model: Image,
        attributes: ['image'],
      },
    })
      .then((user) => {
        const fullName = !user.lastName
          ? user.firstName
          : `${user.firstName} ${user.lastName}`;
        res.json({
          fullName,
          image: user.image.image,
        });
      })
      .catch((err) => {
        throw err.message;
      });
  });
};

exports.notification_count_get = (req, res) => {
  jwt.verify(req.params.idToken, res.jwtKey, (errorGet, decoded) => {
    Notification.findAll({
      attributes: ['description'],
      where: {
        userId: decoded.subject,
      },
    })
      .then((notifications) => {
        res.json(notifications);
      })
      .catch((err) => {
        throw err.message;
      });
  });
};

exports.role_name_get = (req, res) => {
  jwt.verify(req.params.token, res.jwtKey, (errVerify, decoded) => {
    getUserRole(decoded.subject)
      .then((userRole) => {
        res.json(userRole.role.roleName);
      })
      .catch((err) => {
        throw (err.message);
      });
  });
};
