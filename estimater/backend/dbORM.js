const config = require('config');
const Sequelize = require('sequelize');

const dbConfig = config.get('Estimator.dbConfig');

const sequelize = new Sequelize(`postgres://${dbConfig.user}:${dbConfig.password}@${dbConfig.host}:${dbConfig.port}/${dbConfig.database}`, {
  dialect: 'postgres',
});

console.log(`NODE_ENV: ${config.util.getEnv('NODE_ENV')}`);
console.log(`SUPPRESS_NO_CONFIG_WARNING: ${config.util.getEnv('SUPPRESS_NO_CONFIG_WARNING')}`);
console.log(config.get('Estimator.dbConfig'));

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize;
