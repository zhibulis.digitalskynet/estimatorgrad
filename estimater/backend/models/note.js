const Sequelize = require('sequelize');
const Project = require('./project');

module.exports = (sequelize, dataTypes) => {
  const Note = sequelize.define('note', {
    noteId: {
      type: dataTypes.INTEGER,
      field: 'noteid',
      primaryKey: true,
    },
    projectId: {
      type: dataTypes.INTEGER,
      references: {
        model: Project,
        key: 'projectId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'projectid',
    },
    noteName: {
      type: dataTypes.STRING,
      field: 'notename',
    },
    note: {
      type: dataTypes.STRING,
    },
  }, {
    tableName: 'notes',
    timestamps: false,
  });

  Note.associate = (models) => {
    Note.belongsTo(models.project, { foreignKey: 'projectId' });
  };

  return Note;
};
