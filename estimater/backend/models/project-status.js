module.exports = (sequelize, dataTypes) => {
  const ProjectStatus = sequelize.define('projectStatus', {
    projectStatusId: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      field: 'projectstatusid',
    },
    projectStatusName: {
      type: dataTypes.STRING,
      field: 'projectstatusname',
    },
  }, {
    tableName: 'projectstatus',
    timestamps: false,
  });

  ProjectStatus.associate = (models) => {
    ProjectStatus.hasMany(models.project, { foreignKey: 'projectStatusId' });
  };

  return ProjectStatus;
};
