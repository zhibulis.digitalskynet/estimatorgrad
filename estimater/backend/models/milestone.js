const Sequelize = require('sequelize');
const Project = require('./project');
const MilestoneStatus = require('./milestone-statuses');

module.exports = (sequelize, dataTypes) => {
  const Milestone = sequelize.define('milestone', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
    },
    name: {
      type: dataTypes.STRING,
    },
    projectId: {
      type: dataTypes.INTEGER,
      references: {
        model: Project,
        key: 'projectId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'projectid',
    },
    milestoneStatusId: {
      type: dataTypes.INTEGER,
      references: {
        model: MilestoneStatus,
        key: 'milestoneStatusId',
      },
      field: 'milestonestatusid',
    },
  }, {
    tableName: 'milestones',
    timestamps: false,
  });

  Milestone.associate = (models) => {
    Milestone.belongsTo(models.milestoneStatus, { foreignKey: 'milestoneStatusId' });
    Milestone.hasMany(models.task, { foreignKey: 'milestoneId' });
  };
  return Milestone;
};
