const User = require('./user');

module.exports = (sequelize, dataTypes) => {
  const PasswordReset = sequelize.define('passwordReset', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
    },
    userId: {
      type: dataTypes.INTEGER,
      references: {
        model: User,
        key: 'userid',
      },
      field: 'userid',
    },
    passwordResetToken: {
      type: dataTypes.STRING,
      field: 'passwordresettoken',
    },
  }, {
    tableName: 'resetpassword',
    timestamps: false,
  });

  PasswordReset.associate = (models) => {
    PasswordReset.belongsTo(models.user, { foreignKey: 'userId' });
  };
  return PasswordReset;
};
