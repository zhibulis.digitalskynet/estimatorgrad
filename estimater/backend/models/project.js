const Sequelize = require('sequelize');
const ProjectStatus = require('./project-status');
const User = require('./index');

module.exports = (sequelize, dataTypes) => {
  const Project = sequelize.define('project', {
    projectId: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      field: 'projectid',
    },
    projectName: {
      type: dataTypes.STRING,
      field: 'projectname',
    },
    startDate: {
      type: dataTypes.STRING,
      field: 'startdate',
    },
    endDate: {
      type: dataTypes.STRING,
      field: 'enddate',
    },
    currentProjectManagerId: {
      type: dataTypes.INTEGER,
      references: {
        model: User,
        key: 'userId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'currentprojectmanagerid',
    },
    projectStatusId: {
      type: dataTypes.INTEGER,
      references: {
        model: ProjectStatus,
        key: 'projectStatusId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'projectstatusid',
    },
    url: {
      type: dataTypes.STRING,
    },
    description: {
      type: Sequelize.STRING,
    },
    deleted: {
      type: Sequelize.BOOLEAN,
    },
  }, {
    tableName: 'projects',
    timestamps: false,
  });

  Project.associate = (models) => {
    Project.hasMany(models.task, { as: 'tasks', foreignKey: 'projectId' });
    Project.hasMany(models.note, { as: 'notes', foreignKey: 'projectId' });
    Project.belongsToMany(models.user, { through: { model: models.userProject }, foreignKey: 'projectId', otherKey: 'userId' });
    Project.belongsTo(models.projectStatus, { foreignKey: 'projectStatusId' });
  };

  return Project;
};
