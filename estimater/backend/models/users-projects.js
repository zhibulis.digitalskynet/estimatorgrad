const Sequelize = require('sequelize');
const User = require('./user');
const Project = require('./project');

module.exports = (sequelize, dataTypes) => {
  const UserProject = sequelize.define('userProject', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
    },
    userId: {
      type: dataTypes.INTEGER,
      references: {
        model: User,
        key: 'userId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'userid',
    },
    projectId: {
      type: dataTypes.INTEGER,
      references: {
        model: Project,
        key: 'projectId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'projectid',
    },
  }, {
    tableName: 'usersprojects',
    timestamps: false,
  });

  UserProject.associate = (models) => {
    UserProject.belongsTo(models.user, { foreignKey: 'userId' });
    UserProject.belongsTo(models.project, { foreignKey: 'projectId' });
  };

  return UserProject;
};
