module.exports = (sequelize, dataTypes) => {
  const MilestoneStatus = sequelize.define('milestoneStatus', {
    milestoneStatusId: {
      type: dataTypes.INTEGER,
      field: 'milestonestatusid',
      primaryKey: true,
    },
    milestoneStatusName: {
      type: dataTypes.STRING,
      field: 'milestonestatusname',
    },
  }, {
    tableName: 'milestonestatuses',
    timestamps: false,
  });

  MilestoneStatus.associate = (models) => {
    MilestoneStatus.hasMany(models.milestone, { foreignKey: 'milestoneStatusId' });
  };

  return MilestoneStatus;
};
