const Sequelize = require('sequelize');
const Role = require('./role');

module.exports = (sequelize, dataTypes) => {
  const User = sequelize.define('user', {
    userId: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      field: 'userid',
    },
    username: {
      type: dataTypes.STRING,
    },
    email: {
      type: dataTypes.STRING,
    },
    password: {
      type: dataTypes.STRING,
    },
    registerDate: {
      type: dataTypes.STRING,
      field: 'registerdate',
    },
    firstName: {
      type: dataTypes.STRING,
      field: 'firstname',
    },
    lastName: {
      type: dataTypes.STRING,
      field: 'lastname',
    },
    isConfirmed: {
      type: dataTypes.BOOLEAN,
      field: 'isconfirmed',
    },
    roleId: {
      type: dataTypes.INTEGER,
      references: {
        model: Role,
        key: 'roleId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'roleid',
    },
    workingHours: {
      type: dataTypes.INTEGER,
      field: 'workinghours',
    },
    isNewInvited: {
      type: dataTypes.BOOLEAN,
      field: 'isnewinvited',
    },
  }, {
    tableName: 'users',
    timestamps: false,
  });

  User.associate = (models) => {
    User.belongsToMany(models.project, { through: { model: models.userProject }, foreignKey: 'userId', otherKey: 'projectId' });
    User.belongsTo(models.role, { foreignKey: 'roleId' });
    User.hasOne(models.image, { foreignKey: 'userId' });
  };

  return User;
};
