const Sequelize = require('sequelize');
const Milestone = require('./milestone');
const Project = require('./project');
const User = require('./user');
const TaskStatus = require('./task-status');

module.exports = (sequelize, dataTypes) => {
  const Task = sequelize.define('task', {
    taskId: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      field: 'taskid',
    },
    taskName: {
      type: dataTypes.STRING,
      field: 'taskname',
    },
    dateStart: {
      type: dataTypes.STRING,
      field: 'datestart',
    },
    endStart: {
      type: dataTypes.STRING,
      field: 'endstart',
    },
    taskStatusId: {
      type: dataTypes.INTEGER,
      references: {
        model: TaskStatus,
        key: 'taskStatusId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'taskstatusid',
    },
    milestoneId: {
      type: dataTypes.INTEGER,
      references: {
        model: Milestone,
        key: 'id',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'milestoneid',
    },
    projectId: {
      type: dataTypes.INTEGER,
      references: {
        model: Project,
        key: 'projectId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'projectid',
    },
    assignedId: {
      type: dataTypes.INTEGER,
      references: {
        model: User,
        key: 'userId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'assignedid',
    },
    optimisticTime: {
      type: dataTypes.INTEGER,
      field: 'optimistictime',
    },
    pessimisticTime: {
      type: dataTypes.INTEGER,
      field: 'pessimistictime',
    },
    completed: {
      type: dataTypes.INTEGER,
    },
    taskOrder: {
      type: dataTypes.INTEGER,
      field: 'taskorder',
    },
  }, {
    tableName: 'tasks',
    timestamps: false,
  });


  Task.associate = (models) => {
    Task.belongsTo(models.project, { as: 'project', foreignKey: 'projectId', targetKey: 'projectId' });
    Task.belongsToMany(models.task, {
      as: 'blockers', through: models.blocker, foreignKey: 'taskId', otherKey: 'taskBlockerId',
    });
    Task.belongsTo(models.user, { foreignKey: 'assignedId' });
    Task.hasMany(models.blocker, { as: 'taskBlockers', foreignKey: 'taskId' });
    Task.belongsTo(models.milestone, { foreignKey: 'milestoneId' });
    Task.belongsTo(models.taskStatus, { foreignKey: 'taskStatusId' });
  };

  return Task;
};
