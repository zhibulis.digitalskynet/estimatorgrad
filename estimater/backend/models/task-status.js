module.exports = (sequelize, dataTypes) => {
  const TaskStatus = sequelize.define('taskStatus', {
    taskStatusId: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      field: 'taskstatusid',
    },
    taskStatusName: {
      type: dataTypes.STRING,
      field: 'taskstatusname',
    },
  }, {
    tableName: 'taskstatus',
    timestamps: false,
  });

  TaskStatus.associate = (models) => {
    TaskStatus.hasOne(models.task, { foreignKey: 'taskStatusId' });
  };

  return TaskStatus;
};
