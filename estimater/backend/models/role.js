module.exports = (sequelize, dataTypes) => {
  const Role = sequelize.define('role', {
    roleId: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      field: 'roleid',
    },
    roleName: {
      type: dataTypes.STRING,
      field: 'rolename',
    },
    createDate: {
      type: dataTypes.STRING,
      field: 'createdate',
    },
  }, {
    tableName: 'roles',
    timestamps: false,
  });

  Role.associate = (models) => {
    Role.hasOne(models.user, { foreignKey: 'roleId' });
  };

  return Role;
};
