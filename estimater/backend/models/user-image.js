const Sequelize = require('sequelize');
const User = require('./user');

module.exports = (sequelize, dataTypes) => {
  const UserImage = sequelize.define('image', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
    },
    userId: {
      type: dataTypes.INTEGER,
      references: {
        model: User,
        key: 'userId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'userid',
    },
    image: {
      type: dataTypes.STRING,
    },
  }, {
    tableName: 'usersimages',
    timestamps: false,
  });

  UserImage.associate = (models) => {
    UserImage.belongsTo(models.user);
  };

  return UserImage;
};
