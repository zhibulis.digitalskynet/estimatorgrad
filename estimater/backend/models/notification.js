const Sequelize = require('sequelize');

const User = require('./user');
const Project = require('./project');

module.exports = (sequelize, dataTypes) => {
  const Notification = sequelize.define('notification', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
    },
    userId: {
      type: dataTypes.INTEGER,
      references: {
        model: User,
        key: 'userId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'userid',
    },
    projectId: {
      type: dataTypes.INTEGER,
      references: {
        model: Project,
        key: 'projectId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'projectid',
    },
    isConfirmed: {
      type: dataTypes.BOOLEAN,
      field: 'isconfirmed',
    },
    isDeleted: {
      type: dataTypes.BOOLEAN,
      field: 'isdeleted',
    },
    description: {
      type: dataTypes.STRING,
    },
    createdDate: {
      type: dataTypes.STRING,
      field: 'createddate',
    },
    updatedDate: {
      type: dataTypes.STRING,
      field: 'updateddate',
    },
  }, {
    tableName: 'notifications',
    timestamps: false,
  });

  return Notification;
};
