const Sequelize = require('sequelize');
const Task = require('./task');

module.exports = (sequelize, dataTypes) => {
  const Blocker = sequelize.define('blocker', {
    blockerId: {
      type: dataTypes.INTEGER,
      field: 'blockerid',
      primaryKey: true,
    },
    taskId: {
      type: dataTypes.INTEGER,
      references: {
        model: Task,
        key: 'taskId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'taskid',
    },
    taskBlockerId: {
      type: dataTypes.INTEGER,
      references: {
        model: Task,
        key: 'taskId',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
      },
      field: 'taskblockerid',
    },
  }, {
    tableName: 'blockers',
    timestamps: false,
  });

  return Blocker;
};
