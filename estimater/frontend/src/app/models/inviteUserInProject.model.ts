export interface InviteUserInProject {
    inviterTokenId: string;
    projectName: string;
    projectId: number;
    createdDate: string;
    email: string;
}
