export interface Notification {
    id?: number;
    userId?: number;
    projectId?: number;
    isConfirmed?: boolean;
    isDeleted?: boolean;
    description?: string;
    createdDate?: string;
    updatedDate?: string;
}
