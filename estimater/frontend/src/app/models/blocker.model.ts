export interface Blocker {
  blockerId: number;
  taskId: number;
  taskBlockerId: number;
}
