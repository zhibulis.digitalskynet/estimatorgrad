export interface User {
    userId?: number;
    username?: string;
    email?: string;
    password?: string;
    registerDate?: string;
    firstName?: string;
    lastName?: string;
    fullName?: string;
    image?: string;
    isConfirmed?: boolean | string;
    roleId?: number;
    workingHours?: number;
}
