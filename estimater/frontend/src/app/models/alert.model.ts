export interface Alert {
    message: string;
    isError: boolean;
}
