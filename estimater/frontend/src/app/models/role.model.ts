export interface Role {
    roleId: number;
    roleName: string;
    createdDate: string;
}
