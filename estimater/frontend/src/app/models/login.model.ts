export interface LoginAnswerBody {
    token: string;
    roleName: string;
    success: boolean;
    error: string;
}
