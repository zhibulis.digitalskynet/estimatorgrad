export interface DefaultServerMessage {
    error: boolean;
    status: number;
    errorMessage: string;
}
