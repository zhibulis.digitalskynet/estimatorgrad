import { Role } from './role.model';
import { User } from './user.model';

export interface MemberWithRoleInfo extends User {
    role: Role;
}

export interface Member extends User {
    name: string;
    role: string;
    confirmation: string;
}
