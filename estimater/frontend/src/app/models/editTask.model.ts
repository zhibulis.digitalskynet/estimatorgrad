export interface EditTaskModel {
    taskId: number;
    field: string;
    value: string | number;
    assignedId?: number;
}
