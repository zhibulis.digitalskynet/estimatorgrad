export interface ProjectCore {
  projectId?: number;
  projectName?: string;
  startDate?: string;
  endDate?: string;
  projectStatusId?: number;
  currentProjectManagerId?: number;
  description?: string;
  deleted?: boolean;
  url?: string;
}
