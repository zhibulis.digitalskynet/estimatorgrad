import { User } from './user.model';

export interface InviteUserRequestBody {
    creatorTokenId: string;
    userBody: User;
}
