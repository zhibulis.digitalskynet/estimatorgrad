import { ProjectCore } from './project.model';
import { Task } from './task.model';

export interface DashboardStatistic extends ProjectCore {
  projectManagerName: string;
  projectStatusName: string;
  membersCount: number;
  tasksCount: number;
  developersCount: number;
}

export interface PieData {
  name: string;
  value: number;
}

export interface TasksHours {
  tasksCount: number;
  completedTasks: Task[];
}
