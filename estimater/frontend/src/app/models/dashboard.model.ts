import { User } from './user.model';
import { ProjectCore } from './project.model';

export interface TasksProgress {
    total: number;
    done: number;
}

export interface Project extends ProjectCore {
    projectStatusName: string;
    tasksProgress: TasksProgress;
    live: number;
    startsIn: number;
    total: number;
    members: UsersProjects[];
    users?: User[];
}

export interface UsersProjects {
    userId: number;
    projectId: number;
}

export interface DashboardUser extends User {
    projectId: number;
    notification?: number;
}
