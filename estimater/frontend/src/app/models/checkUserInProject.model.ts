export interface UserInProject {
    token: string;
    projectId: number;
}
