import { Milestone } from './milestone.model';
import { ProjectCore } from './project.model';
import { User } from './user.model';

export interface Task {
    taskId?: number;
    taskName?: string;
    optimisticTime?: number;
    pessimisticTime?: number;
    dateStart?: string;
    endStart?: string;
    projectId?: number;
    milestoneId?: number;
    completed?: number;
    assignedId?: number;
    user?: User;
    taskOrder?: number;
    milestone?: Milestone;
    project?: ProjectCore;
    taskStatusId?: number;
    taskBlockers?: TaskBlocker[];
}

export interface DisplayedTasks extends Task {
    username?: string;
    realTime: number;
    pert: number;
}

export interface TaskBlocker {
    taskId: number;
    taskBlockerId: number;
}
