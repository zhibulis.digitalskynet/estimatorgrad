export interface ResetPassword {
    passwordResetToken: string;
    password: string;
}
