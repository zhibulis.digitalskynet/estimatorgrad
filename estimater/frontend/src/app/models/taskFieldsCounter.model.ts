export interface TaskFieldToCount {
    realTime: number;
    pert: number;
}

export interface NextAvailableDay {
    endDate: Date;
    availableHoursCurrentDay: number;
    nextTaskStartDate: Date;
}
