import { DisplayedTasks, Task } from './task.model';

export interface MilestoneStatus {
    milestoneStatusId: number;
    milestoneStatusName: string;
}

export interface Milestone {
    id: number;
    name: string;
    projectId: number;
    milestoneStatusId: number;
    hours?: number;
    tasks?: Task[];
}

export interface MilestoneWithStatusModel extends Milestone {
    milestoneStatus: MilestoneStatus;
}
