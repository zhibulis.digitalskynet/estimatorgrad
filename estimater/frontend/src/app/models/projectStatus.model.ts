export interface ProjectStatus {
    projectStatusId: number;
    projectStatusName: string;
}
