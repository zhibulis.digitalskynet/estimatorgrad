export interface Note {
    noteId: number;
    noteName: string;
    note: string;
    projectId: number;
}
