export interface TaskAndBlockerList {
  taskId: number;
  taskBlockerId: number;
  taskName: string;
  taskBlockerName: string;
}
