export enum Roles {
    admin = 'Admin',
    projectManager = 'Project Manager',
    guest = 'Guest',
    developer = 'Developer',
    qa = 'QA',
}
