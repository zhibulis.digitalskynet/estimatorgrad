import { ProjectCore } from './project.model';

export interface CreateProject extends ProjectCore {
    isAdmin: boolean;
    userIdToken: string;
}
