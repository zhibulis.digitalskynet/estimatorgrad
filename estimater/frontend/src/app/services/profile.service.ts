import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MemberWithRoleInfo, Task, DefaultServerMessage } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class ProfileService {
    constructor(private http: HttpClient) { }

    getMemberInfo(idToken: string): Observable<MemberWithRoleInfo> {
        return this.http.get<MemberWithRoleInfo>(`/api/profile/member-info/${idToken}/get`);
    }

    getTasks(idToken: string): Observable<Task[]> {
        return this.http.get<Task[]>(`/api/profile/tasks/${idToken}/get`);
    }

    getProjectsCount(idToken: string): Observable<number> {
        return this.http.get<number>(`/api/profile/projects/${idToken}/get`);
    }

    changeProfileAvatar(idToken: string, file): Observable<DefaultServerMessage> {
        return this.http.put<DefaultServerMessage>(`/api/profile/avatar/${idToken}/put`, { file });
    }
}
