import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DashboardStatistic, PieData, TasksHours } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class DashboardStatisticService {

  constructor(private http: HttpClient) { }

  getPieData(): Observable<PieData[]> {
    return this.http.get<PieData[]>(`/api/dashboard-statistic/pie-data/get`);
  }

  getAllDashboardProjects(): Observable<DashboardStatistic[]> {
    return this.http.get<DashboardStatistic[]>(`/api/dashboard-statistic/get`);
  }

  getProjectsWhereExist(idToken: string): Observable<DashboardStatistic[]> {
    return this.http.get<DashboardStatistic[]>(`/api/dashboard-statistic/${idToken}/get`);
  }

  getTasksHoursWidgetData(): Observable<TasksHours> {
    return this.http.get<TasksHours>(`/api/dashboard-statistic/tasks-hours/get`);
  }

  getProjectStatusWidgetData(): Observable<PieData[]> {
    return this.http.get<PieData[]>(`/api/dashboard-statistic/project-status/get`);
  }

}
