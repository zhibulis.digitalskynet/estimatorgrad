import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DefaultServerMessage, InviteUserInProject, UsersProjects } from '@estimator/models';

@Injectable()
export class UsersInProjectService {
    constructor(private http: HttpClient) { }

    checkUserEmail(body: InviteUserInProject): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`api/user-in-project/invite-user-in-project`, body);
    }
    deleteUserFromProject(body: UsersProjects): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`api/user-in-project/delete-user-from-project`, body);
    }
}
