import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, LoginAnswerBody } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class LoginService {

    constructor(private http: HttpClient) { }

    tryLogin(loginData: User): Observable<LoginAnswerBody> {
        return this.http.post<LoginAnswerBody>('api/login', loginData);
    }

}
