import { Injectable, EventEmitter } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class SharedService {
    headerReload = new EventEmitter();
    activeTabProjectSettings = 0;
}
