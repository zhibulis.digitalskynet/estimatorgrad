import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  MemberWithRoleInfo, DefaultServerMessage,
  Milestone, MilestoneStatus, MilestoneWithStatusModel,
  TaskAndBlockerList, Note, ProjectCore, ProjectStatus, Task
} from '@estimator/models';
import { GetCountTaskFields } from './taskFieldsCounter.service';

@Injectable()
export class ProjectSettingsService {
  constructor(private http: HttpClient) { }

  getProjectInfo(projectId: number): Observable<ProjectCore> {
    return this.http.get<ProjectCore>(`/api/project-settings/${projectId}`);
  }

  checkProjectExist(projectId: number): Observable<boolean> {
    return this.http.get<boolean>(`/api/project-settings/check-project-exist/${projectId}`);
  }

  getAllProjectManagers(): Observable<MemberWithRoleInfo[]> {
    return this.http.get<MemberWithRoleInfo[]>(`/api/project-settings/project-managers/get`);
  }

  getProjectNotes(projectId: number): Observable<Note[]> {
    return this.http.get<Note[]>(`/api/project-settings/notes/get/${projectId}`);
  }

  getMilestones(projectId: number): Observable<MilestoneWithStatusModel[]> {
    return this.http.get<MilestoneWithStatusModel[]>(`/api/project-settings/milestones/get-all/${projectId}`);
  }

  getProjectStatuses(projectId: number): Observable<ProjectStatus[]> {
    return this.http.get<ProjectStatus[]>(`/api/project-settings/project-statuses/get/${projectId}`);
  }

  getBlockers(projectId: number): Observable<TaskAndBlockerList[]> {
    return this.http.get<TaskAndBlockerList[]>(`/api/project-settings/blockers/get-all/${projectId}`);
  }

  getMembers(projectId: number): Observable<MemberWithRoleInfo[]> {
    return this.http.get<MemberWithRoleInfo[]>(`/api/project-settings/members/get-all/${projectId}`);
  }

  getMilestoneStatuses(): Observable<MilestoneStatus[]> {
    return this.http.get<MilestoneStatus[]>(`/api/project-settings/milestones/get`);
  }

  updateProjectInfo(body: ProjectCore): Observable<DefaultServerMessage> {
    return this.http.post<DefaultServerMessage>(`/api/project-settings/update`, body);
  }

  addMilestoneToProject(body: Milestone): Observable<DefaultServerMessage> {
    return this.http.put<DefaultServerMessage>(`/api/project-settings/milestones/insert`, body);
  }

  addBlockerToTask(body: TaskAndBlockerList): Observable<DefaultServerMessage> {
    return this.http.post<DefaultServerMessage>(`/api/project-settings/blockers/insert`, body);
  }

  addNote(body: Note): Observable<DefaultServerMessage> {
    return this.http.put<DefaultServerMessage>(`/api/project-settings/notes/insert`, body);
  }

  deleteTaskBlockers(body: TaskAndBlockerList[]): Observable<DefaultServerMessage> {
    return this.http.post<DefaultServerMessage>(`/api/project-settings/blockers/delete`, body);
  }

  deleteMembers(projectId: number, body: number[], projectManagerId: number): Observable<DefaultServerMessage> {
    return this.http.post<DefaultServerMessage>(`/api/project-settings/members/delete/${projectId}/${projectManagerId}`, body);
  }

  deleteNotes(body: number[]): Observable<DefaultServerMessage> {
    return this.http.post<DefaultServerMessage>(`/api/project-settings/notes/delete`, body);
  }

  deleteMilestones(body: number[]): Observable<DefaultServerMessage> {
    return this.http.post<DefaultServerMessage>(`/api/project-settings/milestones/delete`, body);
  }
}

export class MilestonesService {
  static getCountMilestoneTasks(tasks: Task[]) {
    let hours = 0;
    tasks.forEach((task) => {
      const { pessimisticTime, optimisticTime } = task;
      const pertAndRealTime = GetCountTaskFields.countTaskFields(optimisticTime, pessimisticTime);
      hours += pertAndRealTime.pert;
    });
    return hours;
  }
}
