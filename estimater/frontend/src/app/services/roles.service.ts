import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthServiceJwt } from './auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class RolesService {
    constructor(private http: HttpClient, private readonly authService: AuthServiceJwt) { }

    getRoleName(): Observable<string> {
        const token = this.authService.getToken();
        return this.http.get<string>(`/api/header/get-role/${token}`);
    }
}
