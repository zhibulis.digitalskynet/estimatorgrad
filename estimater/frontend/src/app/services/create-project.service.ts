import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProjectStatus, CreateProject, DefaultServerMessage } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class CreateProjectService {

    constructor(private http: HttpClient) { }

    createProject(project: CreateProject): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`api/create-project`, project);
    }

    existCreateProject(project: CreateProject): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`api/create-project/delete-exist`, project);
    }

    restoreProject(project: CreateProject): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`api/create-project/restore`, project);
    }

    getProjectStatusList(): Observable<ProjectStatus[]> {
        return this.http.get<ProjectStatus[]>(`api/create-project/get-status`);
    }
}
