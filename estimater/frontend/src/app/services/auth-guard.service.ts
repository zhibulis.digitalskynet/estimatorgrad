import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthServiceJwt } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthServiceJwt, public router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      localStorage.removeItem('token');
      this.router.navigate(['login']);
      this.auth.navBarToggle(false);
      return false;
    }
    this.auth.navBarToggle(true);
    return true;
  }
}
