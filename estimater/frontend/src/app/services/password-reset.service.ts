import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DefaultServerMessage, ResetPassword } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class PasswordResetService {
  constructor(private http: HttpClient) { }

  sendPasswordResetRequest(email: string): Observable<DefaultServerMessage> {
    return this.http.get<DefaultServerMessage>(`/api/password-reset/generate-token/${email}/get`);
  }

  resetPassword(body: ResetPassword): Observable<DefaultServerMessage> {
    return this.http.post<DefaultServerMessage>(`/api/password-reset/set-new-password/post`, body);
  }

  checkTokenValid(resetPasswordToken: string): Observable<DefaultServerMessage> {
    return this.http.get<DefaultServerMessage>(`/api/password-reset/check-token/${resetPasswordToken}/get`);
  }
}
