import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User, Notification } from '@estimator/models';

@Injectable()
export class HeaderService {

  showHeader = false;

  constructor(private http: HttpClient) { }

  toggleNavbarEvent = new EventEmitter();

  toggle(value: boolean) {
    this.showHeader = value;
    this.toggleNavbarEvent.emit(this.showHeader);
  }

  getHeaderStatus() {
    return this.showHeader;
  }

  getProfileData(token: string): Observable<User> {
    return this.http.get<User>(`api/header/${token}`);
  }

  getNotifications(idToken: string): Observable<Notification[]> {
    return this.http.get<Notification[]>(`/api/header/get-notification/${idToken}`);
  }
}
