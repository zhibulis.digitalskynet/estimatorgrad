import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProjectDropdown, Task } from '@estimator/models';

@Injectable()
export class EstimateChartService {

    constructor(private http: HttpClient) { }

    getTasks(projectId: number): Observable<Task[]> {
        return this.http.get<Task[]>(`/api/estimate-chart/tasks/${projectId}/get`);
    }

    getProjectsList(idToken: string): Observable<ProjectDropdown[]> {
        return this.http.get<ProjectDropdown[]>(`/api/estimate-chart/get-projects/${idToken}`);
    }

    getAllProjectsList(): Observable<ProjectDropdown[]> {
        return this.http.get<ProjectDropdown[]>(`/api/estimate-chart/get-all-projects`);
    }
}
