import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { HeaderService } from './header.service';

@Injectable()
export class AuthServiceJwt {
  constructor(
    private headerService: HeaderService,
    public jwtHelper: JwtHelperService,
    private router: Router
  ) { }


  login(token: string) {
    localStorage.setItem('token', token);
    this.navBarToggle(true);
    this.router.navigateByUrl('');
  }

  navBarToggle(value: boolean) {
    this.headerService.toggle(value);
  }

  getToken() {
    if (this.isAuthenticated()) {
      return localStorage.getItem('token');
    } else {
      this.logout();
    }
  }

  logout() {
    localStorage.removeItem('token');
    this.navBarToggle(false);
    this.router.navigateByUrl('login');
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }
}
