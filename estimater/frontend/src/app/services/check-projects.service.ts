import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CheckProjectService {

    constructor(private http: HttpClient) { }

    checkProjects(idToken: string): Observable<boolean> {
        return this.http.post<boolean>(`api/home/check-projects/${idToken}`, {});
    }
}
