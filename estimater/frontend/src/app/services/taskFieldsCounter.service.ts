import { TaskFieldToCount, NextAvailableDay } from '@estimator/models';

export class GetCountTaskFields {
  static countTaskFields(optimisticTime: number, pessimisticTime: number): TaskFieldToCount {
    /*Pert=(PW*PES+RW*REAL+OW*OP)/(PW+RW+OW)
        PW - Pessimistic Weight = 1
        RW - Realistic Weight = 4
        OW - Optimistic Weight = 1
        PES - Pessimistic Time Value
        OP - Optimistic Time Value
        REAL- Realistic Time Value = (OP*PES)/2*/
    const realTime = (pessimisticTime + optimisticTime) / 2;
    const pert = Math.round((pessimisticTime + realTime * 4 + optimisticTime) / (6) * 100) / 100;
    return {
      realTime,
      pert
    };
  }

  static checkWeekend(dateToCheck: Date): number {
    switch (dateToCheck.getDay()) {
      case 0:
        return 1;
      case 6:
        return 2;
      default:
        return 0;
    }
  }

  static getNextAvailableDay(startDate: string, availableHours: number, hoursPerDay: number, pert: number): NextAvailableDay {
    let daysIterator = 0;
    let stopFlag = false;
    let endDate: Date;
    let currentHoursVariable = availableHours;
    let nextTaskStartDate: Date;
    let availableHoursCurrentDay: number;
    while (!stopFlag) {
      if (currentHoursVariable - pert > 0) {
        endDate = new Date(startDate);
        for (let i = 0; i < daysIterator; i++) {
          endDate.setDate(endDate.getDate() + 1);
          endDate.setDate(endDate.getDate() + this.checkWeekend(endDate));
        }
        availableHoursCurrentDay = currentHoursVariable - pert;
        nextTaskStartDate = new Date(endDate);
        stopFlag = true;
      } else if (currentHoursVariable - pert === 0) {
        endDate = new Date(startDate);
        for (let i = 0; i < daysIterator; i++) {
          endDate.setDate(endDate.getDate() + 1);
          endDate.setDate(endDate.getDate() + this.checkWeekend(endDate));
        }
        availableHoursCurrentDay = hoursPerDay;
        nextTaskStartDate = new Date(endDate);
        nextTaskStartDate.setDate(endDate.getDate() + 1);
        nextTaskStartDate.setDate(nextTaskStartDate.getDate() + this.checkWeekend(nextTaskStartDate));
        stopFlag = true;
      } else if (currentHoursVariable - pert < 0) {
        currentHoursVariable += hoursPerDay;
        daysIterator += 1;
      }
    }
    return { endDate, availableHoursCurrentDay, nextTaskStartDate };
  }
}
