import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Member, DefaultServerMessage, InviteUserRequestBody, Role } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class MembersService {
    constructor(private http: HttpClient) { }

    getRoles(): Observable<Role[]> {
        return this.http.get<Role[]>(`/api/members/get-roles`);
    }

    getMembers(): Observable<Member[]> {
        return this.http.get<Member[]>(`/api/members/get-all`);
    }

    deleteMembers(membersId: number[]): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`/api/members/delete-members`, membersId);
    }

    addMember(inviteUser: InviteUserRequestBody): Observable<DefaultServerMessage> {
        return this.http.put<DefaultServerMessage>(`/api/members/add-member`, inviteUser);
    }
}
