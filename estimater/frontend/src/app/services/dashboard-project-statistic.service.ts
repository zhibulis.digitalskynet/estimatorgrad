import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PieData, TasksHours, ProjectCore } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class DashboardProjectStatisticService {

  constructor(private http: HttpClient) { }

  getProjectName(projectId: number): Observable<ProjectCore> {
    return this.http.get<ProjectCore>(`/api/dashboard-project-statistic/project-name/${projectId}/get`);
  }

  getPieData(projectId: number): Observable<PieData[]> {
    return this.http.get<PieData[]>(`/api/dashboard-project-statistic/pie-data/${projectId}/get`);
  }

  getTasksHoursWidgetData(projectId: number): Observable<TasksHours> {
    return this.http.get<TasksHours>(`/api/dashboard-project-statistic/tasks-hours/${projectId}/get`);
  }
}
