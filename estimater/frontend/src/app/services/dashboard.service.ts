import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project, User, DefaultServerMessage } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }
  deleteProject(projectId: number): Observable<boolean> {
    return this.http.delete<boolean>(`/api/dashboard/delete-project/${projectId}`);
  }

  getDashboardData(idToken: string): Observable<Project[]> {
    return this.http.post<Project[]>(`/api/dashboard-data/${idToken}`, {});
  }

  getUsersData(): Observable<User[]> {
    return this.http.get<User[]>(`/api/dashboard/load-users/get`);
  }

  getAllProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(`/api/dashboard/get-projects`);
  }

  leaveProject(idToken: string, currentProjectManagerId: number, projectId: number): Observable<DefaultServerMessage> {
    return this.http.delete<DefaultServerMessage>(`/api/dashboard/user-project/${idToken}/${currentProjectManagerId}/${projectId}/delete`);
  }

}
