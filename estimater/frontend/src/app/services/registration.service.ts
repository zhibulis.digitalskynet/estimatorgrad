import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, DefaultServerMessage, Role } from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class RegistrationService {
  constructor(private http: HttpClient) { }

  registrate(userInfo: User): Observable<DefaultServerMessage> {
    return this.http.post<DefaultServerMessage>('api/registration', userInfo);
  }

  checkEmail(email: string): Observable<boolean> {
    return this.http.post<boolean>(`/api/registration/check-email/${email}`, {});
  }

  checkUsername(username: string): Observable<boolean> {
    return this.http.post<boolean>(`/api/registration/check-username/${username}`, {});
  }

  getRoles(): Observable<Role[]> {
    return this.http.get<Role[]>('/api/registration/roles');
  }
}
