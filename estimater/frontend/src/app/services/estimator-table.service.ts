import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
    Task, DefaultServerMessage, UserInProject,
    EditTaskModel, BlockersOnTask, Milestone, MemberWithRoleInfo
} from '@estimator/models';
import { Observable } from 'rxjs';

@Injectable()
export class EstimatorTableService {

    constructor(private http: HttpClient) { }

    addTask(task: Task): Observable<DefaultServerMessage> {
        return this.http.put<DefaultServerMessage>('api/table/create-task', task);
    }

    importTasks(tasks: Task[]): Observable<DefaultServerMessage> {
        return this.http.put<DefaultServerMessage>('/api/table/import-table', tasks);
    }

    checkUserInProject(userProject: UserInProject): Observable<boolean> {
        return this.http.get<boolean>(`api/table/${userProject.token}/${userProject.projectId}`);
    }

    getMilestones(projectId: number): Observable<Milestone[]> {
        return this.http.get<Milestone[]>(`api/table/milestones/${projectId}/get`);
    }

    getTasks(projectId: number): Observable<Task[]> {
        return this.http.get<Task[]>(`api/table/${projectId}`);
    }

    deleteTasks(tasksId: number[]): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`/api/table/delete-task`, tasksId);
    }

    updateProjectStatus(statusId: number, projectId: number): Observable<DefaultServerMessage> {
        return this.http.put<DefaultServerMessage>(`/api/table/project-status/${projectId}/update`, { statusId });
    }

    getProjectName(projectId: number): Observable<string> {
        return this.http.get<string>(`api/table/get-project-name/${projectId}`);
    }

    editTask(editTask: EditTaskModel): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>('/api/table/change-task', editTask);
    }

    getUsersByProjectId(projectId: number): Observable<MemberWithRoleInfo[]> {
        return this.http.get<MemberWithRoleInfo[]>(`/api/table/get-users/${projectId}`);
    }

    editBlockersOnTask(taskWithBlockers: BlockersOnTask): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>('api/table/add-blockers', taskWithBlockers);
    }

    getAvailableWorkingHoursProject(projectId: number): Observable<number> {
        return this.http.get<number>(`/api/table/working-hours/${projectId}`);
    }

    updateTasksStartDate(tasks: Task[]): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`/api/table/update-start-date`, tasks);
    }

    updateTaskOrders(tasks: Task[]): Observable<DefaultServerMessage> {
        return this.http.post<DefaultServerMessage>(`/api/table/switch-tasks`, tasks);
    }
}
