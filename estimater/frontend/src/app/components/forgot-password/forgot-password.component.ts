import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { PasswordResetService, Patterns } from '@estimator/services';
import { NgForm } from '@angular/forms';
import { DefaultServerMessage } from '@estimator/models';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertComponent } from '../alert/alert.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers: [PasswordResetService, AlertComponent]
})
export class ForgotPasswordComponent implements OnInit {

  emailPattern = Patterns.emailPattern;

  @ViewChild('successDialog', {static: false}) successDialog: TemplateRef<unknown>;

  constructor(
    private passwordResetService: PasswordResetService,
    private alert: MatSnackBar,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    const token = localStorage.getItem('token');
    if (token) {
      this.router.navigateByUrl('not-found');
    }
  }

  openAlert(message: string, isError: boolean, duration: number) {
    this.alert.openFromComponent(AlertComponent, {
      duration,
      panelClass: isError ? ['alert-danger'] : ['alert-success'],
      data: {
        isError,
        message
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(this.successDialog);
    dialogRef.afterClosed().subscribe(() => {
      this.router.navigateByUrl('login');
    });
  }

  onSendResetRequest(form: NgForm) {
    if (form.invalid) {
      return;
    }

    this.passwordResetService
      .sendPasswordResetRequest(form.value.email)
      .subscribe((answer: DefaultServerMessage) => {
        if (answer.error) {
          this.openAlert(answer.errorMessage, answer.error, 2500);
        } else {
          this.openDialog();
        }
      });
  }
}
