import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { PasswordResetService } from '@estimator/services';
import { ActivatedRoute, Router } from '@angular/router';
import { DefaultServerMessage, ResetPassword } from '@estimator/models';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AlertComponent } from '../alert/alert.component';

@Component({
  selector: 'app-invite-password-set',
  templateUrl: './invite-password-set.component.html',
  styleUrls: ['./invite-password-set.component.css'],
  providers: [PasswordResetService, AlertComponent],
})
export class InvitePasswordSetComponent implements OnInit {

  setPasswordToken: string;
  contentHidden = true;

  @ViewChild('successDialog', { static: false }) successDialog: TemplateRef<unknown>;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private passwordResetService: PasswordResetService,
    private alert: MatSnackBar,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.setPasswordToken = this.route.snapshot.paramMap.get('setPasswordToken');
    if (!this.setPasswordToken) {
      this.router.navigateByUrl('not-found');
    }
    this.passwordResetService
      .checkTokenValid(this.setPasswordToken)
      .subscribe((answer: DefaultServerMessage) => {
        if (answer.error) {
          this.router.navigateByUrl('not-found');
        } else {
          this.contentHidden = false;
        }
      });
  }

  openAlert(message: string, isError: boolean, duration: number) {
    this.alert.openFromComponent(AlertComponent, {
      duration,
      panelClass: isError ? ['alert-danger'] : ['alert-success'],
      data: {
        isError,
        message
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(this.successDialog);
    dialogRef.afterClosed().subscribe(() => {
      this.router.navigateByUrl('login');
    });
  }

  onSetPassword(form: NgForm) {
    if (form.invalid) {
      return;
    }
    const passwordResetBody: ResetPassword = {
      password: form.value.password,
      passwordResetToken: this.setPasswordToken
    };
    this.passwordResetService
      .resetPassword(passwordResetBody)
      .subscribe((answer: DefaultServerMessage) => {
        if (answer.error) {
          this.openAlert(answer.errorMessage, answer.error, 2500);
          this.router.navigateByUrl('login');
        } else {
          this.openDialog();
        }
      });
  }
}
