import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PieData, Roles, UserInProject, TasksHours, ProjectCore } from '@estimator/models';
import { RolesService, EstimatorTableService, GetCountTaskFields, DashboardProjectStatisticService } from '@estimator/services';

@Component({
  selector: 'app-dashboard-project-statistic',
  templateUrl: './dashboard-project-statistic.component.html',
  styleUrls: ['./dashboard-project-statistic.component.css'],
  providers: [RolesService, EstimatorTableService, DashboardProjectStatisticService]
})
export class DashboardProjectStatisticComponent implements OnInit, AfterViewInit {

  projectId: number;
  projectName = '';
  totalTasks = 0;
  totalTimeLost = 0;
  dataLoaded = false;
  rolesExist = false;
  // pie options
  pieData: PieData[];
  pieProjectStatusData: PieData[];
  doughnut = true;
  arcWidth = 0.6;
  showLegend = true;
  showLabels = true;
  isDoughnut = false;
  pieLegendPosition = 'below';
  pieProjectLegendPosition = 'below';
  pieColorTheme = {
    domain: ['#9cce04', '#fb634b', '#ffe008']
  };
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private roleService: RolesService,
    private tableService: EstimatorTableService,
    private dashboardProjectStatisticService: DashboardProjectStatisticService
  ) { }

  ngOnInit() {
    this.projectId = +this.route.snapshot.paramMap.get('projectId');
    if (!this.projectId) {
      this.router.navigateByUrl('not-found');
    } else {
      this.loadRoleFunctionality();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    });
  }

  loadRoleFunctionality() {
    this.roleService
      .getRoleName()
      .subscribe((answer) => {
        if (answer === Roles.admin || answer === Roles.projectManager) {
          this.loadData();
        } else {
          this.checkUser();
        }
      });
  }

  checkUser() {
    const userProject: UserInProject = {
      token: localStorage.getItem('token'),
      projectId: this.projectId
    };

    this.tableService
      .checkUserInProject(userProject)
      .subscribe((answer) => {
        if (answer) {
          this.loadData();
        } else {
          this.router.navigateByUrl('not-found');
        }
      });
  }

  loadData() {
    this.loadProjectName();
    this.loadTasksHoursWidgetData();
    this.loadPieData();
  }

  loadProjectName() {
    this.dashboardProjectStatisticService
      .getProjectName(this.projectId)
      .subscribe((project: ProjectCore) => {
        this.projectName = project.projectName;
      });
  }

  loadTasksHoursWidgetData() {
    this.dashboardProjectStatisticService
      .getTasksHoursWidgetData(this.projectId)
      .subscribe((answer: TasksHours) => {
        this.totalTasks = answer.tasksCount;
        answer.completedTasks.forEach((task) => {
          this.totalTimeLost += GetCountTaskFields
            .countTaskFields(task.optimisticTime, task.pessimisticTime).pert;
        });
      });
  }

  loadPieData() {
    this.dashboardProjectStatisticService
      .getPieData(this.projectId)
      .subscribe((answer: PieData[]) => {
        this.rolesExist = !!answer.find((elem) => elem.value > 0) || false;
        this.pieData = answer;
        this.dataLoaded = true;
        setTimeout(() => {
          window.dispatchEvent(new Event('resize'));
        });
      });
  }
}
