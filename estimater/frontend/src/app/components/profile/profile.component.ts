import { Component, ElementRef, AfterViewInit, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService, GetCountTaskFields, SharedService, EstimatorTableService } from '@estimator/services';
import { MemberWithRoleInfo, Task, DisplayedTasks, EditTaskModel, DefaultServerMessage } from '@estimator/models';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AlertComponent } from '../alert/alert.component';
import { DatePipe } from '@angular/common';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [EstimatorTableService, ProfileService, DatePipe]
})
export class ProfileComponent implements OnInit, AfterViewInit, OnDestroy {
  routeToken: string;
  memberInfo: MemberWithRoleInfo;
  tasksForTodayCount = 0;
  myProjectsCount = 0;
  headerTasksInfoLoaded = false;
  currentRowIndex: number;
  currentPercentValue = 0; uploadActivated = false;
  currentDate: string;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  memberInfoLoaded = false;
  totalTasksLoaded = false;
  newTasksLoaded = false;
  tasksInProgressLoaded = false;
  totalTasks: MatTableDataSource<DisplayedTasks>;
  newTasks: MatTableDataSource<DisplayedTasks>;
  tasksInProgress: MatTableDataSource<DisplayedTasks>;
  displayedColumns = ['projectName', 'taskName', 'dateStart', 'pert', 'completed'];
  @ViewChild(MatPaginator, { static: true }) pagTotal: MatPaginator;
  @ViewChild('sortTotal', { static: true }) sortTotal: MatSort;
  @ViewChild('sortNew', { static: true }) sortNew: MatSort;
  @ViewChild('sortProgress', { static: true }) sortProgress: MatSort;
  constructor(
    private elementRef: ElementRef,
    private route: ActivatedRoute,
    private router: Router,
    private sharedService: SharedService,
    private dialog: MatDialog,
    private profileService: ProfileService,
    private tableService: EstimatorTableService,
    private alert: MatSnackBar,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit() {
    this.routeToken = this.route.snapshot.paramMap.get('id');
    const token = localStorage.getItem('token');
    if (token !== this.routeToken) {
      this.router.navigateByUrl('not-found');
    } else {
      this.currentDate = this.datePipe.transform(new Date(), 'yyyy/MM/dd');
      this.loadCoreMemberInfo();
      this.loadTasks();
    }
  }

  loadCoreMemberInfo() {
    this.profileService
      .getMemberInfo(this.routeToken)
      .subscribe((answer: MemberWithRoleInfo) => {
        this.memberInfo = answer;
        this.memberInfoLoaded = true;
      });
  }

  headerTasksCounter() {
    this.tasksForTodayCount = this.totalTasks.data
      .filter((task) => new Date(task.dateStart) < new Date() && task.completed < 100).length;
    this.profileService
      .getProjectsCount(this.routeToken)
      .subscribe((answer) => {
        this.myProjectsCount = answer;
        this.headerTasksInfoLoaded = true;
      });
  }

  loadTasksInProgress(tasks: DisplayedTasks[]) {
    this.tasksInProgress = new MatTableDataSource(tasks.filter(task => task.completed > 0 && task.completed < 100));
    this.tasksInProgress.sort = this.sortProgress;
    this.tasksInProgressLoaded = true;
    this.tasksInProgress.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'taskName': {
          return item.taskName.length;
        }
        case 'projectName': {
          return item.project.projectName.length;
        }
        default: {
          return item[property];
        }
      }
    };
  }

  doubleClick(index: number, taskId: number) {
    const item = this.totalTasks.data
      .find(task => task.taskId === taskId);
    this.currentPercentValue = item.completed;
    this.currentRowIndex = index;
  }

  saveEditCell(taskId: number, inputValue: number) {
    const editTask: EditTaskModel = {
      taskId,
      field: 'completed',
      value: inputValue,
      assignedId: 1
    };
    this.tableService
      .editTask(editTask)
      .subscribe((answer: DefaultServerMessage) => {
        this.openAlert(answer.error ? answer.errorMessage : 'Task was edited successfully', answer.error, 2500);
        if (!answer.error) {
          const totalIndex = this.totalTasks.data.findIndex(task => task.taskId === taskId);
          this.totalTasks.data[totalIndex].completed = inputValue;
          this.tasksInProgress.data = this.totalTasks.data.filter(item => item.completed > 0 && item.completed < 100);
          this.newTasks.data = this.totalTasks.data.filter(item => item.completed === 0);
          this.headerTasksCounter();
          this.cancelEditCell();
        }
      });
  }

  cancelEditCell() {
    this.currentRowIndex = null;
  }

  loadNewTasks(tasks: DisplayedTasks[]) {
    this.newTasks = new MatTableDataSource(tasks.filter(task => task.completed === 0));
    this.newTasks.sort = this.sortNew;
    this.newTasksLoaded = true;
    this.newTasks.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'taskName': {
          return item.taskName.length;
        }
        case 'projectName': {
          return item.project.projectName.length;
        }
        default: {
          return item[property];
        }
      }
    };
  }

  loadTotalTasks(tasks: DisplayedTasks[]) {
    this.totalTasks = new MatTableDataSource(tasks);
    this.headerTasksCounter();
    this.totalTasks.paginator = this.pagTotal;
    this.totalTasks.sort = this.sortTotal;
    this.totalTasksLoaded = true;
    this.totalTasks.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'taskName': {
          return item.taskName.length;
        }
        case 'projectName': {
          return item.project.projectName.length;
        }
        default: {
          return item[property];
        }
      }
    };
  }

  loadTasks() {
    this.profileService
      .getTasks(this.routeToken)
      .subscribe((answer: Task[]) => {
        const tasks: DisplayedTasks[] = [];
        answer.forEach((task) => {
          const pertAndRealTime = GetCountTaskFields
            .countTaskFields(task.optimisticTime, task.pessimisticTime);
          tasks.push({
            taskId: task.taskId,
            project: task.project,
            taskName: task.taskName,
            dateStart: task.dateStart,
            completed: task.completed,
            realTime: pertAndRealTime.realTime,
            pert: pertAndRealTime.pert
          });
        });
        this.loadTotalTasks(tasks);
        this.loadNewTasks(tasks);
        this.loadTasksInProgress(tasks);
      });
  }

  openDialog(templateRef) {
    this.dialog.open(templateRef, {
      width: '410px',
    });
  }

  openAlert(message: string, isError: boolean, duration: number) {
    this.alert.openFromComponent(AlertComponent, {
      duration,
      panelClass: isError ? ['alert-danger'] : ['alert-success'],
      data: {
        isError,
        message
      }
    });
  }

  myUploader(event) {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  uploadCroppedImage() {
    const base64Image = this.croppedImage.slice(22);
    this.profileService
      .changeProfileAvatar(this.routeToken, base64Image)
      .subscribe((answer) => {
        this.openAlert(answer.error ? answer.errorMessage : 'Updated successfully', answer.error, 2500);
        if (!answer.error) {
          this.loadCoreMemberInfo();
          this.dialog.closeAll();
          this.sharedService.headerReload.emit();
        }
      });
  }

  viewGetLastName() {
    return this.memberInfo.lastName || 'Not Set';
  }

  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
  }

  viewGetFullName() {
    if (!this.memberInfo) {
      return 'Loading...';
    } else {
      return `${this.memberInfo.firstName} ${this.memberInfo.lastName || ''}`;
    }
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'whitesmoke';
  }
  ngOnDestroy() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '';
  }
}
