import { Component, OnInit } from '@angular/core';
import { DefaultServerMessage, UsersProjects } from '@estimator/models';
import { UsersInProjectService } from '@estimator/services';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-delete-invite',
  templateUrl: './delete-invite.component.html',
  styleUrls: ['./delete-invite.component.css'],
  providers: [UsersInProjectService]
})
export class DeleteInviteComponent implements OnInit {
  userid: number;
  projectid: number;
  serverError = false;
  successDelete = false;
  constructor(
    private usersInProjectService: UsersInProjectService,
    private router: Router,
    private route: ActivatedRoute, ) { }

  ngOnInit() {
    const projectId = +this.route.snapshot.paramMap.get('projectId');
    const userId = +this.route.snapshot.paramMap.get('userId');
    const deletedUserBody: UsersProjects = {
      userId,
      projectId
    };
    this.usersInProjectService
      .deleteUserFromProject(deletedUserBody)
      .subscribe((answer: DefaultServerMessage) => {
        if (answer.error) {
          this.serverError = true;
        } else {
          this.successDelete = true;
        }
      });
  }
  navigateToLogin() {
    this.router.navigateByUrl('login');
  }
}
