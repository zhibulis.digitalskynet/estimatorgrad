import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { PasswordResetService } from '@estimator/services';
import { ResetPassword, DefaultServerMessage } from '@estimator/models';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertComponent } from '../alert/alert.component';

@Component({
  selector: 'app-forgot-password-reset',
  templateUrl: './forgot-password-reset.component.html',
  styleUrls: ['./forgot-password-reset.component.css'],
  providers: [PasswordResetService, AlertComponent]
})
export class ForgotPasswordResetComponent implements OnInit {

  passwordResetToken: string;
  contentHidden = true;

  @ViewChild('successDialog', {static: false}) successDialog: TemplateRef<unknown>;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private passwordResetService: PasswordResetService,
    private alert: MatSnackBar,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    const token = localStorage.getItem('token');
    this.passwordResetToken = this.route.snapshot.paramMap.get('resetPasswordToken');
    if (!this.passwordResetToken || token) {
      this.router.navigateByUrl('not-found');
    }
    this.passwordResetService
      .checkTokenValid(this.passwordResetToken)
      .subscribe((answer: DefaultServerMessage) => {
        if (answer.error) {
          this.router.navigateByUrl('not-found');
        } else {
          this.contentHidden = false;
        }
      });
  }

  openAlert(message: string, isError: boolean, duration: number) {
    this.alert.openFromComponent(AlertComponent, {
      duration,
      panelClass: isError ? ['alert-danger'] : ['alert-success'],
      data: {
        isError,
        message
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(this.successDialog);
    dialogRef.afterClosed().subscribe(() => {
      this.router.navigateByUrl('login');
    });
  }

  onPasswordReset(form: NgForm) {
    if (form.invalid) {
      return;
    }
    const passwordResetBody: ResetPassword = {
      password: form.value.password,
      passwordResetToken: this.passwordResetToken
    };
    this.passwordResetService
      .resetPassword(passwordResetBody)
      .subscribe((answer: DefaultServerMessage) => {
        if (answer.error) {
          this.openAlert(answer.errorMessage, answer.error, 2500);
          this.router.navigateByUrl('login');
        } else {
          this.openDialog();
        }
      });
  }
}
