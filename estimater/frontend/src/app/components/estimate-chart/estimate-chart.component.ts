import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GanttEditorComponent, GanttEditorOptions } from 'ng-gantt';
import { EstimatorTableService, EstimateChartService, RolesService, GetCountTaskFields } from '@estimator/services';
import { ActivatedRoute } from '@angular/router';
import { Task, ProjectDropdown, Roles } from '@estimator/models';
import { DatePipe } from '@angular/common';
import html2canvas from 'html2canvas';
import * as jspdf from 'jspdf';

@Component({
  selector: 'app-estimate-chart',
  templateUrl: './estimate-chart.component.html',
  styleUrls: ['./estimate-chart.component.css'],
  providers: [EstimatorTableService, EstimateChartService, RolesService, DatePipe]
})
export class EstimateChartComponent implements OnInit {
  @ViewChild('editor', { static: false }) editor: GanttEditorComponent;
  @ViewChild('gantt', { static: false }) gantt: ElementRef;
  public editorOptions: GanttEditorOptions;
  public data: any;
  projectId: number;
  export = false;
  selected = false;
  availableWorkingHours: number;
  noDevelopersOnProject = false;
  projectsDropdown: ProjectDropdown[] = [];
  hidden = true;
  constructor(
    public fb: FormBuilder,
    private tableService: EstimatorTableService,
    private route: ActivatedRoute,
    private roleService: RolesService,
    private datePipe: DatePipe,
    private chartService: EstimateChartService) {
  }

  ngOnInit() {
    this.projectId = +this.route.snapshot.paramMap.get('projectId');
    this.export = !!this.route.snapshot.paramMap.get('export');
    const token = localStorage.getItem('token');
    this.roleService
      .getRoleName()
      .subscribe((answer) => {
        if (answer === Roles.admin || answer === Roles.projectManager) {
          this.chartService
            .getAllProjectsList()
            .subscribe((projects: ProjectDropdown[]) => {
              this.projectsDropdown = projects;
              this.checkIfProjectsExist();
            });
        } else {
          this.chartService
            .getProjectsList(token)
            .subscribe((projects: ProjectDropdown[]) => {
              this.projectsDropdown = projects;
              this.checkIfProjectsExist();
            });
        }
      });

    if (this.projectId) {
      this.loadWorkingHours(this.projectId);
    }
  }

  checkIfProjectsExist() {
    if (this.projectsDropdown.length && !this.projectId) {
      this.loadGanttData(this.projectsDropdown[0].projectId);
    }
  }

  loadGanttData(projectId: number) {
    this.projectId = projectId;
    this.loadWorkingHours(projectId);
  }

  loadWorkingHours(projectId: number) {
    this.tableService
      .getAvailableWorkingHoursProject(projectId)
      .subscribe((answer: number) => {
        if (answer <= 0) {
          this.availableWorkingHours = 8;
          this.noDevelopersOnProject = true;
        } else {
          this.availableWorkingHours = answer;
        }
        this.initialData();
      });
  }

  countTaskFiels(tasks: Task[]) {
    const convertedTasks = [];
    const startDateValidator = new Date(tasks[0].dateStart);
    startDateValidator.setDate(startDateValidator.getDate() + GetCountTaskFields.checkWeekend(startDateValidator));
    let startDate = this.datePipe.transform(startDateValidator, 'yyyy-MM-dd');
    const tmpDate = startDate;
    let start = true;
    let availableDayHours = this.availableWorkingHours;
    tasks.forEach(task => {
      const pertAndRealTime = GetCountTaskFields
        .countTaskFields(task.optimisticTime, task.pessimisticTime);
      const recountDate = GetCountTaskFields
        .getNextAvailableDay(startDate, availableDayHours, this.availableWorkingHours, pertAndRealTime.pert);
      availableDayHours = recountDate.availableHoursCurrentDay;
      convertedTasks.push({
        pID: task.taskId,
        pName: task.taskName,
        pRes: task.user ? `${task.user.firstName} ${task.user.lastName || ''}` : 'Unassigned',
        pStart: start ? tmpDate : startDate,
        pClass: 'gtaskgreen',
        pEnd: this.datePipe.transform(recountDate.endDate, 'yyyy-MM-dd'),
        pComp: task.completed,
      });
      startDate = this.datePipe.transform(recountDate.nextTaskStartDate, 'yyyy-MM-dd');
      start = false;
    });
    return convertedTasks;
  }

  exportPdf() {
    const data = document.getElementById('gantt');
    const container = document.getElementById('gantt-container');
    html2canvas(data).then(canvas => {
      const contentDataURL = canvas.toDataURL('image/png');
      const width = container.offsetWidth;
      const height = container.offsetHeight;
      const pdf = new jspdf('l', 'cm', [width, height]);
      pdf.addImage(contentDataURL, 'PNG', 0, 0, pdf.internal.pageSize.getWidth(), pdf.internal.pageSize.getHeight());
      pdf.save(`${this.projectsDropdown.find(elem => elem.projectId === this.projectId).projectName}.pdf`);
      window.history.back();
    });
  }

  initialData() {
    this.chartService
      .getTasks(this.projectId)
      .subscribe((tasks: Task[]) => {
        if (!tasks.length) {
          this.hidden = true;
          return;
        }
        this.data = this.countTaskFiels(tasks);
        this.editorOptions = {
          vFormat: 'day'
        };
        this.hidden = false;
        if (this.export) {
          setTimeout(() => {
            this.exportPdf();
          }, 100);
        }
      });
  }
}


