import { Component, OnInit, ViewChild } from '@angular/core';
import { Member, DefaultServerMessage, User, InviteUserRequestBody, Role, Roles } from '@estimator/models';
import { MembersService, DashboardService, RegistrationService, RolesService, Patterns } from '@estimator/services';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { AlertComponent } from '../alert/alert.component';
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css'],
  providers: [MembersService, DashboardService, DatePipe, RegistrationService, RolesService]
})
export class MembersComponent implements OnInit {
  filerPlaceholder = 'Filter members';
  emailPattern = Patterns.emailPattern;
  displayedColumns: string[] = ['select', 'image', 'name', 'email', 'registerDate', 'confirmation', 'role'];
  dataSource: MatTableDataSource<Member>;
  selection = new SelectionModel<Member>(true, []);
  roles: Role[] = [];
  emailTaken = false;
  dataLoaded = false;
  usernameTaken = false;
  extendedFunctionality = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private membersService: MembersService,
    private dialog: MatDialog,
    private datePipe: DatePipe,
    private alert: MatSnackBar,
    private registrationService: RegistrationService,
    private rolesService: RolesService,
    private router: Router
  ) { }

  getMembers() {
    this.membersService
      .getMembers()
      .subscribe((answer: Member[]) => {
        this.dataSource = new MatTableDataSource(answer);
        this.selection = new SelectionModel<Member>(true, []);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataLoaded = true;
        this.dataSource.filterPredicate =
          (data: Member, filter: string) => {
            const filterFields = data.email
              + data.name
              + data.registerDate
              + data.role
              + data.confirmation;
            return filterFields.trim().toLowerCase().includes(filter);
          };
      });
  }

  ngOnInit() {
    this.rolesService
      .getRoleName()
      .subscribe((answer) => {
        if (answer === Roles.admin || answer === Roles.projectManager) {
          this.extendedFunctionality = true;
          this.membersService
            .getRoles()
            .subscribe((answerRole: Role[]) => {
              this.roles = answerRole;
            });
        }
        if (answer === Roles.guest) {
          this.router.navigateByUrl('not-found');
        }
        this.getMembers();
      });
  }

  validateEmailNotTaken(value: string) {
    if (value === '') {
      this.emailTaken = false;
      return;
    }
    this.registrationService
      .checkEmail(value)
      .subscribe((res: boolean) => {
        this.emailTaken = res;
      });
  }

  validateUsernameNotTaken(value: string) {
    if (value === '') {
      this.usernameTaken = false;
      return;
    }
    this.registrationService
      .checkUsername(value)
      .subscribe((res: boolean) => {
        this.usernameTaken = res;
      });
  }

  openAlert(message: string, isError: boolean, duration: number) {
    this.alert.openFromComponent(AlertComponent, {
      duration,
      panelClass: isError ? ['alert-danger'] : ['alert-success'],
      data: {
        isError,
        message
      }
    });
  }

  openAddMemberDialog(templateRef) {
    this.dialog.open(templateRef, {
      width: '400px',
    });
  }

  onAddMemberFormSubmit(form: NgForm) {
    if (form.invalid || this.emailTaken || this.usernameTaken) {
      return;
    }
    const user: User = {
      email: form.value.inputEmail,
      roleId: form.value.inputRole,
      registerDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      username: form.value.inputUsername
    };

    const inviteUserRequestBody: InviteUserRequestBody = {
      creatorTokenId: localStorage.getItem('token'),
      userBody: user
    };

    this.membersService
      .addMember(inviteUserRequestBody)
      .subscribe((answer: DefaultServerMessage) => {
        this.openAlert(answer.error ? answer.errorMessage : 'Member added successfully', answer.error, 2500);
        if (!answer.error) {
          form.resetForm();
          this.getMembers();
        }
      });
  }

  onDeleteMembers() {
    const membersId: number[] = [];
    this.selection.selected.forEach((member) => {
      membersId.push(member.userId);
    });
    this.membersService
      .deleteMembers(membersId)
      .subscribe((answer: DefaultServerMessage) => {
        const oneOrMore = this.selection.selected.length === 1 ? 'Member' : 'Members';
        this.openAlert(answer.error ? answer.errorMessage : `${oneOrMore} deleted successfully`, answer.error, 2500);
        if (!answer.error) {
          this.getMembers();
        }
      });
  }

  masterToggle() {
    this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: Member): string {
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row `;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
