import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-registration-dialog',
  templateUrl: 'registration-dialog.html'
})
export class RegistrationDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<RegistrationDialogComponent>) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
