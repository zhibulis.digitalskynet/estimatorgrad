import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { AlertComponent } from '../alert/alert.component';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { User, DefaultServerMessage, Role } from '@estimator/models';
import { RegistrationDialogComponent } from './registration-dialog/registration-dialog.component';
import { RegistrationService, Patterns } from '@estimator/services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [DatePipe, RegistrationService, FormBuilder],
})
export class RegistrationComponent implements OnInit {
  emailPattern = Patterns.emailPattern;
  myForm: FormGroup;
  rolesList: Role[] = [];
  emailTaken = false;
  usernameTaken = false;

  constructor(
    private datePipe: DatePipe,
    private registrationService: RegistrationService,
    public dialog: MatDialog,
    private alert: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
    this.registrationService
      .getRoles()
      .subscribe((answer: Role[]) => {
        this.rolesList = answer;
      });
  }

  openAlert(message: string, isError: boolean, duration: number) {
    this.alert.openFromComponent(AlertComponent, {
      duration,
      panelClass: isError ? ['alert-danger'] : ['alert-success'],
      data: {
        isError,
        message
      }
    });
  }

  validateEmailNotTaken(value: string) {
    if (value === '') {
      this.emailTaken = false;
      return;
    }
    this.registrationService.checkEmail(value).subscribe((res: boolean) => {
      this.emailTaken = res;
    });
  }

  validateUsernameNotTaken(value: string) {
    if (value === '') {
      this.usernameTaken = false;
      return;
    }
    this.registrationService.checkUsername(value).subscribe((res: boolean) => {
      this.usernameTaken = res;
    });
  }

  onRegistration(form: NgForm) {
    if (form.invalid || this.emailTaken || this.usernameTaken) {
      return;
    }
    const date = new Date();
    const userInfo: User = {
      username: form.value.username,
      email: form.value.email,
      password: form.value.password,
      registerDate: this.datePipe.transform(date, 'yyyyMMdd'),
      firstName: form.value.firstName,
      lastName: form.value.lastName,
      roleId: form.value.roleSelect,
    };
    this.registrationService.registrate(userInfo)
      .subscribe((answer: DefaultServerMessage) => {
        if (!answer.error) {
          this.openDialog();
        } else {
          this.openAlert(answer.errorMessage, true, 2500);
        }
      });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RegistrationDialogComponent);
    dialogRef.afterClosed().subscribe(() => {
      this.router.navigate(['login']);
    });
  }
}


