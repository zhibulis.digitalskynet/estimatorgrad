import { Component, OnInit, ElementRef, ChangeDetectorRef, ViewChild } from '@angular/core';
import { User, Roles, Notification } from '@estimator/models';
import { HeaderService, AuthServiceJwt, RolesService, SharedService } from '@estimator/services';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css'],
  providers: [HeaderService, RolesService],
})
export class AppHeaderComponent implements OnInit {
  user: User;
  notifications: Notification[];
  notificationsCount = 0;
  subscription: any;
  profileLoaded = false;
  notificationsListActive = false;
  guestFunctionality = false;
  @ViewChild('sidenav', { static: true }) sidenav: MatSidenav;
  constructor(
    private router: Router,
    private headerService: HeaderService,
    private authService: AuthServiceJwt,
    private roleService: RolesService,
    private sharedService: SharedService,
    private changeDetector: ChangeDetectorRef,
    private el: ElementRef
  ) { }

  loadProfile() {
    const idToken = localStorage.getItem('token');
    this.headerService
      .getProfileData(idToken)
      .subscribe((user) => {
        this.user = user;
        this.profileLoaded = true;
        this.changeDetector.detectChanges();
      });
  }

  ngOnInit() {
    this.headerReload();
    this.loadProfile();
    this.getNotificationsCount();
    this.roleService
      .getRoleName()
      .subscribe((answer: string) => {
        if (answer === Roles.guest) {
          this.guestFunctionality = true;
          this.changeDetector.detectChanges();
        }
      });
  }

  headerReload() {
    this.sharedService
      .headerReload
      .subscribe(() => {
        const idToken = localStorage.getItem('token');
        this.profileLoaded = false;
        this.headerService
          .getProfileData(idToken)
          .subscribe((user) => {
            this.user = user;
            this.profileLoaded = true;
            this.changeDetector.detectChanges();
          });
      });
  }

  onProfileClick() {
    this.sidenav.close();
    const token = localStorage.getItem('token');
    this.router.navigateByUrl(`profile/${token}`);
  }

  onNavigate(url: string) {
    this.sidenav.close();
    this.router.navigateByUrl(url);
  }

  notificationsActiveSwitcher() {
    this.notificationsListActive = !this.notificationsListActive;
  }

  onLogoutClick() {
    this.authService.logout();
  }

  hideNotifications(e: Event) {
    if (e.target !== this.el.nativeElement.querySelector('#notificationsToggler') &&
      e.target !== this.el.nativeElement.querySelector('#notificationsIcon')) {
      this.notificationsListActive = false;
    }
  }

  getNotificationsCount() {
    const idtoken = localStorage.getItem('token');
    this.headerService
      .getNotifications(idtoken)
      .subscribe((response: Notification[]) => {
        this.notificationsCount = response.length;
        this.notifications = response;
      });
  }
}
