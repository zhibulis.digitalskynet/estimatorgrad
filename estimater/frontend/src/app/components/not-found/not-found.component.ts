import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent {

  constructor(private router: Router) { }

  onSignUpClick(): void {
    this.router.navigateByUrl('registration');
  }
  onPrivacyClick(): void {
    this.router.navigateByUrl('privacy');
  }

  onLegalClick(): void {
    this.router.navigateByUrl('legal');
  }

  onContactClick(): void {
    this.router.navigateByUrl('contact');
  }
}
