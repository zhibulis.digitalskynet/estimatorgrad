import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  ProjectSettingsService, CreateProjectService, RolesService, EstimatorTableService,
  SharedService, GetCountTaskFields, MilestonesService
} from '@estimator/services';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ProjectCore, ProjectStatus, Roles, MemberWithRoleInfo,
  TaskAndBlockerList, MilestoneWithStatusModel, Note, UserInProject, Task
} from '@estimator/models';
import { FormControl } from '@angular/forms';
import { IAppState } from '../../store/state/app.state';
import { selectActiveTab } from '../../store/selectors/project-settings.selector';
import { Store, select } from '@ngrx/store';
import { SetTab } from '../../store/actions/project-settings.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-project-settings-view',
  templateUrl: './project-settings-view.component.html',
  styleUrls: ['./project-settings-view.component.css'],
  providers: [ProjectSettingsService, CreateProjectService, RolesService, EstimatorTableService]
})
export class ProjectSettingsViewComponent implements OnInit, OnDestroy {
  dataLoaded = false;
  projectId: number;
  projectSettingsData: ProjectCore;
  projectStatusList: ProjectStatus[] = [];
  milestonesInfo: MilestoneWithStatusModel[] = [];
  members: MemberWithRoleInfo[] = [];
  blockers: TaskAndBlockerList[] = [];
  extendedFunctionality = false;
  notes: Note[] = [];
  displayedMilestonesColumns = ['name', 'status', 'hours'];
  displayedMembersColumns = ['image', 'name', 'email', 'registerDate', 'confirmation', 'role'];
  displayedNotesColumns = ['noteName', 'note'];
  displayedTasksWithBlockersColumns = ['taskName', 'taskBlockerName'];

  selected = new FormControl();
  private activeTab$: Subscription;

  constructor(
    private projectSettingsViewService: ProjectSettingsService,
    private createProjectService: CreateProjectService,
    private route: ActivatedRoute,
    private roleService: RolesService,
    private tableService: EstimatorTableService,
    private router: Router,
    private sharedService: SharedService,
    private store: Store<IAppState>
  ) { }

  ngOnInit() {
    this.subscribeToTabsChange();
    this.projectId = +this.route.snapshot.paramMap.get('projectId');
    this.selected.setValue(this.sharedService.activeTabProjectSettings);
    this.projectSettingsViewService
      .checkProjectExist(this.projectId)
      .subscribe((answer: boolean) => {
        if (answer) {
          this.checkRole();
        } else {
          this.router.navigateByUrl('not-found');
        }
      });
  }

  private subscribeToTabsChange() {
    this.activeTab$ = this.store.pipe(select(selectActiveTab))
      .subscribe(activeTabIndex => {
        this.selected.setValue(activeTabIndex);
      });
  }

  selectedIndexChanged(index: number) {
    this.store.dispatch(new SetTab(index));
  }

  checkRole() {
    this.roleService
      .getRoleName()
      .subscribe((answer) => {
        if (answer === Roles.admin || answer === Roles.projectManager) {
          this.extendedFunctionality = true;
          this.loadOnStart();
        } else {
          this.checkUser();
        }
      });
  }

  loadOnStart() {
    // Load project statuses
    this.createProjectService
      .getProjectStatusList()
      .subscribe((projectStatusList: ProjectStatus[]) => {
        this.projectStatusList = projectStatusList;
      });
    // Load all data about project
    this.projectSettingsViewService
      .getProjectInfo(this.projectId)
      .subscribe((projectSettingsData: ProjectCore) => {
        this.projectSettingsData = projectSettingsData;
        this.dataLoaded = true;
      });
    this.loadMilestones();
    this.loadBlockers();
    this.loadNotes();
    this.loadMembers();
  }

  checkUser() {
    const userProject: UserInProject = {
      token: localStorage.getItem('token'),
      projectId: this.projectId
    };

    this.tableService
      .checkUserInProject(userProject)
      .subscribe((answer) => {
        if (answer) {
          this.loadOnStart();
        } else {
          this.router.navigateByUrl('not-found');
        }
      });
  }

  onEditClick() {
    this.sharedService.activeTabProjectSettings = this.selected.value;
    this.router.navigateByUrl(`project-settings/edit/${this.projectId}`);
  }

  loadNotes() {
    this.projectSettingsViewService
      .getProjectNotes(this.projectId)
      .subscribe((notes: Note[]) => {
        this.notes = notes;
      });
  }

  loadMembers() {
    this.projectSettingsViewService
      .getMembers(this.projectId)
      .subscribe((members: MemberWithRoleInfo[]) => {
        this.members = members;
      });
  }

  loadBlockers() {
    this.projectSettingsViewService
      .getBlockers(this.projectId)
      .subscribe((blockers: TaskAndBlockerList[]) => {
        this.blockers = blockers;
      });
  }

  loadMilestones() {
    this.projectSettingsViewService
      .getMilestones(this.projectId)
      .subscribe((milestones: MilestoneWithStatusModel[]) => {
        this.milestonesInfo = milestones;
        this.milestonesInfo.forEach((milestone, index, arr) => {
          const hours = MilestonesService.getCountMilestoneTasks(milestone.tasks);
          arr[index].hours = hours;
        });
      });
  }

  // Find current project manager or return notification if not exist
  viewGetCurrentProjectManager() {
    const manager = this.members.find(member => member.userId === this.projectSettingsData.currentProjectManagerId);
    if (!this.projectSettingsData.currentProjectManagerId || !manager) {
      return 'Project manager not assigned to the project!';
    } else {
      return `${manager.firstName} ${manager.lastName || ''}`;
    }
  }

  // Get project description or return notification if not exist
  viewGetRrojectDescription() {
    const description = this.projectSettingsData.description;
    return description || 'Description not exists';
  }

  // Get current project status
  viewGetProjectStatus() {
    const projectStatusId = this.projectSettingsData.projectStatusId;
    return this.projectStatusList.find(status => status.projectStatusId === projectStatusId).projectStatusName;
  }

  // Get project url
  viewGetUrl() {
    return this.projectSettingsData.url;
  }

  // Get end date
  viewGetEndDate() {
    return this.projectSettingsData.endDate || 'Counted tasks required';
  }

  // Get start date
  viewGetStartDate() {
    return this.projectSettingsData.startDate || 'Counted tasks required';
  }

  ngOnDestroy() {
    this.activeTab$.unsubscribe();
  }
}
