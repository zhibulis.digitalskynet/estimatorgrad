import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '@estimator/models';
import { LoginService, AuthServiceJwt } from '@estimator/services';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertComponent } from '../alert/alert.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [LoginService, AlertComponent],
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private router: Router,
    private authServiceJwt: AuthServiceJwt,
    private alert: MatSnackBar
  ) { }

  ngOnInit() {
    if (this.authServiceJwt.isAuthenticated()) {
      this.router.navigateByUrl('');
      return;
    }
  }

  openAlert(message: string, isError: boolean, duration: number) {
    this.alert.openFromComponent(AlertComponent, {
      duration,
      panelClass: isError ? ['alert-danger'] : ['alert-success'],
      data: {
        isError,
        message
      }
    });
  }

  onLogin(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    const loginData: User = {
      username: form.value.username,
      password: form.value.password,
    };

    this.loginService.tryLogin(loginData)
      .subscribe(answer => {
        if (answer.success) {
          this.authServiceJwt.login(answer.token);
        } else {
          const duration = 2500;
          this.openAlert(answer.error, true, duration);
        }
      });
  }

  onSignUpClick(): void {
    this.router.navigateByUrl('registration');
  }

  onPasswordResetClick(): void {
    this.router.navigateByUrl('forgot-password');
  }

  onPrivacyClick(): void {
    this.router.navigateByUrl('privacy');
  }

  onLegalClick(): void {
    this.router.navigateByUrl('legal');
  }

  onContactClick(): void {
    this.router.navigateByUrl('contact');
  }
}
