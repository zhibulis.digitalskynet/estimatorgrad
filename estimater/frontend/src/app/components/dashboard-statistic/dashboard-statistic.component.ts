import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DashboardStatisticService, RolesService, GetCountTaskFields } from '@estimator/services';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DashboardStatistic, Roles, PieData, TasksHours } from '@estimator/models';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard-statistic',
  templateUrl: './dashboard-statistic.component.html',
  styleUrls: ['./dashboard-statistic.component.css'],
  providers: [DashboardStatisticService, RolesService]
})
export class DashboardStatisticComponent implements OnInit, AfterViewInit {

  dataSource: MatTableDataSource<DashboardStatistic>;
  displayedColumns = [
    'project',
    'startDate',
    'endDate',
    'status',
    'membersCount',
    'tasksCount',
    'developersCount'
  ];
  tableLoaded = false;
  totalTasks = 0;
  totalTimeLost = 0;
  membersPieLoaded = false;
  projectStatusPieLoaded = false;
  statisticTabActive = false;
  // pie options
  pieData: PieData[];
  pieProjectStatusData: PieData[];
  doughnut = true;
  arcWidth = 0.6;
  showLegend = true;
  showLabels = true;
  isDoughnut = false;
  pieLegendPosition = 'below';
  pieProjectLegendPosition = 'below';
  pieColorTheme = {
    domain: ['#9cce04', '#fb634b', '#ffe008']
  };
  pieProjectColorTheme = {
    domain: ['#ecef33', '#0d86cc', '#d42e26', '#9cce04']
  };

  constructor(
    private dashboardStatisticService: DashboardStatisticService,
    private roleService: RolesService,
    private router: Router
  ) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    const idToken = localStorage.getItem('token');
    if (idToken === null) {
      this.router.navigateByUrl('login');
    } else {
      this.loadPieData();
      this.loadProjectStatusWidgetData();
      this.loadTasksHoursWidgetData();
      this.getRoleName(idToken);
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    });
  }

  loadProjectStatusWidgetData() {
    this.dashboardStatisticService
      .getProjectStatusWidgetData()
      .subscribe((answer: PieData[]) => {
        this.pieProjectStatusData = answer;
        this.projectStatusPieLoaded = true;
        setTimeout(() => {
          window.dispatchEvent(new Event('resize'));
        });
      });
  }

  loadTasksHoursWidgetData() {
    this.dashboardStatisticService
      .getTasksHoursWidgetData()
      .subscribe((answer: TasksHours) => {
        this.totalTasks = answer.tasksCount;
        answer.completedTasks.forEach((task) => {
          this.totalTimeLost += GetCountTaskFields
            .countTaskFields(task.optimisticTime, task.pessimisticTime).pert;
        });
      });
  }

  loadPieData() {
    this.dashboardStatisticService
      .getPieData()
      .subscribe((answer: PieData[]) => {
        this.pieData = answer;
        this.membersPieLoaded = true;
        setTimeout(() => {
          window.dispatchEvent(new Event('resize'));
        });
      });
  }

  loadData(answer: DashboardStatistic[]) {
    this.dataSource = new MatTableDataSource(answer);
    this.dataSource.data.forEach((data, index) => {
      this.dataSource.data[index]
        .projectManagerName = this.viewGetProjectManagerName(data.projectManagerName);
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'project': {
          return item.projectManagerName;
        }
        case 'status': {
          return item.projectStatusName;
        }
        default: {
          return item[property];
        }
      }
    };
    this.tableLoaded = true;
  }

  getAllProjects() {
    this.dashboardStatisticService
      .getAllDashboardProjects()
      .subscribe((answer: DashboardStatistic[]) => {
        this.loadData(answer);
      });
  }

  getProjectsWhereUserExist(idToken: string) {
    this.dashboardStatisticService
      .getProjectsWhereExist(idToken)
      .subscribe((answer: DashboardStatistic[]) => {
        if (!answer.length) {
          this.router.navigateByUrl('home');
        } else {
          this.loadData(answer);
        }
      });
  }

  getRoleName(idToken: string) {
    this.roleService
      .getRoleName()
      .subscribe((answer) => {
        if (answer === Roles.admin || answer === Roles.projectManager) {
          this.getAllProjects();
        } else {
          this.getProjectsWhereUserExist(idToken);
        }
      });
  }

  getChipBackColor(projectStatusName: string) {
    switch (projectStatusName) {
      case ('In progress'):
        return { color: '#ffffff', 'background-color': '#81a33f' };
      case ('Pending'):
        return { 'background-color': '#ecda2b' };
      case ('Completed'):
        return { color: '#ffffff', 'background-color': '#0d86cc' };
      case ('Not Started'):
        return { color: '#ffffff', 'background-color': '#d42e26' };
      default:
        return { 'background-color': '#ffffff' };
    }
  }

  viewGetProjectManagerName(projectManagerName: string) {
    if (projectManagerName) {
      return `managed by: ${projectManagerName}`;
    } else {
      return 'project manager not set';
    }
  }
}
