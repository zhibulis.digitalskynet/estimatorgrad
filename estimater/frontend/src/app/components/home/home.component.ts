import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CheckProjectService, RolesService } from '@estimator/services';
import { Roles } from '@estimator/models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [CheckProjectService, RolesService]
})
export class HomeComponent implements OnInit {

  hidden = true;
  extendedFunctionality = false;

  constructor(
    private router: Router,
    private checkProjectService: CheckProjectService,
    private roleService: RolesService) { }

  ngOnInit() {
    const idToken = localStorage.getItem('token');
    this.checkProjectService
      .checkProjects(idToken)
      .subscribe((answer) => {
        if (answer) {
          this.router.navigateByUrl('dashboard');
        } else {
          this.loadScreen();
          this.hidden = false;
        }
      });
  }

  loadScreen() {
    this.roleService
      .getRoleName()
      .subscribe((answer: string) => {
        if (answer === Roles.admin || answer === Roles.projectManager) {
          this.extendedFunctionality = true;
        }
        this.hidden = false;
      });
  }


  onCreateClick() {
    this.router.navigateByUrl('create-project');
  }

  onSignOut() {
    localStorage.removeItem('token');
  }
}
