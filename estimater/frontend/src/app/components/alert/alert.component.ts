import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { Alert } from '../../models/alert.model';
import { MAT_SNACK_BAR_DATA, MatSnackBarConfig } from '@angular/material/snack-bar';

@Component({
  selector: 'app-alert',
  templateUrl: 'alert.component.html',
  styleUrls: ['./alert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AlertComponent {
  isError = false;
  message = '';
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: Alert) {
    this.isError = data.isError;
    this.message = data.message;
  }
}
