import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-input-filter',
  templateUrl: './input-filter.component.html',
  styleUrls: ['./input-filter.component.css']
})
export class InputFilterComponent implements OnInit {

  @Output() changeEvent = new EventEmitter();
  @Input() placeholder: string;
  constructor() { }

  ngOnInit(): void {
  }

  inputChangeHandler(value: string) {
    this.changeEvent.emit(value);
  }

}
