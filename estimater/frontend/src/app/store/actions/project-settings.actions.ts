import { Action } from '@ngrx/store';

export enum EProjectSettingsActions {
    SetTab = '[Project Settings] Set Tab Number'
}

export class SetTab implements Action {
    public readonly type = EProjectSettingsActions.SetTab;
    constructor(public payload: number) { }
}

export type ProjectSettingsActions = SetTab;
