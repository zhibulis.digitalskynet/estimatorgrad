import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { IProjectSettingsState } from '../state/project-settings.state';

const projectSettingsSelector = (state: IAppState) => state.projectSettings;

export const selectActiveTab = createSelector(
    projectSettingsSelector,
    (state: IProjectSettingsState) => state.activeTabProjectSettings
);
