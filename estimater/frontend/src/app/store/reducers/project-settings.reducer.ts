import { EProjectSettingsActions, ProjectSettingsActions } from '../actions/project-settings.actions';
import { initialAppState } from '../state/app.state';
import { IProjectSettingsState } from '../state/project-settings.state';

export function projectSettingsReducers(
  state = initialAppState.projectSettings,
  action: ProjectSettingsActions
): IProjectSettingsState {
  switch (action.type) {
    case EProjectSettingsActions.SetTab: {
      return { ...state, activeTabProjectSettings: action.payload };
    }
    default:
      return state;
  }
}
