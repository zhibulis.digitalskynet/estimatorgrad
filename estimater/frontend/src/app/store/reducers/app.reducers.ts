import { ActionReducerMap } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { projectSettingsReducers } from './project-settings.reducer';

export const appReducers: ActionReducerMap<IAppState, any> = {
    projectSettings: projectSettingsReducers
};
