import { IProjectSettingsState, initialProjectSettingsState } from './project-settings.state';

export interface IAppState {
  projectSettings: IProjectSettingsState;
}

export const initialAppState: IAppState = {
  projectSettings: initialProjectSettingsState
};

export function getInitialState(): IAppState {
  return initialAppState;
}
