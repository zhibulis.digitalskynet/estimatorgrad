export interface IProjectSettingsState {
    activeTabProjectSettings: number;
}

export const initialProjectSettingsState: IProjectSettingsState = {
    activeTabProjectSettings: 0
};
