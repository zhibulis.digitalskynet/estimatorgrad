import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AuthServiceJwt, AuthGuardService as AuthGuard } from '@estimator/services';
import {
  AboutComponent, ContactComponent, DashboardComponent,
  EstimatorTableComponent, HomeComponent, LegalComponent, LoginComponent,
  NotFoundComponent, PrivacyComponent, RegistrationComponent,
  SupportComponent, DashboardStatisticComponent, TermsComponent, EstimateChartComponent,
  ProfileComponent, MembersComponent, ProjectSettingsComponent, ProjectSettingsViewComponent,
  DeleteInviteComponent, DashboardProjectStatisticComponent, ForgotPasswordResetComponent, ForgotPasswordComponent,
  InvitePasswordSetComponent
} from '@estimator/components';
const routes: Routes = [
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'members',
    component: MembersComponent,
    canActivate: [AuthGuard]
  },
  {

    path: 'profile/:id',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'estimator-table/:projectId',
    component: EstimatorTableComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'project-settings/edit/:projectId',
    component: ProjectSettingsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'project-settings/view/:projectId',
    component: ProjectSettingsViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'password-initialization/:setPasswordToken',
    component: InvitePasswordSetComponent,
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dashboard-statistic',
    component: DashboardStatisticComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dashboard-project-statistic/:projectId',
    component: DashboardProjectStatisticComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'terms',
    component: TermsComponent
  },
  {
    path: 'support',
    component: SupportComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'legal',
    component: LegalComponent
  },
  {
    path: 'privacy',
    component: PrivacyComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'user-in-project/:userId/:projectId',
    component: DeleteInviteComponent
  },
  {
    path: 'estimate-chart/:projectId',
    component: EstimateChartComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'estimate-chart/:projectId/:export',
    component: EstimateChartComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'estimate-chart',
    component: EstimateChartComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
  },
  {
    path: 'forgot-password-reset/:resetPasswordToken',
    component: ForgotPasswordResetComponent,
  },
  {
    path: '**',
    redirectTo: 'not-found'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [

    AuthGuard,
    AuthServiceJwt,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService
  ]
})
export class AppRoutingModule { }
