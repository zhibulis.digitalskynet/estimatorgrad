**Need to check all website before deploying.**

* [ ]  Make sure admin panel works as expected

* [ ]  Make sure all public pages work as expected

* [ ]  Make sure SSR works correctly

* [ ]  Make sure all new routes created due the sprint are in sitemap.xml

* [ ]  Make sure robot.txt has all necessary rules
