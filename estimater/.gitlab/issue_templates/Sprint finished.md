* [ ]  Build SQL Migration of all database work - no need to commit a sprint script to the code - just attach it to the file (we have it all commited already)
* [ ]  Make sure newly added routes are in sitemap.xml
* [ ]  Make sure newly added routes are in robot.txt
* [ ]  Make sure you created MR correctly:
    * [ ]  Make sure all sprint tasks are closed
    * [ ]  Make sure PM gives a greenline to create a MR of entire `dev` to `master`
    * [ ]  Make sure all the above checks are done - then go to the next task:
    * [ ]  Create a MR of dev to master
