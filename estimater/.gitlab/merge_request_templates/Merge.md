### Developer checks:

* [ ] Have you followed the guidelines in our [Coding Style](https://gitlab.digitalskynet.com/digitalskynet/knowledge-base/wikis/C%23-Coding-Style-and-Best-Practice)?
* [ ] Have you followed our [Gitlab Workflow](https://gitlab.digitalskynet.com/digitalskynet/knowledge-base/wikis/GitLab-Workflow)?
* [ ] Have you lint your code locally prior to submission?
* [ ] Does your submission pass tests?


## Quick PR Description & Checks:

* [ ] Does your submission removes a part of functionality?
     * [ ]  Have you considered that this functionality is not being used anywhere on the project?
* [ ] Does your submission changes the database?
     * [ ]  Have you attached `.sql` migration to the task?
     * [ ]  Have you added `.sql` migration to the `/SQL` folder?
     * [ ]  Have you added `Changes Database` label to the task?
